import binascii
import struct
import random
from typing import List

from Crypto.PublicKey import RSA
from Crypto.Util.number import long_to_bytes

import dtella.common.local_config as local
import dtella.bridge_config as bridge
import dns.resolver

from dtella.util import parse_bytes, Ad
from dtella.common.packet import PacketEncrypter

from hashlib import md5


def dns_pull():
    try:
        return local.dconfig_puller.query()
    except dns.resolver.Timeout:
        return


def parse_records(records):

    results = {"pkhash": set(), "ipcache": None}
    pk_enc = PacketEncrypter(local.network_key)

    for line in records:
        try:
            name, value = line.split('=', 1)
        except ValueError:
            continue

        name = name.lower()

        if name == 'minshare':
            try:
                results["minshare"] = parse_bytes(value)
            except ValueError:
                pass

        elif name == 'version':
            try:
                results["version"] = value.split()
            except ValueError:
                pass

        elif name == 'pkhash':
            h = binascii.a2b_base64(value)
            results["pkhash"].add(h)

        elif name == 'ipcache':
            try:
                data = binascii.a2b_base64(value)
                data = pk_enc.decrypt(data)
            except (ValueError, binascii.Error):
                continue

            if (len(data) - 4) % 6 != 0:
                continue

            # 2011-08-21: New nodes ignore the value of 'when'.
            when = struct.unpack("!I", data[:4])
            ipps = [data[i:i + 6] for i in range(4, len(data), 6)]

            results["ipcache"] = when, ipps

    return results


def build_dns(exempt_ips: List[str], ipp_cache: List[bytes]):

    def b64(arg):
        return binascii.b2a_base64(arg).rstrip()

    entries = bridge.dconfig_fixed_entries.copy()
    pk_enc = PacketEncrypter(local.network_key)

    # Generate public key hash
    if bridge.private_key:
        pubkey = long_to_bytes(RSA.construct(bridge.private_key).n)
        entries['pkhash'] = b64(md5(pubkey).digest()).decode()

    # Collect IPPs for the ipcache string
    GOAL = 10
    ipps = ipp_cache.copy()
    exempt_ips = list(map(bytes, map(Ad, exempt_ips)))
    for _ip in exempt_ips:
        try:
            ipps.remove(_ip)
        except ValueError:
            pass
    random.shuffle(ipps)
    ipps = ipps[:GOAL - len(exempt_ips)] + exempt_ips
    random.shuffle(ipps)

    ipcache = b'\xFF\xFF\xFF\xFF' + b''.join(ipps)
    ipcache = b64(pk_enc.encrypt(ipcache))

    entries['ipcache'] = ipcache.decode()

    return entries


if __name__ == "__main__":
    print("current")
    records = dns_pull()
    data = parse_records(records)
    print(records)
    print(data)
    for ip in sorted(data["ipcache"][1]):
        print(Ad(ip))

    # print("\nupdate")
    # new_records = build_dns(["98.228.245.155", "128.210.109.182:3389"], data["ipcache"][1])
    # print(new_records)
    # new_data = parse_records([f"{k}={v}" for k, v in new_records.items()])
    # print(new_data)
    # for ip in sorted(new_data["ipcache"][1]):
    #     print(Ad(ip))

    # bridge.dconfig_push_func(new_records)
