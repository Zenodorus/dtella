import asyncio
import time

from dtella.util.scheduled_task import ScheduledTask


async def some_task():
    print(int(time.time() % 1000), "some_task")
    await asyncio.sleep(5)
    print(int(time.time() % 1000), "done with some_task")


async def main():
    # print(int(time.time() % 1000), "making dcall")
    # dcall = ScheduledTask(some_task(), delay=5)
    # await asyncio.sleep(11)

    print(int(time.time() % 1000), "making dcall")
    dcall = ScheduledTask(some_task(), delay=5)
    await asyncio.sleep(1)
    print(dcall.cancel())
    print(dcall.reset(2))
    await asyncio.sleep(5)
    print(dcall.cancel())
    print(dcall.reset(0))
    await asyncio.sleep(5)


if __name__ == "__main__":
    asyncio.run(main())
