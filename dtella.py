#!/usr/bin/env python

"""
Dtella - Startup Module
Copyright (C) 2007-2008  Dtella Labs (http://www.dtella.org/)
Copyright (C) 2007-2008  Paul Marks (http://www.pmarks.net/)
Copyright (C) 2007-2008  Jacob Feisley (http://www.feisley.com/)
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

# When Dtella is packaged by py2app, dtella.py and the dtella.* package are
# split into separate directories, causing the import to fail.  We'll hack
# around the problem by stripping the base directory from the path.

from dtella.common import terminate
import dtella.common.local_config as local

if __name__ == '__main__':
    try:
        import dtella.common
    except ImportError:
        import sys
        sys.path = [p for p in sys.path if p != sys.path[0]]

import sys
import time
import getopt


def main():
    # Parse command-line arguments
    allowed_opts = []
    usage_str = f"Usage: {sys.argv[0]}"

    try:
        import dtella.client
    except ImportError:
        pass
    else:
        usage_str += " [--port=#] [--terminate]"
        allowed_opts.extend(['port=', 'terminate'])

    try:
        import dtella.bridge.server
    except ImportError:
        pass
    else:
        usage_str += " [--bridge] [--makeprivatekey]"
        allowed_opts.extend(['bridge', 'makeprivatekey'])

    try:
        import dtella.dconfig.main
    except ImportError:
        pass
    else:
        usage_str += " [--dconfigpusher]"
        allowed_opts.extend(['dconfigpusher'])

    try:
        opts, args = getopt.getopt(sys.argv[1:], '', allowed_opts)
    except getopt.GetoptError:
        print(usage_str)
        return

    opts = dict(opts)

    if '--bridge' in opts:
        from dtella.bridge.server.main import DtellaMain_Bridge
        DtellaMain_Bridge().start()
        return

    if '--dconfigpusher' in opts:
        from dtella.dconfig.main import DtellaMain_DconfigPusher
        DtellaMain_DconfigPusher().start()
        return

    if '--makeprivatekey' in opts:
        from dtella.bridge.private_key import makePrivateKey
        makePrivateKey()
        return

    # User-specified TCP port
    dc_port = local.dc_port
    if '--port' in opts:
        try:
            dc_port = int(opts['--port'])
            if not (1 <= dc_port < 65536):
                raise ValueError
        except ValueError:
            print("Port must be between 1-65535")
            return
        else:
            local.dc_port = dc_port

    # Try to terminate an existing process
    if '--terminate' in opts:
        if terminate(dc_port):
            # Give the other process time to exit first
            print("Sleeping...")
            time.sleep(2.0)
        print("Done.")
        return

    if local.use_bridge:
        try:
            from dtella.bridge.client import main
        except ImportError as e:
            e.msg = "client dtella configuration requires missing bridge module"
            raise e
    else:
        from dtella.client import main
    main(dc_port)


if __name__ == '__main__':
    main()
