"""
Dtella - Message Routing Manager
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import random
import struct
import time
from typing import Iterable, TYPE_CHECKING, Dict, Optional

from dtella.common import MessageCollisionError, BadTimingError
from dtella.common.node import PacketNX
from dtella.util import Ad

if TYPE_CHECKING:
    from dtella.common.base import DtellaMain_Base
    from dtella.common.packet import BroadcastPacket


class _BroadcastMessage(object):

    MESSAGE_EXPIRE_EXTEND = 180

    def __init__(self, packet: "BroadcastPacket", tries: int, sendto: Iterable[bytes], main: "DtellaMain_Base"):
        self.status_pktnum: Optional[int] = None

        self._create_time: float = time.monotonic()
        self._sending: Dict[bytes, asyncio.Task] = {
            nb_ipp: asyncio.create_task(self._schedule_send(packet, tries, nb_ipp, main)) for nb_ipp in sendto}
        self._timeout_task: asyncio.Task = asyncio.create_task(self._message_timeout())
        self._timeout_time: float = time.monotonic() + _BroadcastMessage.MESSAGE_EXPIRE_EXTEND

    async def _message_timeout(self):
        while (t := time.monotonic()) < self._timeout_time:
            await asyncio.sleep(self._timeout_time - t)

    def reschedule_timeout(self):
        if not self._timeout_task.done():
            self._timeout_time = time.monotonic() + _BroadcastMessage.MESSAGE_EXPIRE_EXTEND

    @property
    def expired(self):
        return self._timeout_task.done()

    async def _schedule_send(self, packet: "BroadcastPacket", tries: int, nb_ipp: bytes, main: "DtellaMain_Base"):
        """
        Send this message, retransmitting every few seconds until a timeout is reached or the message was
        acknowledged.
        """

        # if we're passing an NF to the node who's dying, then up the number of retries to 8, because it's rather
        # important.
        if packet.code == b'NF' and packet.src_ipp == nb_ipp:
            tries = 8

        nb_ad = Ad(nb_ipp)
        init_hops = packet.hops
        while tries > 0:
            # decrease the hop count by the number of seconds the packet has been buffered, dropping the packet if
            # buffered time exceeds hops
            buffered_time = int(time.monotonic() - self._create_time)
            if init_hops >= buffered_time:
                packet.hops = init_hops - buffered_time
            else:
                break

            if tries > 0:
                await main.ph.sendPacket(packet, nb_ad)
            if tries > 1:
                await asyncio.sleep(random.uniform(1.0, 2.0))
            tries -= 1

        del self._sending[nb_ipp]

    def cancel_send_to_nb(self, nb_ipp: bytes):
        """Stop _sending this message to the given neighbor."""
        try:
            self._sending.pop(nb_ipp).cancel()
        except KeyError:
            return

    def cancel_all(self):
        """Cancel all pending messages and cancel the expiration."""
        for d in self._sending.values():
            d.cancel()
        self._sending.clear()
        self._timeout_task.cancel()


class MessageRoutingManager(object):

    def __init__(self, main: "DtellaMain_Base", pktnum: int):
        self.main: "DtellaMain_Base" = main
        self.msgs: Dict[bytes, _BroadcastMessage] = {}
        self._cache_task: asyncio.Task = asyncio.create_task(self._clear_message_cache())

        self.rcollide_last_NS = None
        self.rcollide_ipps = set()

        self.search_pktnum: int = pktnum
        self.chat_pktnum: int = pktnum

    async def _clear_message_cache(self):
        while True:
            await asyncio.sleep(60)
            expired = [k for k, v in self.msgs.items() if v.expired]
            for k in expired:
                del self.msgs[k]

    def poke_message(self, ack_key: bytes, nb_ipp: Optional[bytes]):
        """
        Check if the message is in the cache.  If it is, then extend its timeout and remove the specified neighbor
        from its list of destinations.  Return true if the message is still in the cache.
        """
        try:
            m = self.msgs[ack_key]
        except KeyError:
            return False
        else:
            m.reschedule_timeout()
            if nb_ipp:
                m.cancel_send_to_nb(nb_ipp)
            return True

    def new_message(self, packet: "BroadcastPacket", tries: int, nb_ipp: Optional[bytes] = None):
        """
        Forward a new message to my neighbors, trying tries times.  nb_ipp is the IP:Port of the neighbor that sent
        it to me, so don't send it back to them.
        """

        ack_key = packet.ack_key

        if ack_key in self.msgs:
            raise MessageCollisionError(f"duplicate {packet.code.decode()}")

        # Get my current neighbors who we know to be alive.  We don't
        # need to verify pn.u_got_ack because it doesn't really matter.
        sendto = (pn.ipp for pn in self.main.osm.pgm.pnbs.values() if pn.got_ack)

        # Start _sending.
        m = self.msgs[ack_key] = _BroadcastMessage(packet, tries, sendto, self.main)
        assert self.poke_message(ack_key, nb_ipp), "new message disappeared"

        if packet.src_ipp == self.main.osm.me.ipp:
            assert not self.main.hide_node, "hidden nodes can't broadcast"

            if packet.code in (b'NH', b'CH', b'SQ', b'TP'):
                # Save the current status_pktnum for this message, because
                # it's useful if we receive a Reject message later.
                m.status_pktnum = self.main.osm.me.status_pktnum

            elif packet.code == b'NS':
                # Save my last NS message, so that if it gets rejected,
                # it can be interpreted as a remote nick collision.
                self.rcollide_last_NS = m
                self.rcollide_ipps.clear()

    def newMessage(self, data, tries, nb_ipp=None):
        pass

    def receivedRejection(self, ack_key: bytes, ipp: bytes):
        from dtella.client.dc import DCHandler

        # Broadcast rejection, sent in response to a previous broadcast if
        # another node doesn't recognize us on the network.

        # We attach a status_pktnum to any broadcast which could possibly
        # be rejected.  If this matches my status_pktnum now, then we should
        # broadcast a new status, which will change status_pktnum and
        # prevent this same broadcast from triggering another status update.

        osm = self.main.osm

        try:
            m = self.msgs[ack_key]
        except KeyError:
            raise BadTimingError("Reject refers to an unknown broadcast")

        if m is self.rcollide_last_NS:
            # Remote nick collision might have occurred

            self.rcollide_ipps.add(ipp)

            if len(self.rcollide_ipps) > 1:
                # Multiple nodes have reported a problem, so tell the user.
                if isinstance(dch := self.main.state_observer, DCHandler):
                    dch.remote_nick_collision()

                # No more reports until next time
                self.rcollide_last_NS = None
                self.rcollide_ipps.clear()

        if osm.me.status_pktnum == m.status_pktnum:
            # One of my hash-containing broadcasts has been rejected, so
            # send my full status to refresh everyone.
            # (Note: m.status_pktnum is None for irrelevant messages.)
            osm.send_my_status()

    def getPacketNumber_search(self):
        self.search_pktnum = (self.search_pktnum + 1) % 0x100000000
        return self.search_pktnum

    def getPacketNumber_chat(self):
        self.chat_pktnum = (self.chat_pktnum + 1) % 0x100000000
        return self.chat_pktnum

    def getPacketNumber_status(self):
        me = self.main.osm.me
        me.status_pktnum = (me.status_pktnum + 1) % 0x100000000
        return me.status_pktnum

    def broadcastHeader(self, kind, src_ipp, hops=32, flags=0):
        # Build the header used for all broadcast packets
        packet = [kind]
        packet.append(self.main.osm.me.ipp)
        packet.append(struct.pack('!BB', hops, flags))
        packet.append(src_ipp)
        return packet

    # def broadcast_header(self, packet_type: Type[Packet], src_ipp, hops=32, flags=0, **kwargs):
    #     pass

    async def shutdown(self):
        """Stop all messages and shutdown the manager.  Broadcast NX packet."""

        for m in self.msgs.values():
            m.cancel_all()
        self.msgs.clear()
        self._cache_task.cancel()

        osm = self.main.osm
        if osm and osm.synced and not self.main.hide_node:
            packet = PacketNX(nb_ipp=osm.me.ipp, hops=32, b_flags=0, src_ipp=osm.me.ipp, sesid=osm.me.sesid)
            await asyncio.gather(*(self.main.ph.sendPacket(packet, Ad(pn.ipp))
                                   for pn in osm.pgm.pnbs.values()))
