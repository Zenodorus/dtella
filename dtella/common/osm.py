"""
Dtella - Common Online State Manager
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import bisect
import os
import random
from time import time as seconds
from typing import Iterable, Optional, Dict, List, Union, Generator, TYPE_CHECKING, Set

import dtella.common as common
from dtella.common import NODE_EXPIRE_EXTEND, local_config as local, NickError
from dtella.common.node import PacketNS, PacketNH, User, Node, DtellaNodeMe
from dtella.common.log import LOG
from dtella.common.sync import PacketYR
from dtella.common.packet import Packet, BadPacketError, register_packet_classes
from dtella.util import Ad, validate_nick


if TYPE_CHECKING:
    from dtella.common.base import DtellaMain_Base


class PacketEC(Packet):
    """
    Login Echo Packet.  Packet sent to myself to observe how loopback routing works.

    Attributes:
        code: the 2-letter string 'PF'
        rand: 8-byte random string
    """

    _code = b'EC'
    _data_model = [('code', '2s'), ('rand', '8s')]
    __slots__ = ('rand',)


register_packet_classes(PacketEC)


class BaseOnlineStateManager(object):
    """
    Base class for the OnlineStateManager.  This class manages the logic of when nodes are live and dead and directs
    callbacks.

    Attributes:
        main: main Dtella namespace
        synced: state boolean for if this node is fully online now
        me: MeNode instance with my node data

        _lookup_ipp: dictionary lookup for nodes by IP:Port
        _users: dictionary lookup for users by nick
        _nodes: list of online nodes sorted by distance.  Prior to synced, this list consists of IP:Port bytes

        _echo_packet: PacketEC sent to determine loopback routing info
        _echo_timeout_task: waiting for _echo_packet to return

        _status_throttle: list of timestamps for when a status update was sent
    """

    def __init__(self, main: "DtellaMain_Base", my_ipp: bytes, node_ipps: Iterable[bytes]):
        self.main: "DtellaMain_Base" = main

        self.synced: bool = False

        self.me: DtellaNodeMe = DtellaNodeMe(my_ipp, main)
        # self._lookup_ipp: Dict[bytes, Node] = {ipp: Node(ipp, main) for ipp in node_ipps}
        self._lookup_ipp: Dict[bytes, Node] = {}
        self._users: Dict[str, User] = {}
        self._nodes: List[Union[bytes, Node]] = list(node_ipps)
        random.shuffle(self._nodes)

        # Send a packet to myself to determine how my router (if any) reacts to loopbacked packets.
        self._echo_packet: PacketEC = PacketEC(rand=os.urandom(8))
        self._echo_timeout_task: Optional[asyncio.Task] = asyncio.create_task(self._login_echo_timeout())

        self.pgm: common.PingManager = common.PingManager(main)
        self.sm: common.SyncManager = common.SyncManager(main)

        r = random.randint(0, 0xFFFFFFFF)
        self.me.status_pktnum = r
        self.mrm: common.MessageRoutingManager = common.MessageRoutingManager(main, r)
        self.tm: common.TopicManager = common.TopicManager(main)
        self.yqrm: common.SyncRequestRoutingManager = common.SyncRequestRoutingManager(main)

        # Keep track of outbound status rate limiting
        self._status_throttle: List[float] = [0]
        self._status_task: Optional[asyncio.Task] = None
        self._next_status_update_time: float = 0

        if self._nodes:
            self.pgm.schedule_make_new_links()
        else:
            asyncio.create_task(self.sync_complete())

    def __bool__(self):
        """Override the use of __len__ in conditionals when testing for None."""
        return True

    def __len__(self):
        """Number of dtella nodes on the network."""
        return len(self.nodes)

    # node lists

    @property
    def nodes(self) -> List[Node]:
        if self.synced:
            return self._nodes
        else:
            return []

    def nearest_node_ipp(self) -> Generator[bytes, None, None]:
        """
        Generator that yields IP:Ports of the nodes in order of distance if the node is synced.  If the node is not
        synced, yield a random order.
        """
        for v in self._nodes:
            if isinstance(v, Node):
                yield v.ipp
            else:
                yield v

    def random_ipp_sample(self, num: int) -> Set[bytes]:
        """Return a random sample of online nodes."""
        if not self.synced:
            return set()
        return {n.ipp for n in random.sample(self._nodes + [self.me], min(num, len(self._nodes) + 1))}

    def get_node(self, ipp: bytes) -> Node:
        return self._lookup_ipp[ipp]

    def has_node(self, ipp: bytes) -> bool:
        return ipp in self._lookup_ipp

    def _add_node(self, n: Node):
        """Add a node to the nodes list."""
        if self.synced:
            n.calc_distance()
            bisect.insort(self._nodes, n)
        else:
            self._nodes.append(n)
        self._lookup_ipp[n.ipp] = n

    def _remove_node(self, n):
        """Remove a node from the nodes list."""
        if self.synced:
            i = bisect.bisect_left(self._nodes, n)
            assert self._nodes[i] == n, "trying to remove a non-existent node that should have existed"
            del self._nodes[i]
        else:
            self._nodes.remove(n)
        del self._lookup_ipp[n.ipp]

    def _sort_nodes(self):
        """Recalculate node distances and sort the nodes list."""
        for n in self._nodes:
            n.calc_distance()
        self._nodes.sort()

    # user lists

    def add_user(self, n: User):
        if not n.nick:
            return

        nick = n.nick.lower()

        if nick in self._users:
            raise NickError("collision")

        if so := self.main.state_observer:
            # Might raise NickError
            so.event_AddNick(n)
            so.event_UpdateInfo(n)

        self._users[nick] = n

    def get_user(self, nick: str) -> User:
        return self._users[nick.lower()]

    def remove_user(self, n: User, reason: str):
        try:
            if self._users[n.nick.lower()] is not n:
                raise KeyError
        except KeyError:
            return

        del self._users[n.nick.lower()]

        if so := self.main.state_observer:
            so.event_RemoveNick(n, reason)

        # clean up nick-specific stuff
        if isinstance(n, Node):
            n.cancel_messages()

    def update_user(self, n: User, info: str):
        if not n.set_info(info):
            # dc_info hasn't changed, so there's nothing to send
            return

        try:
            if self._users[n.nick.lower()] is not n:
                raise KeyError
        except KeyError:
            return
        else:
            if so := self.main.state_observer:
                so.event_UpdateInfo(n)

    @property
    def user_list(self) -> Iterable[User]:
        return self._users.values()

    # sync operations

    async def sync_complete(self):

        # Unconfirmed nodes (without an expiration) can't exist once the
        # network is synced, so purge them from the nodes list.
        old_nodes = list(self._lookup_ipp.values())
        self._nodes = []
        self._lookup_ipp.clear()
        for n in old_nodes:
            if not n.expired:
                self._add_node(n)
            else:
                self.pgm.remove_outbound_link(n.ipp)
        self._sort_nodes()

        self.synced = True

        # make new connections to the closest nodes now that we have a complete set of online nodes
        self.pgm.schedule_make_new_links()

        # Tell observers to get the nick list, topic, etc.
        self.main.stateChange_DtellaUp()

        await self.main.show_login_status("Sync Complete; You're Online!", counter='inc')

    def refresh_node_status(self, packet: Union[PacketYR, PacketNS]) -> Node:
        assert packet.src_ipp != self.me.ipp, "should not be updating my own status with data from peers"

        try:
            n = self._lookup_ipp[packet.src_ipp]
            in_nodes = True
        except KeyError:
            n = Node(packet.src_ipp, self.main)
            in_nodes = False

        self.main.log_packet(f"Status: {Ad(packet.src_ipp)} {packet.expire} ({packet.nick})")

        # if session ID changed, remove and reinsert the node so that the list is sorted
        if self.synced and in_nodes and n.sesid != packet.sesid:
            self._remove_node(n)
            in_nodes = False

        # Update info
        n.status_pktnum = packet.pktnum
        n.sesid = packet.sesid
        n.uptime = seconds() - packet.uptime
        n.persist = packet.persist

        if packet.nick == n.nick:
            # Nick hasn't changed, just update info
            self.update_user(n, packet.info)

        else:
            # Nick has changed.

            # Remove old nick, if it's in there
            self.remove_user(n, "No DC client")

            # Run a sanity check on the new nick
            if packet.nick and validate_nick(packet.nick) != '':
                # Malformed
                n.set_no_user()

            else:
                # Good nick, update the info
                n.nick = packet.nick
                n.set_info(packet.info)

                # Try to add the new nick (no-op if the nick is empty)
                try:
                    self.add_user(n)
                except NickError:
                    n.set_no_user()

        # If n isn't in nodes list, then add it
        if not in_nodes:
            self._add_node(n)

        # Expire this node after the expected retransmit
        n.reschedule_timeout(packet.expire + NODE_EXPIRE_EXTEND)

        # Possibly make this new node an outgoing link
        self.pgm.schedule_make_new_links()

        # Return the node
        return n

    def node_exited(self, n: Node, reason):
        # Node n dropped off the network

        n.shutdown()
        self._remove_node(n)

        # Tell the TopicManager this node is leaving
        self.tm.checkLeavingNode(n)

        # Remove from the nick mapping
        self.remove_user(n, reason)
        n.set_no_user()

        # Remove from the SyncManager, if it's active
        if self.sm:
            self.sm.giveUpNode(n.ipp)

        # Remove from outbound links; find more if needed.
        if self.pgm.remove_outbound_link(n.ipp):
            self.pgm.schedule_make_new_links()

    def reset_session(self):
        """
        In the event that the session needs to be restarted (e.g. a node tried to tell the network I exited),
        this method will make a new session ID, re-sort nodes list, and make new connections.
        """
        self.me.sesid = os.urandom(4)
        self._sort_nodes()
        self.send_my_status()
        self.pgm.schedule_make_new_links()






    def updateMyInfo(self, send=False):
        from dtella.client.dc import DCHandler

        # Grab my info from the DC client (if any) and maybe broadcast
        # it into the network.

        me = self.me

        old_state = (me.nick, me.info_out, me.persist)

        me.persist = self.main.state.persistent

        if isinstance(dch := self.main.state_observer, DCHandler):
            me.info_out = dch.info
            nick = dch.nick
        else:
            me.info_out = "<%s>" % local.version_tag
            nick = ''

        if me.nick == nick:
            # Nick hasn't changed, just update info
            self.update_user(me, me.info_out)

        else:
            # Nick has changed

            # Remove old node, if I'm in there
            self.remove_user(me, "Removing Myself")

            # Set new info
            me.nick = nick
            me.set_info(me.info_out)

            # Add it back in, (no-op if my nick is empty)
            try:
                self.add_user(me)
            except NickError:
                # Nick collision.  Force the DC client to go invisible.
                # This will recursively call updateMyInfo with an empty nick.
                lines = [
                    "The nick <%s> is already in use on this network." % nick,
                    "Please change your nick, or type !REJOIN to try again."
                ]
                self.main.kickObserver(lines=lines, rejoin_time=None)
                return

        changed = (old_state != (me.nick, me.info_out, me.persist))

        if (send or changed) and self.synced:
            self.send_my_status()

    def send_my_status(self, send_full=True):
        """Immediately send my status, and keep sending updates over time."""

        async def _send_status(_send_full):
            # do a sanity check on the rate of status updates that I'm sending.
            # if other nodes are causing me to trigger 8 updates in 8 seconds, then something's amiss and shutdown
            now = seconds()
            if self._status_throttle[-1] < now - 8:
                self._status_throttle.clear()
            elif len(self._status_throttle) >= 8 and (self._status_throttle.pop(0) > now - 8):
                await self.main.show_login_status("*** YIKES! Too many status updates!")
                await self.main.shutdown(reconnect='max')
                return
            self._status_throttle.append(now)
            assert len(self._status_throttle) <= 8, "status throttle has too many events"
            self._next_status_update_time = now

            while True:
                expire = max(60.0, min(900.0, len(self.nodes))) * random.uniform(0.9, 1.1)
                if _send_full:
                    packet = PacketNS(nb_ipp=self.main.osm.me.ipp, hops=32, b_flags=0, src_ipp=self.me.ipp,
                                      pktnum=self.mrm.getPacketNumber_status(), expire=int(expire), sesid=self.me.sesid,
                                      uptime=int(seconds() - self.me.uptime), flags=self.me.flags, nick=self.me.nick,
                                      info=self.me.info_out)
                    _send_full = False
                else:
                    packet = PacketNH(nb_ipp=self.main.osm.me.ipp, hops=32, b_flags=0, src_ipp=self.me.ipp,
                                      pktnum=self.mrm.getPacketNumber_status(), expire=int(expire),
                                      info_hash=self.me.info_hash)

                self.mrm.new_message(packet, tries=8)
                self._next_status_update_time += expire
                await asyncio.sleep(expire)

        # skip this stuff for hidden nodes.
        if self.main.hide_node:
            return

        if self._status_task is not None:
            self._status_task.cancel()
        self._status_task = asyncio.create_task(_send_status(send_full))

    @property
    def next_status_update_time(self) -> float:
        return self._next_status_update_time

    @property
    def is_moderated(self):
        return False

    async def _login_echo_timeout(self):
        await self.main.ph.sendPacket(self._echo_packet, Ad(self.me.ipp))
        await asyncio.sleep(3.0)
        self._echo_timeout_task = None
        self._echo_packet = None
        LOG.warn("No EC response")

    def received_login_echo(self, ad: Ad, packet: PacketEC):

        self._echo_timeout_task.cancel()
        if packet.rand != self._echo_packet.rand:
            raise BadPacketError("EC rand mismatch")

        myad = Ad(self.me.ipp)

        if ad.ip == myad.ip:
            return

        if ad.is_private():
            # This matches an RFC1918 address, so it looks like a router.  Remap this address to my external IP in the
            # future
            self.main.ph.remap_ip = (ad.ip, myad.ip)
            self.main.log_packet(f"EC: Remap {ad}->{myad}")
        else:
            self.main.log_packet("EC: Not RFC1918")

    async def shutdown(self):

        if self._status_task is not None:
            self._status_task.cancel()

        # If I'm still syncing, shutdown the SyncManager
        if self.sm:
            self.sm.shutdown()

        # Shut down the MessageRoutingManager (and broadcast NX)
        if self.mrm:
            await self.mrm.shutdown()

        # Shut down all nodes
        for n in self.nodes:
            n.shutdown()

        # Shut down the PingManager (and notify outbounds)
        if self.pgm:
            await self.pgm.shutdown()

        # Shut down the SyncRequestRoutingManager
        if self.yqrm:
            self.yqrm.shutdown()
