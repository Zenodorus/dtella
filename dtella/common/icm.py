"""
Dtella - Initial Contact Manager
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import heapq
import time
from typing import Optional, Tuple, Dict, List, TYPE_CHECKING, Union, Set

from dtella.common import CODE_IP_OK, CODE_IP_FOREIGN, CODE_IP_BANNED, BadTimingError
from dtella.common.log import LOG
from dtella.common.packet import Packet, BadPacketError, build_packet, register_packet_classes, pk_enc
from dtella.util import Ad
from dtella.util.encoding import decode_array1, encode_array1

if TYPE_CHECKING:
    from asyncio import transports
    from asyncio.selector_events import _SelectorDatagramTransport

    from dtella.common.base import DtellaMain_Base


class PacketIQ(Packet):
    """
    Initialization Request Packet.  Sent to request node data from the receiver so that the sender may join the
    network.  A sender of this packet is a node that is trying to join the network.  The receiver is a node that is
    supposedly already on the network (based on the newcomer's state file or the dynamic config).

    Attributes:
        - code: the 2-letter string 'IQ'
        - dest_ip: the IP the packet is going to
        - src_port: the main dtella port number of the sender
    """

    _code = b'IQ'
    _data_model = [('code', '2s'), ('dest_ip', '4s'), ('src_port', 'H')]
    __slots__ = ("dest_ip", "src_port")
    dest_ip: bytes
    src_port: int


class PacketIC(Packet):
    """
    Initialization Cache Packet.  Response to the IQ packet containing a small cache of peers on the network.  This
    packet is sent back to the ICM port and *not* the main dtella port. A sender of this packet is a node that is
    already on the network.  The receiver is a node trying to join the network.

    Attributes:
        - code: the 2-letter string 'IC'
        - src_ipp: the IP:Port of the sender, reflected from the dest_ip of the IQ packet
        - my_ip: the IP of the receiver (as decoded from the UDP header of the IQ packet)
        - ip_code: IP validation response code
        - peer_cache: a list of at most 8 (ipp, age) tuples of the most recently active dtella nodes that the sender
            has seen
    """

    _code = b'IC'
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('my_ip', '4s'), ('ip_code', 'B'), ('peer_cache', '+6sI')]
    __slots__ = ("src_ipp", "my_ip", "ip_code", "peer_cache")
    _decode_array = decode_array1
    _encode_array = encode_array1
    src_ipp: bytes
    my_ip: bytes
    ip_code: int
    peer_cache: List[Tuple[bytes, int]]


class PacketIR(Packet):
    """
    Initialization Response Packet.  Response to the IQ packet containing a list of active dtella nodes on the
    network.  This packet is sent back to the main dtella port.  A sender of this packet is a node that is already
    on the network.  The receiver is a node trying to join the network.

    Attributes:
        - code: the 2-letter string 'IR'
        - src_ipp: the IP:Port of the sender, reflected from the dest_ip of the IQ packet
        - my_ip: the IP of the receiver (as decoded from the UDP header of the IQ packet)
        - ip_code: IP validation response code
        - node_list: a list of at most 48 (ipp, age) tuples of the "closest" dtella nodes
        - peer_cache: a list of (ipp, age) tuples of the most recently active dtella nodes that the sender has seen.

    Note:
    The combined number of addresses in node_list and peer_cache does not exceed 48, and there should not be any
    duplicate addresses between the node_list and peer_cache.
    """

    _code = b'IR'
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('my_ip', '4s'), ('ip_code', 'B'), ('node_list', '+6sI'),
                   ('peer_cache', '+6sI')]
    __slots__ = ("src_ipp", "my_ip", "ip_code", "node_list", "peer_cache")
    _decode_array = decode_array1
    _encode_array = encode_array1
    src_ipp: bytes
    my_ip: bytes
    ip_code: int
    node_list: List[Tuple[bytes, int]]
    peer_cache: List[Tuple[bytes, int]]


register_packet_classes(PacketIQ, PacketIC, PacketIR)


class _ICMContactTask(object):
    """
    Data structure to hold data about peers to be contacted.  This will be used in a list and sorted by heapq
    """

    STATUS_CODES = {"IQ": 1 << 0, "IC": 1 << 1, "IR": 1 << 2, "TIMEOUT": 1 << 3, "BAD_IP": 1 << 4}

    def __init__(self, ipp: bytes, seen: float):
        self._ipp: bytes = ipp
        self._seen: float = seen
        self._timeout_task: Optional[asyncio.Task] = None
        self._status: int = 0

        self.alt_reply: bool = False
        self.bad_code: bool = False

    @property
    def ipp(self) -> bytes:
        return self._ipp

    @property
    def seen(self) -> float:
        return self._seen

    def set_status(self, code: str):
        self._status |= _ICMContactTask.STATUS_CODES[code]

    def check_status(self, *codes: str) -> bool:
        return bool(self._status & sum(_ICMContactTask.STATUS_CODES[c] for c in codes))

    def update(self, seen: float) -> bool:
        """
        Update the last time this node was seen.  Return true if the node was updated.
        """
        if seen > self._seen:
            self._seen = seen
            return True
        return False

    def schedule_timeout(self, coro):
        self._timeout_task = asyncio.create_task(coro)

    def cancel_timeout(self):
        if self._timeout_task is not None:
            self._timeout_task.cancel()

    def __lt__(self, other: "_ICMContactTask"):
        return self._seen > other._seen

    def __le__(self, other: "_ICMContactTask"):
        return self._seen >= other._seen


class _ICMProtocol(asyncio.DatagramProtocol):

    def __init__(self, icm: "InitialContactManager"):
        self._icm = icm
        self._transport: Optional[transports.BaseTransport] = None
        self._last_received_time: float = 0

    def connection_made(self, transport: "transports.BaseTransport"):
        self._transport = transport
        self._last_received_time = time.time()

    @property
    def is_inactive(self) -> bool:
        return time.time() - self._last_received_time > 5

    def datagram_received(self, data: bytes, addr: Tuple[str, int]):
        """
        Process data received through the alternate port.  The only packet type that should be seen is the IC packet.
        """
        self._last_received_time = time.time()
        ad = Ad(addr)
        if not ad.port:
            return

        try:
            packet = build_packet(data)
            if not isinstance(packet, PacketIC):
                raise BadPacketError("expecting only IC packets on ICM port")
            LOG.debug(packet.as_string(src_ad=ad))
            self._icm.received_init_response(ad, packet)
        except ValueError as e:
            LOG.error(f"decrypt failed: {e}")
        except (BadPacketError, BadTimingError) as e:
            LOG.error(f"{e.__class__.__name__} on ICM port: {e}")

    # this is simply to make create_datagram_endpoint simpler without needing a lambda/partial
    def __call__(self):
        return self


class InitialContactManager(object):
    """
    Manager for finding nodes to hook into the Dtella network.  Scans through a list of known IP:Ports obtained from
    the dtella.state file and the dynamic configuration, sends a small ping to a bunch of them, collects addresses of
    known online peers, and eventually passes off the list back to the DtellaMainBase class where it is processed to
    determine if a connection to the network is viable.

    Attributes:
        - main: dtella main namespace
        - _running: whether or not the ICM has been started
        - _peers: dictionary of ipp bytes to potential nodes on the network
        - _uncontacted_heap: list of nodes yet to have an IQ packet sent to them
        - _IR_wait_peers: set of nodes for which an IQ was sent and now waiting for IR back
        - _node_ipps: set of IP:Ports that nodes who send OK IR packets back told us are good
        - _counters: tally of ip_codes from IC/IR packets
        - _transport: wrapper for outgoing socket
        - _protocol: wrapper for incoming socket
    """

    def __init__(self, main: "DtellaMain_Base"):
        self.main = main
        self._running = False

        self._peers: Dict[bytes, _ICMContactTask] = {}
        self._uncontacted_heap: List[_ICMContactTask] = []
        self._IR_wait_peers: Set[_ICMContactTask] = set()
        self._node_ipps: Set[bytes] = set()
        self._counters: Dict[str, int] = {}

        self._transport: Optional[_SelectorDatagramTransport] = None
        self._protocol: _ICMProtocol = _ICMProtocol(self)

    def __bool__(self):
        """Return true if the ICM is running."""
        return self._running

    async def run(self) -> Tuple[str, Optional[Set[bytes]]]:
        assert not self._running, "ICM already started"

        # initialize the ICM
        self._running = True
        self._peers = {ipp: _ICMContactTask(ipp, seen) for ipp, seen in self.main.state.peers.items()}
        self._uncontacted_heap = list(self._peers.values())
        heapq.heapify(self._uncontacted_heap)
        self._counters = {'good': 0, 'foreign_ip': 0, 'banned_ip': 0, 'dead_port': 0}

        await self.main.show_login_status("Scanning for online nodes...", counter=1)

        # listen on an arbitrary UDP port
        try:
            self._transport, _ = await asyncio.get_running_loop().create_datagram_endpoint(
                self._protocol, local_addr=('0.0.0.0', 0))
        except OSError:
            await self.main.show_login_status("Failed to bind alt UDP port")
            return 'no_nodes', None

        is_finished = result = False
        while not is_finished:
            # main loop
            # - pop a potential node, send an IQ packet, and schedule a timeout callback
            # - mark the node as waiting for reply
            # - sleep
            # - check conditions for whether or not ICM should continue
            try:
                p: _ICMContactTask = heapq.heappop(self._uncontacted_heap)
            except IndexError:
                # heap is empty
                pass
            else:
                ad = Ad(p.ipp)

                assert not p.check_status("IQ"), "node had an IQ packet already sent to it"
                p.set_status("IQ")

                packet = PacketIQ(dest_ip=ad.ip, src_port=self.main.state.udp_port)
                self.main.log_packet(packet.as_string(dst_ad=ad))
                self._transport.sendto(pk_enc.encrypt(packet), ad.get_addr_tuple())
                p.schedule_timeout(self._peer_timeout(p))
                self._IR_wait_peers.add(p)

            await asyncio.sleep(0.05)

            total = sum(self._counters.values())
            n_good = self._counters['good']

            if not self._uncontacted_heap and not self._IR_wait_peers:
                is_finished = True
            elif total >= 50:
                is_finished = True
            elif self._protocol.is_inactive:
                is_finished = True

            if is_finished:
                if total > 0 and n_good >= total * 0.10:
                    result = True
                else:
                    result = False
            elif n_good >= 5 and n_good >= total * 0.10:
                is_finished = result = True

        await self.shutdown()

        if result:
            return 'good', self._node_ipps
        else:
            # In a tie, prefer 'banned_ip' over 'foreign_ip', etc.
            rank = []
            i = 3
            for name in ('banned_ip', 'foreign_ip', 'dead_port'):
                rank.append((self._counters[name], i, name))
                i -= 1

            # Sort in descending order
            rank.sort(reverse=True)

            if rank[0][0] == 0:
                # Nobody replied
                return 'no_nodes', None
            else:
                return rank[0][2], None

    async def _peer_timeout(self, p: _ICMContactTask):
        """
        Timeout callback for making contact with a potential node.
        """
        await asyncio.sleep(5.0)
        assert not p.check_status("IR"), "IQ timeout called despite receiving IR packet"
        self._IR_wait_peers.remove(p)
        p.set_status("TIMEOUT")

        if p.check_status("IC"):
            # if IC was received and not IR, then main dtella port is probably not routing correctly
            self._counters['dead_port'] += 1

    def _update_peer(self, ipp, seen):
        if ipp not in self._peers:
            self._peers[ipp] = _ICMContactTask(ipp, seen)
            heapq.heappush(self._uncontacted_heap, self._peers[ipp])
        elif self._peers[ipp].update(seen) and not self._peers[ipp].check_status("IQ"):
            # if the peer has an update and was not contacted yet, bubble it up the heap
            # this takes O(n) and uses an undocumented heapq function...
            heapq._siftdown(self._uncontacted_heap, 0, self._uncontacted_heap.index(self._peers[ipp]))

    def received_init_response(self, udp_src_ad: Ad, packet: Union[PacketIC, PacketIR]):
        """

        :param udp_src_ad: source address as decoded from the UDP header
        :param packet:
        :return:
        """
        if udp_src_ad.is_private():
            if not self.main.auth('sx', udp_src_ad):
                raise BadPacketError("invalid reported source IP")
        else:
            self.main.check_source(packet.src_ipp, udp_src_ad, exempt_ip=True)

        if packet.ip_code not in (CODE_IP_OK, CODE_IP_FOREIGN, CODE_IP_BANNED):
            raise BadPacketError(f"{packet.code}: bad IP code")

        if not self._running:
            raise BadTimingError("not in initial connection mode")

        my_ad = Ad(packet.my_ip)
        src_ad = Ad(packet.src_ipp)
        LOG.debug(f"Init Response: myip={my_ad} code={packet.ip_code}")
        try:
            p = self._peers[packet.src_ipp]
        except KeyError:
            raise BadTimingError("did not ask for IC/IR response")

        if isinstance(packet, PacketIC):
            # ignore this packet if we already got an IC or an IR packet, or if it timed out
            if p.check_status("IC", "IR", "TIMEOUT"):
                return
            p.set_status("IC")
        else:
            if p not in self._IR_wait_peers:
                # wasn't waiting for this reply
                return
            self._IR_wait_peers.remove(p)
            p.cancel_timeout()
            p.set_status("IR")

        for ipp, age in packet.peer_cache:
            if self.main.state.refresh_peer(Ad(ipp), age):
                self._update_peer(ipp, time.time() - age)

        # Add my own IP to the list, even if it's banned.
        self.main.add_my_ip_report(src_ad, my_ad)

        if packet.ip_code != CODE_IP_OK:
            if not p.check_status("BAD_IP"):
                p.set_status("BAD_IP")

                if packet.ip_code == CODE_IP_FOREIGN:
                    self._counters['foreign_ip'] += 1

                elif packet.ip_code == CODE_IP_BANNED:
                    self._counters['banned_ip'] += 1

                if p in self._IR_wait_peers:
                    self._IR_wait_peers.remove(p)
                p.cancel_timeout()
            return

        # Add the node who sent this packet to the cache
        t = time.time()
        self.main.state.refresh_peer(src_ad, 0)
        self._update_peer(packet.src_ipp, t)

        # Add to set of currently online nodes
        if isinstance(packet, PacketIR):
            for ipp, age in packet.node_list:

                # Add to the peer cache
                self.main.state.refresh_peer(Ad(ipp), age)
                self._update_peer(ipp, t - age)

                # Add to set of probably-active nodes
                self._node_ipps.add(ipp)

            self._counters['good'] += 1

    async def shutdown(self):
        """
        Shutdown the ICM, cancelling all request timeouts and closing the ICM port.
        """
        for p in self._peers.values():
            p.cancel_timeout()
        if self._transport:
            self._transport.close()
        self._uncontacted_heap.clear()
        self._IR_wait_peers.clear()
        self._running = False
