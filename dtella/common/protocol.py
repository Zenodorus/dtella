"""
Dtella - Common Protocol
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import time
from asyncio import transports
from time import time as seconds
from typing import Optional, Tuple, Callable, TYPE_CHECKING

from dtella.common import (BadTimingError, Reject, NODE_EXPIRE_EXTEND, CODE_IP_FOREIGN, CODE_IP_BANNED, CODE_IP_OK)
from dtella.common.log import LOG
from dtella.common.icm import PacketIC, PacketIR, PacketIQ
from dtella.common.pgm import PacketPG, PacketPF
from dtella.common.sync import PacketYR, PacketYQ
from dtella.common.packet import (Packet, pk_enc, BroadcastPacket, build_packet, BadPacketError, REJECT_BIT,
                                  register_packet_classes, AckablePacket)
from dtella.util import Ad

if TYPE_CHECKING:
    from dtella.client.dc import PacketCA, PacketCP, PacketPM, PacketCH, PacketSQ
    from dtella.common.base import DtellaMain_Base
    from dtella.common.topic import PacketTP
    from dtella.common.osm import PacketEC
    from dtella.common.node import PacketNS, PacketNH, PacketNX, PacketNF, Node
    from dtella.common.state import StateManager


class BadBroadcast(Exception):
    pass


PKTNUM_BUF = 20

# Ack flags
ACK_REJECT_BIT = 0x1

# ACK Modes
ACK_PRIVATE = 1
ACK_BROADCAST = 2


class PacketAK(AckablePacket):

    _code = b"AK"
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('mode', 'B'), ('flags', 'B'), ('ack_key', '8s')]
    __slots__ = ('src_ipp', 'mode', 'flags')
    src_ipp: bytes
    mode: int
    flags: int

    def __init__(self, **kwargs):
        self.flags = 0x00
        super().__init__(**kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    reject = property(lambda self: PacketAK._get_flag(self, ACK_REJECT_BIT),
                      lambda self, v: PacketAK._set_flag(self, ACK_REJECT_BIT, v))


register_packet_classes(PacketAK)


class BaseProtocol(asyncio.DatagramProtocol):

    # Panic rate limit for broadcast traffic
    CHOKE_RATE = 100000   # bytes per second
    CHOKE_PERIOD = 5      # how many seconds to average over

    def __init__(self, main: "DtellaMain_Base"):
        self.main = main
        self._transport: Optional[transports.BaseTransport] = None
        self._remap_ip: Optional[Tuple[bytes, bytes]] = None

        self.choke_time = seconds() - self.CHOKE_PERIOD
        self.choke_reported = seconds() - 999

    def connection_made(self, transport: transports.BaseTransport):
        self._transport = transport

    async def sendPacket(self, packet: Packet, ad: Ad, broadcast=False):
        # Send a packet, passing it through the encrypter
        # returns False if an error occurs

        # if self.stopping_protocol:
        #     # Still cleaning up after a socket asplosion.
        #     return False

        self.main.log_packet(packet.as_string(dst_ad=ad))
        data = pk_enc.encrypt(packet.encode())

        # For broadcast traffic, set a safety limit on data rate,
        # in order to protect the physical network from DoS attacks.
        if isinstance(packet, BroadcastPacket):

            now = seconds()
            self.choke_time = max(self.choke_time, now - self.CHOKE_PERIOD)

            penalty = (1.0 * len(data) * self.CHOKE_PERIOD / self.CHOKE_RATE)

            # Have we used up the buffer time?
            if self.choke_time + penalty >= now:

                # Tell the user what's going on, but only once every
                # 10 seconds.
                if self.choke_reported < now - 10:
                    await self.main.show_login_status("!!! Dropping broadcast packets due to  excessive flood !!!")
                    self.choke_reported = now

                # Don't send packet
                return False

            # Nibble something off the choke buffer
            self.choke_time += penalty

            self.main.log_packet("choke=%f" % (now - (self.choke_time + penalty)))

        self._transport.sendto(data, ad.get_addr_tuple())
        return True

    async def send_AK_packet(self, ipp, mode, flags, ack_key):
        packet = PacketAK(src_ipp=self.main.osm.me.ipp, mode=mode, flags=flags, ack_key=ack_key)
        await self.main.ph.sendPacket(packet, Ad(ipp))

    def datagram_received(self, data: bytes, addr: Tuple[str, int]):
        ad = Ad(addr)
        if not ad.port:
            return

        # this will remap a router's internal IP to its external IP, if the remapping is known.
        if self._remap_ip and ad.ip == self._remap_ip[0]:
            ad.orig_ip = ad.ip
            ad.set_addr(self._remap_ip[1])

        self._process_packet(data, ad)

    def _process_packet(self, data: bytes, ad: Ad):
        try:
            packet = build_packet(data)
            LOG.debug(packet.as_string(src_ad=ad))
            if isinstance(packet, PacketIC):
                raise BadPacketError("not expecting IC packets on the main port")

            # Make sure the sender's IP is permitted, but delay the check if it's an initialize packet.
            if packet.code not in (b"IQ", b"IR", b"EC"):
                if not self.main.auth('sbx', ad):
                    raise BadPacketError("invalid source IP")

            if isinstance(packet, BroadcastPacket):
                if not self.main.osm:
                    raise BadTimingError(f"not ready to broadcast '{packet.code.decode()}' packet")
                self.handle_broadcast(ad, packet)
            else:
                try:
                    method: Callable[[Ad, Packet], None] = getattr(self, f"handle_packet_{packet.code.decode()}")
                except AttributeError:
                    raise BadPacketError(f"unknown kind: {packet.code.decode()}")
                else:
                    method(ad, packet)

        except ValueError as e:
            LOG.error(f"decrypt failed: {e}")
        except (BadPacketError, BadTimingError) as e:
            self.main.log_packet(f"Bad Packet/Timing: {e} <- {ad}")

    def handle_broadcast(self, ad: Ad, packet: BroadcastPacket, bridgey: bool = False):
        nb_ipp = packet.nb_ipp
        osm = self.main.osm

        # check IP validity
        self.main.check_source(nb_ipp, ad, exempt_ip=True)
        if not self.main.auth('sbx' if bridgey else 'sb', Ad(packet.src_ipp)):
            raise BadPacketError("invalid forwarded source IP")

        # make sure this came from one of my ping neighbors; this helps a little to prevent the injection of random
        # broadcast traffic into the network.
        try:
            if not osm.pgm.pnbs[nb_ipp].got_ack:
                raise KeyError
        except KeyError:
            raise BadTimingError("BroadcastPacket not from a ping neighbor")

        # if I'm supposed to send this message to the sender of this message, then send acknowledge and move on
        ack_key = packet.ack_key
        if osm.mrm.poke_message(ack_key, packet.nb_ipp):
            ak_packet = PacketAK(src_ipp=self.main.osm.me.ipp, mode=ACK_BROADCAST, flags=0, ack_key=ack_key)
            asyncio.create_task(self.sendPacket(ak_packet, Ad(packet.nb_ipp)))
            return

        ack_flags = 0
        # Get the source node object, if any
        try:
            src_n = osm.get_node(packet.src_ipp)
        except KeyError:
            src_n = None

        try:
            try:
                method: Callable[["Node", BroadcastPacket], None] = getattr(
                    self, f"check_broadcast_{packet.code.decode()}")
            except AttributeError:
                raise BadPacketError(f"unknown kind: {packet.code.decode()}")
            else:
                method(src_n, packet)

        except BadBroadcast as e:
            # cache the message and acknowledge
            self.main.log_packet("Bad Broadcast: %s" % str(e))
            osm.mrm.new_message(packet, tries=0, nb_ipp=packet.nb_ipp)
            ak_packet = PacketAK(src_ipp=self.main.osm.me.ipp, mode=ACK_BROADCAST, flags=0, ack_key=ack_key)
            asyncio.create_task(self.sendPacket(ak_packet, Ad(packet.nb_ipp)))
            return

        except Reject:
            # check_cb told us to reject this broadcast
            if packet.src_ipp == packet.nb_ipp:
                # If this is from a neighbor, just set the flag.
                # We'll send the ack later.
                ack_flags |= ACK_REJECT_BIT

            elif not (packet.b_flags & REJECT_BIT):
                # Not from a neighbor, so send a reject packet immediately.
                ak_packet = PacketAK(src_ipp=self.main.osm.me.ipp, mode=ACK_BROADCAST, reject=True, ack_key=ack_key)
                asyncio.create_task(self.sendPacket(ak_packet, Ad(packet.src_ipp)))

            # Set this flag to indicate to forwarded neighbors that we've
            # rejected the message.
            packet.b_flags |= REJECT_BIT

        if packet.hops > 0:
            packet.hops -= 1

            # Pass this message to MessageRoutingManager, so it will be
            # forwarded to all of my neighbors.
            osm.mrm.new_message(packet, tries=2, nb_ipp=packet.nb_ipp)

        # Ack the neighbor
        ak_packet = PacketAK(src_ipp=self.main.osm.me.ipp, mode=ACK_BROADCAST, flags=ack_flags, ack_key=ack_key)
        asyncio.create_task(self.sendPacket(ak_packet, Ad(packet.nb_ipp)))

        # Update the original sender's age in the peer cache
        self.main.state.refresh_peer(Ad(packet.src_ipp), 0)

    def handle_packet_IQ(self, ad: Ad, packet: PacketIQ):
        # TODO refactor, clean up, and document
        # Initialization Request; someone else is trying to get online

        if packet.src_port == 0:
            raise BadPacketError("Zero Port")

        osm = self.main.osm
        state: "StateManager" = self.main.state

        # my_ad is how the address the source believes I am at; src_ad is where the UDP header says it came from
        my_ad = Ad((packet.dest_ip, self.main.state.udp_port))
        if ad.is_private() and self.main.auth('sx', my_ad):
            # if the request came from a private IP address, but was sent toward a public IP address, then assume the
            # sender node also has the same public IP address.
            src_ad = Ad((packet.dest_ip, packet.src_port))
        else:
            src_ad = Ad((ad.ip, packet.src_port))

        if not self.main.auth('sx', src_ad):
            ip_code = CODE_IP_FOREIGN
        elif not self.main.auth('b', src_ad):
            ip_code = CODE_IP_BANNED
        else:
            ip_code = CODE_IP_OK

        packet_ic = PacketIC(src_ipp=bytes(my_ad), my_ip=src_ad.ip, ip_code=ip_code)
        packet_ir = PacketIR(src_ipp=bytes(my_ad), my_ip=src_ad.ip, ip_code=ip_code)

        # Provide a max of 48 addresses in a normal response,
        # 8 addresses in a little cache response
        ir_len, ic_len = 48, 8

        # Lists of stuff
        node_ipps = set()
        if ip_code != CODE_IP_OK:
            # for invalid IPs, send no neighbors, and a small peer cache just so they can try for a second opinion
            ir_len = ic_len
        elif osm and osm.synced:
            node_ipps = osm.random_ipp_sample(ir_len)
        elif osm:
            # not synced yet, don't add any online nodes
            pass
        elif self.main.accept_IQ_trigger and self.main.auth('sx', my_ad):
            # if we've recently failed to connect, then go online as the sole node on the network
            # report our node ipp so this other node can try to join us.

            self.main.add_my_ip_report(src_ad, my_ad)
            # TODO this needs to be awaited
            self.main.start_osm(set())
            osm = self.main.osm
            node_ipps = {osm.me.ipp}

        # Get my own IPP (if I know it)
        my_ipp = osm.me.ipp if osm else None
        now = time.time()

        def get_age(_ipp) -> int:
            if _ipp == my_ipp:
                return 0
            elif (last_seen := state.peers.get(_ipp, None)) is not None:
                return int(max(now - last_seen, 0))
            else:
                return 3600

        packet_ir.node_list = [(ipp, get_age(ipp)) for ipp in node_ipps]

        # fill out the peer caches with the youngest peers in our cache
        entries = [(ipp, max(int(now - when), 0)) for when, ipp in state.get_youngest_peers(ir_len)]
        packet_ic.peer_cache = entries[:ic_len]
        packet_ir.peer_cache = [(ipp, age) for ipp, age in entries if ipp not in node_ipps][:ir_len - len(node_ipps)]

        # Send IC packet to alternate port, undo NAT remapping
        asyncio.create_task(self.sendPacket(packet_ic, ad.remap()))

        # Send IR packet to dtella port
        asyncio.create_task(self.sendPacket(packet_ir, src_ad))

        # Update the sender in my peer cache (if valid)
        self.main.state.refresh_peer(src_ad, 0)

    def handle_packet_IR(self, ad: Ad, packet: PacketIR):
        # TODO document
        if self.main.icm:
            self.main.icm.received_init_response(ad, packet)

    def handle_packet_PG(self, ad: Ad, packet: PacketPG):
        # TODO clean up and document
        osm = self.main.osm
        if not osm:
            raise BadTimingError("not ready to receive pings yet")

        self.main.check_source(packet.src_ipp, ad, exempt_ip=True)
        packet.nbs = [ipp for ipp in packet.nbs if self.main.auth('sx', Ad(ipp))]

        osm.pgm.received_ping(packet)

    def handle_packet_PF(self, ad: Ad, packet: PacketPF):
        # TODO clean up and document
        # Direct: Possible Falure (precursor to NF)

        osm = self.main.osm

        if not (osm and osm.synced):
            raise BadTimingError("not ready for PF")

        self.main.check_source(packet.nb_ipp, ad, exempt_ip=True)

        try:
            n = osm.get_node(packet.dead_ipp)
        except KeyError:
            raise BadTimingError("PF received for not-online node")

        if n.sesid != packet.sesid:
            raise BadTimingError("PF has the wrong session ID")

        if self.is_outdated_status(n, packet.pktnum):
            raise BadTimingError("PF is outdated")

        osm.pgm.handle_node_failure(n.ipp, packet.nb_ipp)

    def handle_packet_EC(self, ad: Ad, packet: "PacketEC"):
        # TODO document
        if not self.main.osm:
            raise BadTimingError("not ready for login echo")
        self.main.osm.received_login_echo(ad, packet)

    def handlePacket_YQ(self, ad: Ad, packet: PacketYQ):
        # Sync Request

        osm = self.main.osm
        if not (osm and osm.synced):
            raise BadTimingError("not ready to handle a sync request")

        # hidden nodes shouldn't be getting sync requests.
        if self.main.hide_node:
            raise BadTimingError("hidden node can't handle sync requests.")

        self.main.check_source(packet.nb_ipp, ad, exempt_ip=True)

        if not self.main.auth('sbx', Ad(packet.src_ipp)):
            raise BadPacketError("invalid source IP")

        if packet.hops > 2:
            raise BadPacketError("bad hop count")
        elif packet.hops == 2 and packet.src_ipp != packet.nb_ipp:
            raise BadPacketError("source ip mismatch")

        osm.yqrm.received_sync_request(packet)

    def handle_packet_YR(self, ad: Ad, packet: PacketYR):
        # TODO clean up and document
        osm = self.main.osm
        if not (osm and osm.sm):
            raise BadTimingError("not ready for sync reply")

        try:
            n = osm.get_node(packet.src_ipp)
        except KeyError:
            n = None

        self.main.check_source(packet.src_ipp, ad)
        packet.c_nbs = [ipp for ipp in packet.c_nbs if self.main.auth('sx', Ad(ipp))]
        packet.u_nbs = [ipp for ipp in packet.u_nbs if self.main.auth('sx', Ad(ipp))]

        if not (5 <= packet.expire <= 30*60):
            raise BadPacketError("expire time out of range")

        # Check for outdated status, in case an NS already arrived.
        if not self.is_outdated_status(n, packet.pktnum):
            n = osm.refresh_node_status(packet)

        if packet.topic:
            osm.tm.receivedSyncTopic(n, packet.topic)

        osm.sm.receivedSyncReply(packet.src_ipp, packet.c_nbs, packet.u_nbs)

    def handle_packet_AK(self, ad: Ad, packet: PacketAK):
        # TODO clean up and document
        osm = self.main.osm
        if not osm:
            raise BadTimingError("not ready for AK packet")

        self.main.check_source(packet.src_ipp, ad, exempt_ip=True)

        if packet.mode == ACK_PRIVATE:
            # Handle a private message ack

            if not osm.synced:
                raise BadTimingError("not ready for PM AK packet")

            try:
                n = osm.get_node(packet.src_ipp)
            except KeyError:
                raise BadTimingError("AK: unknown PM ACK node")
            else:
                n.received_private_message_ack(packet.ack_key, packet.reject)

        elif packet.mode == ACK_BROADCAST:
            # Handle a broadcast ack

            if osm.synced and packet.reject:
                osm.mrm.receivedRejection(packet.ack_key, packet.src_ipp)

            # Tell MRM to stop retransmitting message to this neighbor
            osm.mrm.poke_message(packet.ack_key, packet.src_ipp)

        else:
            raise BadPacketError("unknown AK mode")

    def handle_packet_CA(self, ad: Ad, packet: "PacketCA"):
        raise NotImplementedError()

    def handle_packet_CP(self, ad: Ad, packet: "PacketCP"):
        raise NotImplementedError()

    def handle_packet_PM(self, ad: Ad, packet: "PacketPM"):
        raise NotImplementedError()

    def check_broadcast_NS(self, src_n: "Node", packet: "PacketNS"):

        if not (5 <= packet.expire <= 30*60):
            raise BadPacketError("expire time out of range")
        elif self.isMyStatus(packet.src_ipp, packet.pktnum, sendfull=True):
            raise BadBroadcast("impersonating me")
        elif self.is_outdated_status(src_n, packet.pktnum):
            raise BadBroadcast("outdated")

        n = self.main.osm.refresh_node_status(packet)

        # They had a nick, now they don't.  This indicates a problem. Stop forwarding and notify the user.
        if packet.nick and not n.nick:
            raise Reject

    def check_broadcast_NH(self, src_n: "Node", packet: "PacketNH"):
        # Broadcast: Node Status Hash (keep-alive)

        osm = self.main.osm

        if not (5 <= packet.expire <= 30*60):
            raise BadPacketError("expire time out of range")

        # Make sure this isn't about me
        if self.isMyStatus(packet.src_ipp, packet.pktnum, sendfull=True):
            raise BadBroadcast("impersonating me")

        if self.is_outdated_status(src_n, packet.pktnum):
            raise BadBroadcast("outdated")

        if osm.synced:
            if src_n and src_n.info_hash == packet.info_hash:
                # We are synced, and this node matches, so extend the
                # expire timeout and keep forwarding.
                src_n.status_pktnum = packet.pktnum
                src_n.reschedule_timeout(packet.expire + NODE_EXPIRE_EXTEND)
                return

            else:
                # Syncd, and we don't recognize it
                raise Reject

        else:
            if not (src_n and src_n.expired):
                # Not synced, don't know enough about this node yet,
                # so just forward blindly.
                return

            elif src_n.info_hash == packet.info_hash:
                # We know about this node already, and the info_hash
                # matches, so extend timeout and keep forwarding
                src_n.status_pktnum = packet.pktnum
                src_n.reschedule_timeout(packet.expire + NODE_EXPIRE_EXTEND)
                return

            else:
                # Not synced, but we know the info_hash is wrong.
                raise Reject

    def check_broadcast_NX(self, src_n: "Node", packet: "PacketNX"):

        osm = self.main.osm

        if osm.synced:
            if packet.src_ipp == osm.me.ipp and packet.sesid == osm.me.sesid:
                # Yikes! Make me a new session id and rebroadcast it.
                osm.reset_session()
                raise BadBroadcast("tried to exit me")

            if not src_n:
                raise BadBroadcast("node not online")

            if packet.sesid != src_n.sesid:
                raise BadBroadcast("wrong session ID")

        elif not src_n:
            # Not synced, and haven't seen this node yet.  Forward blindly
            return

        # Remove node
        osm.node_exited(src_n, "Received NX")

    def check_broadcast_NF(self, src_n: "Node", packet: "PacketNF"):

        # make sure this isn't about me
        if self.isMyStatus(packet.src_ipp, packet.pktnum, sendfull=False):
            raise BadBroadcast("I'm not dead!")

        if not (src_n and src_n.expired):
            raise BadBroadcast("nonexistent node")

        if src_n.sesid != packet.sesid:
            raise BadBroadcast("wrong session ID")

        if self.is_outdated_status(src_n, packet.pktnum):
            raise BadBroadcast("outdated")

        # reduce the expiration time; if that node isn't actually dead, it will rebroadcast a status update to
        # correct it
        if src_n.timeout_left > NODE_EXPIRE_EXTEND:
            src_n.reschedule_timeout(NODE_EXPIRE_EXTEND)

    def check_broadcast_CH(self, src_n: "Node", packet: "PacketCH"):
        from dtella.client.dc import DCHandler

        osm = self.main.osm

        if packet.src_ipp == osm.me.ipp:
            # Possibly a spoofed chat from me
            if packet.nhash == osm.me.nick_hash:
                if isinstance(dch := self.main.state_observer, DCHandler):
                    dch.push_Status(f"*** Chat spoofing detected: {packet.text}")
            raise BadBroadcast("Spoofed chat")

        if not osm.synced:
            # Not synced, forward blindly
            return

        if osm.is_moderated:
            # Note: this may desync the sender's chat_pktnum, causing
            # their next valid message to be delayed by 2 seconds, but
            # it's better than broadcasting useless traffic.
            raise BadBroadcast("Chat is moderated")

        elif src_n and packet.nhash == src_n.nick_hash:
            src_n.add_message(src_n.nick, packet.pktnum, packet.text, packet.flags)

        else:
            raise Reject

    def check_broadcast_TP(self, src_n: "Node", packet: "PacketTP"):
        from dtella.client.dc import DCHandler

        osm = self.main.osm

        if packet.src_ipp == osm.me.ipp:
            # Possibly a spoofed topic from me
            if packet.nhash == osm.me.nick_hash:
                if isinstance(dch := self.main.state_observer, DCHandler):
                    dch.push_Status(f"*** Topic spoofing detected: {packet.topic}")
            raise BadBroadcast("spoofed topic")

        if not osm.synced:
            # Not synced, forward blindly
            return None

        if src_n and packet.nhash == src_n.nick_hash:
            osm.tm.gotTopic(src_n, packet.topic)

        else:
            raise Reject

    def check_broadcast_SQ(self, src_n: "Node", packet: "PacketSQ"):
        osm = self.main.osm

        if packet.src_ipp == osm.me.ipp:
            raise BadBroadcast("spoofed search")
        elif not osm.synced:
            # not synced, forward blindly
            return
        elif not src_n:
            # from an invalid node
            raise Reject

    def isMyStatus(self, src_ipp: bytes, pktnum: int, sendfull: bool):
        # This makes corrections to any stray messages on the network that
        # would have an adverse effect on my current state.

        osm = self.main.osm

        # If it's not for me, nothing's wrong.
        if src_ipp != osm.me.ipp:
            return False

        # If it's old, ignore it.
        if 0 < (osm.me.status_pktnum - pktnum) % 0x100000000 < PKTNUM_BUF:
            self.main.log_packet("Outdated from-me packet")
            return True

        # If it's from my near future, then repair my packet number
        if 0 < (pktnum - osm.me.status_pktnum) % 0x100000000 < 2 * PKTNUM_BUF:
            osm.me.status_pktnum = pktnum

        # If I'm synced, retransmit my status
        if osm.synced:
            self.main.log_packet("Reacting to an impersonated status")
            osm.send_my_status(sendfull)

        return True

    def is_outdated_status(self, n: "Node", pktnum: int):
        # This prevents a node's older status messages from taking
        # precedence over newer messages.

        if n is None:
            # Node doesn't exist, can't be outdated
            return False

        if n.status_pktnum is None:
            # Don't have a pktnum yet, can't be outdated
            return False

        if 0 < (n.status_pktnum - pktnum) % 0x100000000 < PKTNUM_BUF:
            self.main.log_packet("Outdated Status")
            return True

        return False

    # this is simply to make create_datagram_endpoint simpler without needing a lambda
    def __call__(self):
        return self
