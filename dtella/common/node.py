"""
Dtella - Node Data
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import os
import struct
import time
from hashlib import md5
from time import time as seconds
from typing import Optional, Dict, Tuple, TYPE_CHECKING, List, Union

from dtella.common import PERSIST_BIT, NODE_EXPIRE_EXTEND
from dtella.common.packet import BroadcastPacket, register_packet_classes, AckablePacket
from dtella.util import Ad, parse_dtella_tag
from dtella.util.scheduled_task import ScheduledTask

if TYPE_CHECKING:
    from dtella.client.dc import PacketPM, PacketCA, PacketCP
    from dtella.common.base import DtellaMain_Base


class PacketNS(BroadcastPacket):
    """
    Node Status Packet.
    """

    _code = b"NS"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'I'), ('expire', 'H'), ('sesid', '4s'), ('uptime', 'I'), ('flags', 'B'), ('nick', 'S1'),
                   ('info', 'S1')]
    _max_extra = 1024
    __slots__ = ('pktnum', 'expire', 'sesid', 'uptime', 'flags', 'nick', 'info')
    pktnum: int
    expire: int
    sesid: bytes
    uptime: int
    flags: int
    nick: str
    info: str

    def __init__(self, **kwargs):
        self.flags = 0x00
        super().__init__(**kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    persist = property(lambda self: PacketNS._get_flag(self, PERSIST_BIT),
                       lambda self, v: PacketNS._set_flag(self, PERSIST_BIT, v))


class PacketNH(BroadcastPacket):
    """
    Node Status Hash Packet (keep alive packet).
    """

    _code = b"NH"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'I'), ('expire', 'H'), ('info_hash', '4s')]
    __slots__ = ('pktnum', 'expire', 'info_hash')
    pktnum: int
    expire: int
    info_hash: bytes


class PacketNX(BroadcastPacket):
    """
    Node Exiting Packet.
    """

    _code = b"NX"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('sesid', '4s')]
    __slots__ = ('sesid',)
    sesid: bytes


class PacketNF(BroadcastPacket):
    """
    Node Failure Packet.
    """

    _code = b"NF"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'I'), ('sesid', '4s')]
    __slots__ = ('pktnum', 'sesid')
    pktnum: int
    sesid: bytes


register_packet_classes(PacketNS, PacketNH, PacketNX, PacketNF)


class User(object):
    """
    Base class to represent a person on the network.
    """

    def __init__(self):
        self.nick: str = ''
        self.dc_info: str = ''

    # properties

    def set_info(self, info: str) -> bool:
        """Verify info is well-formed before setting it.  Returns true if info changed."""

        old_info = self.dc_info
        info = info.replace('\r', '').replace('\n', '').split('$')
        if len(info) != 6:
            self.dc_info = ""
            return old_info != self.dc_info

        # TODO drop this nonsense
        # make sure suffix is no more than 8 characters, if it exists
        try:
            location, suffix = info[2][:-1].split('|', 1)
            suffix = suffix[:8]
        except ValueError:
            pass
        else:
            info[2] = f"{location}|{suffix}" + info[2][-1]

        self.dc_info = '$'.join(info)
        return old_info != self.dc_info

    def set_no_user(self):
        self.dc_info = ""

    @property
    def location(self) -> str:
        if not self.dc_info:
            return ""
        return self.dc_info.split('$', 3)[2][:-1].split('|', 1)[0]

    @property
    def shared(self) -> int:
        try:
            return int(self.dc_info.split('$', 5)[4])
        except ValueError:
            return 0

    @property
    def dttag(self) -> str:
        if not self.dc_info:
            return ""
        return parse_dtella_tag(self.dc_info)

    # event handlers

    async def event_PrivateMessage(self, packet: "PacketPM") -> Optional[str]:
        """Handler for when a private message is to be sent to this user."""
        raise NotImplementedError()

    async def event_ConnectToMe(self, packet: "PacketCA") -> Optional[str]:
        """Handler for when a ConnectToMe is to be sent to this user."""
        raise NotImplementedError()

    async def event_RevConnectToMe(self, packet: "PacketCP") -> Optional[str]:
        """Handler for when a RevConnectToMe is to be sent to this user."""
        raise NotImplementedError()


class ChatSequencedMixin(object):

    # How far forward/back to accept messages
    FUZZ = 10

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._chat_queue: List[Union[Tuple[str, str, int], float]] = []
        self._chat_queue_pktnum: Optional[int] = None
        self._chatq_dcall = None

        self._chat_queue_task: asyncio.Task = asyncio.create_task(self._advance_queue())
        self._chat_queue_lock: asyncio.Event = asyncio.Event()

    def add_message(self, nick: str, pktnum: int, text: str, flags: int):

        msg = (nick, text, flags)

        if self._chat_queue_pktnum is None:
            self._chat_queue_pktnum = pktnum

        # find the pktnum index relative to the current base.
        idx = ((pktnum - self._chat_queue_pktnum + ChatSequencedMixin.FUZZ) % 0x100000000) - ChatSequencedMixin.FUZZ

        if idx < 0:
            # older message, send out of order
            self._send_message(*msg)

        elif idx >= ChatSequencedMixin.FUZZ:
            # way out there; put this at the end and dump everything
            self._chat_queue.append(msg)
            self.flush_queue()

        else:
            # from the near future: (0 <= idx < FUZZ)

            # make sure the queue is big enough; put a timestamp in the empty spaces
            extra = (idx - len(self._chat_queue)) + 1
            if extra > 0:
                self._chat_queue.extend([time.monotonic()] * extra)

            # Insert the current message into its space
            if type(self._chat_queue[idx]) is float:
                self._chat_queue[idx] = msg

            # Possible spoof?
            # Don't know which one's real, so flush the queue and move on.
            elif self._chat_queue[idx] != msg:
                self._chat_queue.insert(idx + 1, msg)
                self.flush_queue()
                return

            # notify the queue was updated
            self._chat_queue_lock.set()

    async def _advance_queue(self):
        """
        Chat message queue consumer.  Unlocks whenever add_message updates a message in the queue.  The consumer
        cycles between two modes: send messages and waiting.  The first mode continues as long as the first item in
        the queue is a message, in which case the queue immediately flushes.  If the first item is a float,
        then the consumer waits for a timeout or receives an update from add_message.  If a timeout occurs,
        flush all floats until the next message on the queue and start over.  If an update occurs, start back in
        message mode.
        """

        def _flush_messages():
            """Flush messages.  Return true if queue is empty."""
            try:
                while not isinstance(msg := self._chat_queue[0], float):
                    self._chat_queue.pop(0)
                    self._chat_queue_pktnum = (self._chat_queue_pktnum + 1) % 0x100000000
                    self._send_message(*msg)
                return False
            except IndexError:
                return True

        def _flush_floats():
            """Flush floats.  Return true if the queue is empty."""
            try:
                while isinstance(self._chat_queue[0], float):
                    self._chat_queue.pop(0)
                    self._chat_queue_pktnum = (self._chat_queue_pktnum + 1) % 0x100000000
                return False
            except IndexError:
                return True

        while await self._chat_queue_lock.wait():
            if not self._chat_queue:
                self._chat_queue_lock.clear()
                continue
            elif _flush_messages():
                continue

            # nonempty queue where the next message is a float
            wait = max(self._chat_queue[0] + 2.0 - time.monotonic(), 0)
            await asyncio.wait_for(self._chat_queue_lock.wait(), timeout=wait)
            if self._chat_queue_lock.is_set():
                continue
            _flush_floats()

    def flush_queue(self):
        """Send all the messages in the queue, in order."""
        for msg in self._chat_queue:
            if type(msg) is not float:
                self._send_message(*msg)
        self.clear_queue()

    def clear_queue(self):
        """Reset the chat queue."""
        self._chat_queue.clear()
        self._chat_queue_pktnum = None

    def _send_message(self, nick: str, text: str, flags: int):
        raise NotImplementedError()

    def shutdown(self):
        self._chat_queue_task.cancel()


class Node(ChatSequencedMixin, User):
    """
    Data structure that represents a node on the network.

    Attributes:
        _ipp: IP:Port of the node
        main: main Dtella namespace
        sesid: session ID, unique to this node
        _dist: distance between this node and me

        _timeout_time: timestamp for when this node expires
        _timeout_task: asyncio.Task to manage the timeout
    """

    # For statistics  (bridge nicks are False)
    is_peer = True

    def __init__(self, ipp: bytes, main: "DtellaMain_Base"):
        super().__init__()

        # Dtella Tracking stuff
        self._ipp: bytes = ipp
        self.main: "DtellaMain_Base" = main

        self.sesid: bytes = b''
        self._dist: bytes = b''
        self.info_hash: Optional[bytes] = None
        self.persist: bool = False

        self._timeout_task: ScheduledTask = ScheduledTask(self._node_timeout(), delay=NODE_EXPIRE_EXTEND)
        self._rc_window_task: Optional[ScheduledTask] = None

        self.status_pktnum = None

        # ack_key -> timeout DelayedCall
        self.msgkeys_out: Dict[bytes, Tuple[asyncio.Task, asyncio.Future[bool]]] = {}
        self.msgkeys_in: Dict[bytes, ScheduledTask] = {}

        self.uptime = 0.0

    # properties

    @property
    def ipp(self) -> bytes:
        return self._ipp

    @property
    def nick_hash(self) -> Optional[bytes]:
        """Creates a 4-byte hash to prevent transient nick mismapping."""
        return md5(self.ipp + self.sesid + self.nick.encode()).digest()[:4] if self.nick else None

    @property
    def flags(self) -> int:
        return int(self.persist) & PERSIST_BIT

    def set_info(self, info: str) -> bool:

        if self.sesid is None:
            # Node is uninitialized
            self.info_hash = None
        else:
            flags = struct.pack('!B', self.flags)
            self.info_hash = md5(self.sesid + flags + self.nick.encode() + b'|' + info.encode()).digest()[:4]

        return super().set_info(info)

    def set_no_user(self):
        # Wipe out the nick, and set info to contain only a Dt tag.
        self.nick = ""
        if self.dttag:
            self.set_info(f"<{self.dttag}>")
        else:
            self.dc_info = ""

    # sorting

    def __lt__(self, other):
        if isinstance(other, Node):
            return self._dist < other._dist
        raise NotImplemented

    def __le__(self, other):
        if isinstance(other, Node):
            return self._dist <= other._dist
        raise NotImplemented

    # timeout

    async def _node_timeout(self):
        """Shut down this node if we haven't received any status updates recently."""
        self.main.osm.node_exited(self, "Node Timeout")

    def reschedule_timeout(self, duration):
        """Resets the time at which this node times out."""
        self._timeout_task.reset(duration)

    @property
    def timeout_left(self) -> float:
        return self._timeout_task.remaining

    @property
    def expired(self) -> bool:
        return self._timeout_task.done

    # utility

    def calc_distance(self):
        """
        Compute the distance between this node and me using a pseudo-random method.  This should only be called if
        we are synced with the network.
        """

        my_key = self.main.osm.me.ipp + self.main.osm.me.sesid
        nb_key = self.ipp + self.sesid
        data = (my_key + nb_key) if my_key <= nb_key else (nb_key + my_key)
        self._dist = md5(data).digest()

    # messaging

    def generate_ack_key(self):
        while (ack_key := os.urandom(8)) in self.msgkeys_out:
            pass
        return ack_key

    def poke_pm_key(self, ack_key: bytes):
        try:
            self.msgkeys_in[ack_key].reset(60.0)
            return False
        except KeyError:
            async def _pop():
                del self.msgkeys_in[ack_key]
            self.msgkeys_in[ack_key] = ScheduledTask(_pop(), delay=60.0)
            return True

    async def send_private_message(self, packet: "AckablePacket") -> Optional[str]:
        """Send an ACK-able direct message to this node.  Returns None if successful."""

        ack_key = packet.ack_key = self.generate_ack_key()

        async def _send_message():
            lock = self.msgkeys_out[ack_key][1]

            for _ in range(3):
                await self.main.ph.sendPacket(packet, Ad(self._ipp))
                await asyncio.wait_for(lock, timeout=1.0)
                if lock.done():
                    break

            if not lock.done():
                return "Timeout"
            elif lock.result():
                return "Rejected"

        self.msgkeys_out[ack_key] = (asyncio.create_task(_send_message()), asyncio.Future())
        return await self.msgkeys_out[ack_key][0]

    def received_private_message_ack(self, ack_key, reject):
        """Got an ACK for a private message."""
        try:
            self.msgkeys_out[ack_key][1].set_result(reject)
        except KeyError:
            return

    async def event_PrivateMessage(self, packet: "PacketPM") -> Optional[str]:
        osm = self.main.osm
        packet.src_ipp = osm.me.ipp
        packet.src_nhash = osm.me.nick_hash
        packet.dst_nhash = self.nick_hash
        return await self.send_private_message(packet)

    async def event_ConnectToMe(self, packet: "PacketCA") -> Optional[str]:
        osm = self.main.osm
        packet.src_ipp = osm.me.ipp
        packet.src_nhash = osm.me.nick_hash
        packet.dst_nhash = self.nick_hash
        return await self.send_private_message(packet)

    async def event_RevConnectToMe(self, packet: "PacketCP") -> Optional[str]:
        osm = self.main.osm
        packet.src_ipp = osm.me.ipp
        packet.src_nhash = osm.me.nick_hash
        packet.dst_nhash = self.nick_hash
        return await self.send_private_message(packet)

    def _send_message(self, nick: str, text: str, flags: int):
        if so := self.main.state_observer:
            so.event_ChatMessage(self, nick, text, flags)

    def cancel_messages(self):
        """Cancel all pending private message timeouts."""
        for task in self.msgkeys_in.values():
            task.cancel()
        for task, _ in self.msgkeys_out.values():
            task.cancel()
        self.msgkeys_in.clear()
        self.msgkeys_out.clear()

        # TODO
        # # Bridge stuff
        # if osm.bsm:
        #     osm.bsm.nickRemoved(self)

    # RevConnect handling

    def open_RevConnect_window(self):
        """
        Upon receiving RevConnect, create a 5 second window during which errors are suppressed for outgoing connects.
        """

        if self._rc_window_task is not None:
            self._rc_window_task.reset(5.0)
        else:
            self._rc_window_task = ScheduledTask(asyncio.sleep(0), delay=5.0)

    def poke_RevConnect_window(self):
        """If the RevConnect window is open, close it and return True."""

        if self._rc_window_task is not None and not self._rc_window_task.done:
            self._rc_window_task.cancel()
            self._rc_window_task = None
            return True
        else:
            return False

    def shutdown(self):
        super().shutdown()

        self.cancel_messages()
        self._timeout_task.cancel()
        if self._rc_window_task is not None:
            self._rc_window_task.cancel()


class DtellaNodeMe(Node):

    info_out = ""

    def __init__(self, ipp: bytes, main: "DtellaMain_Base"):
        super().__init__(ipp, main)
        self.sesid = os.urandom(4)
        self.uptime = seconds()

    async def event_PrivateMessage(self, packet: "PacketPM") -> Optional[str]:
        """Handler for when a private message is to be sent to this user."""
        from dtella.client.dc import DCHandler

        if isinstance(dch := self.main.state_observer, DCHandler):
            await dch.push_PrivMsg(dch.nick, packet.text)
        else:
            return "I'm not online!"

    async def event_ConnectToMe(self, packet: "PacketCA") -> Optional[str]:
        """Handler for when a ConnectToMe is to be sent to this user."""
        return "can't get files from yourself!"

    async def event_RevConnectToMe(self, packet: "PacketCP") -> Optional[str]:
        """Handler for when a RevConnectToMe is to be sent to this user."""
        return "can't get files from yourself!"