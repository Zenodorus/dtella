"""
Dtella - Sync Manager
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import random
import time
from typing import TYPE_CHECKING, Dict, Optional, List, Tuple

from dtella.common import PERSIST_BIT
from dtella.common.packet import Packet, BroadcastPacket, register_packet_classes
from dtella.util import RandSet, Ad, CHECK
from dtella.util.encoding import encode_array1, decode_array1
from dtella.util.scheduled_task import ExpiringDict

if TYPE_CHECKING:
    from dtella.common.base import DtellaMain_Base

# Sync Flags
TIMED_OUT_BIT = 0x1


class PacketYQ(BroadcastPacket):

    _code = b'YQ'
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('sesid', '4s')]
    __slots__ = ('sesid',)
    sesid: bytes

    timed_out = property(lambda self: BroadcastPacket._get_b_flag(self, TIMED_OUT_BIT),
                         lambda self, v: BroadcastPacket._set_b_flag(self, TIMED_OUT_BIT, v))


class PacketYR(Packet):
    """
    Sync Reply packet.
    """

    _code = b'YR'
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('pktnum', 'I'), ('expire', 'H'), ('sesid', '4s'),
                   ('uptime', 'I'), ('flags', 'B'), ('nick', 'S1'), ('info', 'S1'), ('topic', 'S1'),
                   ('c_nbs', '+6s'), ('u_nbs', '+6s')]
    _max_extra = 1024
    __slots__ = ('src_ipp', 'pktnum', 'expire', 'sesid', 'uptime', 'flags', 'nick', 'info', 'topic', 'c_nbs',
                 'u_nbs')
    _encode_array = encode_array1
    _decode_array = decode_array1
    src_ipp: bytes
    pktnum: int
    expire: int
    sesid: bytes
    uptime: int
    flags: int
    nick: str
    info: str
    topic: str
    c_nbs: List[bytes]
    u_nbs: List[bytes]

    def __init__(self, **kwargs):
        self.flags = 0x00
        super().__init__(**kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    persist = property(lambda self: PacketYR._get_flag(self, PERSIST_BIT),
                       lambda self, v: PacketYR._set_flag(self, PERSIST_BIT, v))


register_packet_classes(PacketYQ, PacketYR)


class _SyncInfo(object):
    """
    Data structure for holding information on node sync state.
    """

    def __init__(self, ipp: bytes):
        self._ipp: bytes = ipp
        self.timeout_task: Optional[asyncio.Task] = None

        self.fail_limit: int = 2

        # Used for stats
        self.in_total = False
        self.in_done = False

        self.proxy_request = False

    @property
    def ipp(self) -> bytes:
        return self._ipp

    def cancel_timeout(self) -> bool:
        return bool(self.timeout_task) and self.timeout_task.cancel()


class SyncManager(object):
    """
    Manager for getting the node synced up with the rest of the network.

    Attributes:
        main: the main Dtella namespace
        _uncontacted: a pseudorandom set of nodes yet to be contacted
        _wait_count: the number of nodes waiting to be heard back from
        _info: dictionary of sync data for each node on the network

        _last_bar: the number of '>' displayed in the previous update of the progress bar
    """

    def __init__(self, main: "DtellaMain_Base"):
        self.main: "DtellaMain_Base" = main
        self._running: bool = False
        self._uncontacted: RandSet[bytes] = RandSet()
        self._wait_count: int = 0
        self._info: Dict[bytes, _SyncInfo] = {}

        self._last_bar: int = -1

        self._main_task: Optional[asyncio.Task] = None
        self._show_progress_task: Optional[asyncio.Task] = None
        self._flood_lock: asyncio.Event = asyncio.Event()

    def __bool__(self):
        return self._running

    def start(self):
        self._info.clear()
        for ipp in self.main.osm.nearest_node_ipp():
            s = self._info[ipp] = _SyncInfo(ipp)
            s.in_total = True
            self._uncontacted.add(ipp)

        # Keep stats for how far along we are
        self.stats_done = 0
        self.stats_total = len(self._uncontacted)

        self.proxy_success = 0
        self.proxy_failed = 0

        self._running = True
        self._main_task = asyncio.create_task(self._run())

    async def _run(self):
        await self.main.show_login_status("Network Sync In Progress...", counter='inc')
        self._show_progress()

        # Start smaller to prevent an initial flood
        request_limit = 2

        while self._uncontacted or self._wait_count != 0:
            if self._wait_count >= request_limit:
                await self._flood_lock.wait()
                self._flood_lock.clear()
                continue

            # Raise request limit the first time it fills up
            if request_limit < 5 <= self._wait_count:
                request_limit = 5

            try:
                ipp = self._uncontacted.pop()
            except KeyError:
                assert self._wait_count, "control flow error in SyncManager main loop"
                await self._flood_lock.wait()
                self._flood_lock.clear()
            else:
                s = self._info[ipp]
                packet = PacketYQ(nb_ipp=self.main.osm.me.ipp, hops=2,
                                  timed_out=s.fail_limit < 2,
                                  src_ipp=self.main.osm.me.ipp, sesid=self.main.osm.me.sesid)
                await self.main.ph.sendPacket(packet, Ad(ipp))
                self._scheduleSyncTimeout(s)

        self.shutdown()
        asyncio.create_task(self.main.osm.sync_complete())

    def _updateStats(self, s: _SyncInfo, done: int, total: int):
        # Update the sync statistics for a single node.
        if done > 0 and not s.in_done:
            s.in_done = True
            self.stats_done += 1
        elif done < 0 and s.in_done:
            s.in_done = False
            self.stats_done -= 1

        if total > 0 and not s.in_total:
            s.in_total = True
            self.stats_total += 1
        elif total < 0 and s.in_total:
            s.in_total = False
            self.stats_total -= 1

    def _show_progress(self):
        """
        Schedule a callback to show the progress bar for syncing to the network.  If the method is already scheduled
        or in progress, ignore the call to schedule it again.
        """

        async def _show_progress_cb():

            MAX = 20
            done = self.stats_done
            total = self.stats_total
            bar = (MAX * done) // total if total else MAX

            if bar == self._last_bar:
                return
            self._last_bar = bar

            progress = '>' * bar + '_' * (MAX - bar)
            await self.main.show_login_status("[%s] (%d/%d)" % (progress, done, total))

        if self._show_progress_task and not self._show_progress_task.done():
            return
        self._show_progress_task = asyncio.create_task(_show_progress_cb())

    def giveUpNode(self, ipp: bytes):
        # This node seems to have left the network, so don't contact it.
        try:
            s = self._info.pop(ipp)
        except KeyError:
            return

        self._uncontacted.discard(ipp)

        if s.cancel_timeout():
            self._wait_count -= 1
            self._flood_lock.set()

        self._updateStats(s, -1, -1)
        self._show_progress()

    def receivedSyncReply(self, src_ipp: bytes, c_nbs: List[bytes], u_nbs: List[bytes]):

        my_ipp = self.main.osm.me.ipp

        # Loop through all the nodes that were just contacted by proxy
        for ipp in c_nbs:
            if ipp == my_ipp:
                continue
            try:
                s = self._info[ipp]
            except KeyError:
                # Haven't seen this one before, set a timeout because
                # we should be hearing a reply.
                s = self._info[ipp] = _SyncInfo(ipp)
                self._scheduleSyncTimeout(s, proxy=True)
                self._updateStats(s, 0, +1)
            else:
                if ipp in self._uncontacted:
                    # Seen this node, had planned to ping it later.
                    # Pretend like we just pinged it now.
                    self._uncontacted.discard(ipp)
                    self._scheduleSyncTimeout(s, proxy=True)

        # Loop through all the nodes which weren't contacted by this
        # host, but that the host is neighbors with.
        for ipp in u_nbs:
            if ipp == my_ipp:
                continue
            if ipp not in self._info:
                # If we haven't heard of this node before, create some
                # info and plan on pinging it later
                s = self._info[ipp] = _SyncInfo(ipp)

                self._uncontacted.add(ipp)
                self._updateStats(s, 0, +1)

                self._flood_lock.set()

        # Mark off that we've received a reply.
        try:
            s = self._info[src_ipp]
        except KeyError:
            s = self._info[src_ipp] = _SyncInfo(src_ipp)

        # Keep track of NAT stats
        if s.proxy_request:
            s.proxy_request = False

            if s.fail_limit == 2:
                self.proxy_success += 1
            elif s.fail_limit == 1:
                self.proxy_failed += 1

            if (self.proxy_failed + self.proxy_success >= 10 and
                    self.proxy_failed > self.proxy_success):
                self.main.shutdown('dead_port')
                return

        self._uncontacted.discard(src_ipp)

        self._updateStats(s, +1, +1)
        self._show_progress()

        if s.cancel_timeout():
            self._wait_count -= 1
            self._flood_lock.set()

    def _scheduleSyncTimeout(self, s: _SyncInfo, proxy=False):
        """
        Schedule a timeout to wait for a sync reply.  If a reply is already expected, ignore making a new timeout.
        """

        async def _schedule_sync_timeout_cb():
            await asyncio.sleep(2)
            s.timeout_task = None
            self._wait_count -= 1

            s.fail_limit -= 1
            if s.fail_limit > 0:
                # Try again later
                self._uncontacted.add(s.ipp)
            else:
                self._updateStats(s, 0, -1)
                self._show_progress()

            self._flood_lock.set()

        if s.timeout_task:
            return

        # Remember if this was requested first by another node
        if s.fail_limit == 2 and proxy:
            s.proxy_request = True

        self._wait_count += 1
        s.timeout_task = asyncio.create_task(_schedule_sync_timeout_cb())

    def shutdown(self):
        # Cancel all timeouts
        if self._show_progress_task:
            self._show_progress_task.cancel()

        for s in self._info.values():
            s.cancel_timeout()

        self._running = False


class SyncRequestRoutingManager(object):

    def __init__(self, main: "DtellaMain_Base"):
        self.main: "DtellaMain_Base" = main
        self.msgs: ExpiringDict[Tuple[bytes, bytes], Dict[bytes, int]] = ExpiringDict(expire=180)

    def received_sync_request(self, packet: PacketYQ):
        osm = self.main.osm
        assert osm and osm.synced, "processing a sync request when not synced"

        key = (packet.src_ipp, packet.sesid)

        # get ipp of all synced neighbors who we've heard from recently
        my_nbs = [pn.ipp for pn in osm.pgm.pnbs.values() if pn.got_ack and osm.has_node(pn.ipp)]
        random.shuffle(my_nbs)

        # see if we've seen this sync message before
        try:
            nbs = self.msgs[key]
            isnew = False
        except KeyError:
            nbs = self.msgs[key] = {}
            isnew = True
        else:
            self.msgs.refresh(key)

        # set the hop value of the neighbor who sent us this packet
        if nbs.get(packet.nb_ipp, 0) < packet.hops + 1:
            nbs[packet.nb_ipp] = packet.hops + 1

        cont = []
        uncont = my_nbs

        if packet.hops:
            packet = PacketYQ(nb_ipp=osm.me.ipp, hops=packet.hops - 1, b_flags=0, src_ipp=packet.src_ipp,
                              sesid=packet.sesid)

            uncont = []
            for ipp in my_nbs:
                # if we've already contacted 3 nodes, or we know this node has already been contacted with a higher
                # hop count, then don't forward the sync request to it.
                if len(cont) >= 3 or nbs.get(ipp, -1) >= packet.hops - 1:
                    uncont.append(ipp)
                else:
                    cont.append(ipp)
                    nbs[ipp] = packet.hops - 1
                    asyncio.create_task(self.main.ph.sendPacket(packet, Ad(ipp)))

        # cut off after 16 nodes, just in case
        uncont = uncont[:16]

        if isnew or packet.timed_out:
            self._send_sync_reply(packet.src_ipp, cont, uncont)

    def _send_sync_reply(self, src_ipp: bytes, contacted_nbs: List[bytes], uncontacted_nbs: List[bytes]):
        ad = Ad(src_ipp)
        osm = self.main.osm

        CHECK(osm and osm.synced)

        # If we send a YR which is almost expired, followed closely by
        # an NH with an extended expire time, then a race condition exists,
        # because the target could discard the NH before receiving the YR.

        # So, if we're about to expire, go send a status update NOW so that
        # we'll have a big expire time to give to the target.

        # Exact time left before my status expires.
        # (The receiver will add a few buffer seconds.)
        now = time.time()
        expire = osm.next_status_update_time - now
        if expire <= 5.0:
            osm.send_my_status(send_full=False)
            expire = osm.next_status_update_time - now

        # If I think I set the topic last, then put it in here.
        # It's up to the receiving end whether they'll believe me.
        packet = PacketYR(src_ipp=osm.me.ipp,
                          pktnum=osm.me.status_pktnum,
                          expire=int(expire),
                          sesid=osm.me.sesid,
                          uptime=int(time.time() - osm.me.uptime),
                          flags=osm.me.flags,
                          nick=osm.me.nick,
                          info=osm.me.info_out,
                          topic=osm.tm.topic if osm.tm.topic_node is osm.me else "",
                          c_nbs=contacted_nbs,
                          u_nbs=uncontacted_nbs)

        asyncio.create_task(self.main.ph.sendPacket(packet, ad))

    def shutdown(self):
        self.msgs.clear()
