"""
Dtella - Local Site Configuration
Copyright (C) 2007-2008  Dtella Labs (http://www.dtella.org/)
Copyright (C) 2007-2008  Paul Marks (http://www.pmarks.net/)
Copyright (C) 2007-2008  Jacob Feisley (http://www.feisley.com/)
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import configparser
import re
import sys
from typing import Tuple

from dtella.util import parse_bytes
from dtella.util.ipv4 import SubnetMatcher

_config = configparser.ConfigParser()
_config.read("dtella/local.cfg")

hub_name: str = _config.sections()[0]
build_prefix: str = _config[hub_name]["build_prefix"]

version_string: str = _config[hub_name]["version"]
version: Tuple[int, ...] = tuple(map(int, re.match("(\d*)[.](\d*)[.](\d*)", version_string).groups()))
if sys.platform.startswith('freebsd'):
    version_tag: str = f"Dt:{version_string}/B"
elif sys.platform.startswith('linux'):
    version_tag: str = f"Dt:{version_string}/L"
elif sys.platform.startswith('aix'):
    version_tag: str = f"Dt:{version_string}/A"
elif sys.platform.startswith('win'):
    version_tag: str = f"Dt:{version_string}/W"
elif sys.platform.startswith('cygwin'):
    version_tag: str = f"Dt:{version_string}/C"
elif sys.platform.startswith('sun'):
    version_tag: str = f"Dt:{version_string}/S"
elif sys.platform.startswith('darwin'):
    version_tag: str = f"Dt:{version_string}/M"
else:
    version_tag: str = f"Dt:{version_string}/?"

network_key: str = _config[hub_name]["network_key"]
minshare_cap: int = parse_bytes(_config[hub_name]["minshare_cap"])

dc_port: int = int(_config[hub_name]["dc_port"])
allowed_subnets: SubnetMatcher = SubnetMatcher([subnet.strip() for subnet in
                                                _config[hub_name]["allowed_subnets"].split(',')])

dconfig_puller = None
_dconfig = _config[hub_name]["dconfig"]
if _dconfig == "dns":
    from dtella.dconfig.dns import DnsTxtPuller

    dconfig_puller = DnsTxtPuller(
        servers=[subnet.strip() for subnet in _config[hub_name]["dns.servers"].split(',')],
        hostname=_config[hub_name]["dns.hostname"]
    )
elif _dconfig == "gdata":
    pass
elif _dconfig == "textfile":
    from dtella.dconfig.textfile import TextFilePuller

    dconfig_puller = TextFilePuller(filename=_config[hub_name]["textfile.filename"])

use_locations = _config[hub_name].getboolean("use_locations")
use_bridge = _config[hub_name].getboolean("use_bridge")

# ###############################################################################
#
# # if use_locations is True, then rdns_servers and hostnameToLocation will be
# # used to perform the location translation.  If you set use_locations = False,
# # then you may delete the rest of the lines in this file.
#
# # DNS servers which will be used for doing IP->Hostname reverse lookups.
# # These should be set to your school's local DNS servers, for efficiency.
# rdns_servers = ['128.210.11.5', '128.210.11.57', '128.10.2.5', '128.46.154.76']
#
# # Customized data for our implementation of hostnameToLocation
# import re
# suffix_re = re.compile(r".*\.([^.]+)\.purdue\.edu$")
# prefix_re = re.compile(r"^([a-z]{1,6}).*\.purdue\.edu$")
#
# pre_table = {
#     'erht':'Earhart', 'cary':'Cary', 'hill':'Hillenbrand',
#     'shrv':'Shreve', 'tark':'Tarkington', 'wily':'Wiley',
#     'mrdh':'Meredith', 'wind':'Windsor', 'harr':'Harrison',
#     'hawk':'Hawkins', 'mcut':'McCutcheon', 'owen':'Owen',
#     'hltp':'Hilltop', 'yong':'Young', 'pvil':'P.Village',
#     'fstc':'FST', 'fste':'FST', 'fstw':'FST',
#     'pal':'AirLink', 'dsl':'DSL', 'vpn':'VPN'}
#
# suf_table = {
#     'cerias':'CERIAS', 'cs':'CS', 'ecn':'ECN', 'hfs':'HFS',
#     'ics':'ITaP Lab', 'lib':'Library', 'mgmt':'Management',
#     'uns':'News', 'cfs':'CFS'}
#
# def hostnameToLocation(hostname):
#     # Convert a hostname into a human-readable location name.
#
#     if hostname:
#
#         suffix = suffix_re.match(hostname)
#         if suffix:
#             try:
#                 return suf_table[suffix.group(1)]
#             except KeyError:
#                 pass
#
#         prefix = prefix_re.match(hostname)
#         if prefix:
#             try:
#                 return pre_table[prefix.group(1)]
#             except KeyError:
#                 pass
#
#     return "Unknown"

