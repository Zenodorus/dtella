"""
Dtella - Packet Structure
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

from hashlib import md5
from typing import List, Tuple, Type, Dict, Union, Optional

from Crypto.Cipher import AES
from Crypto.Cipher.AES import AESCipher
from Crypto.PublicKey import pubkey

from dtella.common.log import LOG
from dtella.util import Ad
from dtella.util.encoding import DecodingError, EncodingError, decode_item, decode_array, encode_item, encode_array
import dtella.common.local_config as local


# Broadcast Flags
REJECT_BIT = 0x1


# verify packet encoding functions by immediately encoding a packet after decoding and verifying contents
debug_encoding = True


class PacketError(Exception):
    pass


class BadPacketError(PacketError):
    pass


class PacketEncrypter(object):

    def __init__(self, key: str):
        self.aes: AESCipher = AES.new(md5(key.encode()).digest())

    def encrypt(self, data: Union[bytes, "Packet"]) -> bytes:
        # AES requires packets in blocks of 16 bytes so pad the packet with
        # its MD5 hash.  We need a minimum of 5 hash bytes.
        # There's also 1 byte at the end to store the hash length

        if isinstance(data, Packet):
            data = data.encode()
        hlen = ((10-len(data)) % 16) + 5
        h = md5(data).digest()[:hlen] + b'\0'*(hlen-16)
        data += h + hlen.to_bytes(1, byteorder='little')
        return self.aes.encrypt(data)

    def decrypt(self, data: bytes) -> bytes:
        if not (data and len(data) % 16 == 0):
            raise ValueError("Bad Length")

        data: bytes = self.aes.decrypt(data)
        hlen = data[-1]

        if not (5 <= hlen < len(data)):
            raise ValueError("Bad Hash Length")

        h = data[-(hlen+1):-1][:16]
        data = data[:-(hlen+1)]

        if h != md5(data).digest()[:hlen]:
            raise ValueError("Bad Hash Value")

        return data


pk_enc = PacketEncrypter(local.network_key)


# TODO probably could use some metaclass magic to guarantee that a developer creates a new packet type correctly
# TODO remove having to specify 'code' or other data model attributes in the data model of subclasses
# TODO remove _decode_array and _encode_array in favor of the + or ++ system

class Packet(object):
    """
    Packet is the abstract base class for the containers that hold data to be transmitted across the dtella network.
    Data in packets follow the encoding scheme described in dtella/common/encoding.py.

    _data_model is a static, protected class variable that will describe how the packets are organized.  It should be
    a list of 2-tuples (key, struct format).  The ordering of these tuples determines the order that they will be
    placed in the packet, with all packets starting with the 2 character packet ID code.

    _encode_array and _decode_array variables are here to make Dtella 1.2.9 compatibility easier.  A packet that
    uses a single byte to encode the length of an array should use the encode_array1 and decode_array1 functions in
    dtella/common/encoding.py for the values of these variables instead.

    _signed is a boolean that determines whether or not data at the end of packet is used as a signature to verify
    the sender of the packet.  If _signed is false, then packets should not have any extra data.  Otherwise,
    the extra data is stored in _signature.

    _data is a cached copy of the encoded contents used for signing purposes.  This should not be directly accessed.
    """

    _code: bytes = b'\x00\x00'
    _data_model: List[Tuple[str, str]] = [('code', '2s')]
    _encode_array = encode_array
    _decode_array = decode_array
    _signed: bool = False
    _max_extra: int = 0
    __slots__ = ("code", "_signature", "_data")
    _signature: bytes
    _data: bytes

    def __init__(self, **kwargs):
        for (k, v) in kwargs.items():
            setattr(self, k, v)
        if "code" in kwargs and self._code != kwargs["code"]:
            raise BadPacketError(f"a {self._code} packet is being created with data for a {kwargs['code']} packet")
        elif "code" not in kwargs:
            self.code = self._code
        self._data = kwargs.get("_data", b"")
        self._signature = kwargs.get("_signature", b"")

    def __setattr__(self, key, value):
        if not key.startswith('_') and getattr(self, "_data", True):
            self._data = b""
        super().__setattr__(key, value)

    @classmethod
    def _decode(cls, data: bytes) -> Tuple[dict, bytes]:
        decoded = {"_data": data}
        try:
            for (key, fmt) in cls._data_model:
                value, data = cls._decode_array(fmt, data) if fmt[0] == '+' else decode_item(fmt, data)
                decoded[key] = value
        except DecodingError as e:
            raise BadPacketError(f"could not decode packet: {e}")
        return decoded, data

    @classmethod
    def decode(cls, data: bytes):
        """Decode raw data into a Packet following its _data_model."""

        decoded, signature = cls._decode(data)
        if cls._signed:
            self = cls(**decoded, _signature=signature)
        elif len(signature) > cls._max_extra:
            raise BadPacketError(f"extra data decoding {cls.__name__}")
        else:
            self = cls(**decoded)

        if debug_encoding and data != self.encode():
            LOG.warn(f"{cls.__name__} encoding check did not pass")
        return self

    # TODO add signing ability
    def encode(self, sign: bool = False) -> bytes:
        """Encode this Packet into raw data following its _data_model."""

        if self._data and not debug_encoding:
            return self._data
        data = b""
        try:
            for (key, fmt) in self._data_model:
                value = getattr(self, key)
                data += type(self)._encode_array(fmt, value) if fmt[0] == '+' else encode_item(fmt, value)
        except EncodingError as e:
            raise BadPacketError(f"could not encode packet: {e}")
        if self._signed:
            data += self._signature
        self._data = data
        return data

    def as_string(self, src_ad: Optional[Ad] = None, dst_ad: Optional[Ad] = None) -> str:
        """Pretty print the packet data for logging purposes."""

        assert bool(src_ad) != bool(dst_ad), "cannot supply both source and destination when logging packets"
        return f"{self.code.decode()} {'<-' if src_ad else '->'} {src_ad or dst_ad}"

    def verify(self, rsa_obj: pubkey.pubkey):
        """Verify the contents of the packet using the provided key"""

        if not self._signed:
            return True
        elif not rsa_obj:
            return False

        body = self._data[:-len(self._signature)]
        data_hash = md5(body).digest()
        sig_tuple = (int.from_bytes(self._signature, byteorder="big"),)
        return rsa_obj.verify(data_hash, sig_tuple)

    # TODO
    def sign(self, rsa_obj):
        pass


class AckablePacket(Packet):
    """
    Packet subclass that guarantees an acknowledgement key attribute.  This class is not meant to be used directly.
    """

    __slots__ = ('ack_key',)
    ack_key: bytes


class BroadcastPacket(Packet):
    """
    Packet subclass for packets that are meant to be broadcast to the network.
    """

    _code = b'\xFF\xFF'
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s')]
    __slots__ = ('nb_ipp', 'hops', 'b_flags', 'src_ipp')
    nb_ipp: bytes
    hops: int
    b_flags: int
    src_ipp: bytes

    def __init__(self, **kwargs):
        self.b_flags = 0x00
        Packet.__init__(self, **kwargs)

    def _get_b_flag(self, bit_code: int) -> bool:
        return bool(self.b_flags & bit_code)

    def _set_b_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.b_flags |= bit_code
        else:
            self.b_flags &= (0xFF - bit_code)

    reject = property(lambda self: BroadcastPacket._get_b_flag(self, REJECT_BIT),
                      lambda self, v: BroadcastPacket._set_b_flag(self, REJECT_BIT, v))

    @property
    def ack_key(self) -> bytes:
        if not self._data:
            self.encode()
        return md5(self._data[:2] + self._data[10:]).digest()[:8]

    def verify(self, rsa_obj: pubkey.pubkey):
        """Verify the contents of the packet using the provided key"""

        if not self._signed:
            return True
        elif not rsa_obj:
            return False

        # broadcast packets do not use nb_ipp, hops, and b_flags when signing
        body = self._data[:2] + self._data[10:-len(self._signature)]
        data_hash = md5(body).digest()
        sig_tuple = (int.from_bytes(self._signature, byteorder="big"),)
        return rsa_obj.verify(data_hash, sig_tuple)


_packet_classes: Dict[bytes, Type[Packet]] = {}


def register_packet_classes(*classes: Type[Packet]):
    """Register a class as a Packet that can be built with build_packet."""

    for cls in classes:
        assert issubclass(cls, Packet), f"cannot register {cls.__name__} as a Packet"
        assert cls != Packet, f"cannot register the base Packet class"
        _packet_classes[cls._code] = cls


def build_packet(data: bytes) -> Packet:
    """Build a packet from provided data using registered Packet classes."""

    data = pk_enc.decrypt(data)
    if len(data) < 2:
        raise BadPacketError("too short")
    if data[:2] not in _packet_classes:
        raise BadPacketError(f"unknown packet type {data[:2].decode()}")
    return _packet_classes[data[:2]].decode(data)
