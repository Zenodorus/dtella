"""
Dtella - Ping Neighbor Manager
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import os
import random
import time
from typing import Dict, Optional, Tuple, List, TYPE_CHECKING


# from dtella.bridge.node import BridgeNode
from dtella.common import BadTimingError, NODE_EXPIRE_EXTEND, MessageCollisionError
from dtella.common.log import LOG
from dtella.common.node import PacketNF
from dtella.common.packet import BadPacketError, Packet, register_packet_classes, AckablePacket
from dtella.util import Ad, encoding
from dtella.util.scheduled_task import ScheduledEvent

if TYPE_CHECKING:
    from dtella.common.base import DtellaMain_Base

# Ping Flags
IWANT_BIT = 0x01
GOTACK_BIT = 0x02
REQ_BIT = 0x04
ACK_BIT = 0x08
NBLIST_BIT = 0x10
OFFLINE_BIT = 0x20

# How many seconds our node will last without incoming pings
ONLINE_TIMEOUT = 30.0


class PacketPG(AckablePacket):
    """
    Ping packet.  These packets maintain the connection a node has with the network.

    Attributes:
        code: the 2-letter string 'PG'
        src_ipp: the IP:Port of the node that send the ping
        flags: collection of bit flags for the following:
          - uwant: this link is desired to be an active link by the sender
          - u_got_ack: the receiver's acknowledgement of the sender's request was received
          - req: the packet contains a req_key that the receiver should reply back with to acknowledge its reception
          - ack: the packet contains an ack_key that is an acknowledgement of an earlier ack request ping
          - nblist: the packet contains some the IP:Ports of some neighbor nodes
          - offline: the recipient node is not recognized as being online by the sender (unused flag)
        req_key: 4 byte string containing an acknowledgement key that the sender is requesting for a reply
        ack_key: 4 byte string containing an acknowledgement key that the sender is acknowledging
        nbs: list of raw IP:Ports that are ping neighbors of the sender

    Note:
    The individual flags uwant, u_got_ack, req, ack, nblist, and offline are all boolean properties defined in
    PacketPG.  Then can be get/set via boolean True/False, as opposed to directly altering the bit in the flags
    variable.
    """

    _code = b'PG'
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('flags', 'B'), ('req_key', '4s'), ('ack_key', '4s'),
                   ('nbs', '+6s')]
    __slots__ = ('src_ipp', 'flags', 'req_key', 'nbs')
    _decode_array = encoding.decode_array1
    _encode_array = encoding.encode_array1

    def __init__(self, **kwargs):
        self.flags = 0x00
        Packet.__init__(self, **kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    uwant = property(lambda self: PacketPG._get_flag(self, IWANT_BIT),
                     lambda self, v: PacketPG._set_flag(self, IWANT_BIT, v))
    u_got_ack = property(lambda self: PacketPG._get_flag(self, GOTACK_BIT),
                         lambda self, v: PacketPG._set_flag(self, GOTACK_BIT, v))
    req = property(lambda self: PacketPG._get_flag(self, REQ_BIT),
                   lambda self, v: PacketPG._set_flag(self, REQ_BIT, v))
    ack = property(lambda self: PacketPG._get_flag(self, ACK_BIT),
                   lambda self, v: PacketPG._set_flag(self, ACK_BIT, v))
    nblist = property(lambda self: PacketPG._get_flag(self, NBLIST_BIT),
                      lambda self, v: PacketPG._set_flag(self, NBLIST_BIT, v))
    offline = property(lambda self: PacketPG._get_flag(self, OFFLINE_BIT),
                       lambda self, v: PacketPG._set_flag(self, OFFLINE_BIT, v))

    @classmethod
    def decode(cls, data: bytes) -> "PacketPG":
        packet = cls()
        packet.code, data = encoding.decode_item('2s', data)
        packet.src_ipp, data = encoding.decode_item('6s', data)
        packet.flags, data = encoding.decode_item('B', data)
        packet.req_key, data = encoding.decode_item('4s', data) if packet.req else (None, data)
        packet.ack_key, data = encoding.decode_item('4s', data) if packet.ack else (None, data)
        packet.nbs, data = cls._decode_array('+6s', data) if packet.nblist else ([], data)

        if len(packet.nbs) > 8:
            raise BadPacketError("too many neighbors")
        elif len(set(packet.nbs)) != len(packet.nbs):
            raise BadPacketError("neighbors not all unique")

        if data:
            raise BadPacketError(f"extra data decoding {cls.__name__}")
        return packet

    def encode(self, sign: bool = False) -> bytes:
        return (encoding.encode_item('2s', self.code) +
                encoding.encode_item('6s', self.src_ipp) +
                encoding.encode_item('B', self.flags) +
                (encoding.encode_item('4s', self.req_key) if self.req else b'') +
                (encoding.encode_item('4s', self.ack_key) if self.ack else b'') +
                (self._encode_array('+6s', self.nbs) if self.nblist else b''))


class PacketPF(Packet):
    """
    Possible Failure Packet.  When a ping neighbor node fails to acknowledge a ping, then a PF packet is sent to that
    node's ping neighbors to ask if they have detected a failure as well.

    Note:
    The sender of this packet is the node that possibly detected a failure.
    The receiver is a node that is ping neighbor of the possibly failing node.

    Attributes:
        code: the 2-letter string 'PF'
        nb_ipp: the IP:Port of the sender
        dead_ipp: the IP:Port of the possibly failing node
        pktnum:
        sesid: the session ID of the possibly failing node
    """

    _code = b'PF'
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('dead_ipp', '6s'), ('pktnum', 'I'), ('sesid', '4s')]
    __slots__ = ('nb_ipp', 'dead_ipp', 'pktnum', 'sesid')
    nb_ipp: bytes
    dead_ipp: bytes
    pktnum: int
    sesid: bytes


register_packet_classes(PacketPG, PacketPF)


class _PingNeighbor(object):
    """
    Structure to hold data about neighbors.

    Attributes:
        _ipp: the neighbor node's IP:Port (raw)
        is_outbound_link: boolean for whether or not this node will be one of my links to the network
        is_inbound_link: boolean for whether or not this node has me as a link to the network
        ping_reqs: dictionary of outstanding acknowledgement requests, mapping the acknowledgement key to the event loop
            uptime timestamp of when the acknowledgement request packet was sent
        got_ack: boolean for whether or not I got an acknowledgement from the node
        u_got_ack: boolean for whether or not I sent an acknowledgement to the node
        ping_nbs: tuple of raw IP:Ports that are ping neighbors of the node
        _avg_ping: delay between acknowledgement request sent to the node and received from the node
    """

    def __init__(self, ipp: bytes, main: "DtellaMain_Base"):
        self._ipp: bytes = ipp
        self.main: "DtellaMain_Base" = main

        self.is_outbound_link: bool = False
        self.is_inbound_link: bool = False

        self.ping_reqs: Dict[bytes, float] = {}
        self.got_ack: bool = False
        self.u_got_ack: bool = False
        self._ping_nbs: Tuple[bytes, ...] = ()
        self._avg_ping = None

        self._ping_tasks: List[asyncio.Task] = []
        self._retransmitting_ping_task: Optional[asyncio.Task] = None
        self._failed_timeout_task: ScheduledEvent = ScheduledEvent(delay=0)
        self._next_ping_when: float = 0
        self._ping_is_shortable = False
        self._is_alive = False

    @property
    def ipp(self):
        return self._ipp

    @property
    def avg_ping(self):
        return self._avg_ping

    @property
    def ping_nbs(self) -> Tuple[bytes, ...]:
        return self._ping_nbs

    def _expire_ack_requests(self, age: float):
        """
        Expire any acknowledgement requests that are more than age seconds old.
        """
        now = time.monotonic()
        for req_key, when in tuple(self.ping_reqs.items()):
            if now - when > age:
                del self.ping_reqs[req_key]

    def _generate_req_key(self) -> bytes:
        """
        Generate a new req_key.
        """
        while (req_key := os.urandom(4)) in self.ping_reqs:
            pass
        self.ping_reqs[req_key] = time.monotonic()
        return req_key

    def update(self, packet: PacketPG):
        """
        Update data about this neighbor
        """
        if packet.nbs is not None:
            self._ping_nbs = tuple(packet.nbs)
        self.is_inbound_link = packet.uwant
        self.u_got_ack = packet.u_got_ack
        self._is_alive = True

        if packet.ack:
            try:
                send_time = self.ping_reqs[packet.ack_key]
            except KeyError:
                raise BadPacketError("PG: unknown ack")

            # Keep track of ping delay
            delay = time.monotonic() - send_time
            LOG.debug(f"Ping: {delay * 1000:.2f} ms")

            if self._avg_ping is None:
                self._avg_ping = delay
            else:
                self._avg_ping = 0.8 * self._avg_ping + 0.2 * delay

            # If we just got the first ack, then send a ping now to
            # send the GOTACK bit to neighbor
            if not self.got_ack:
                self.got_ack = True

    def cancel(self, force: bool = False):
        """Cancel the link due to inactivity.  If force, then intentionally drop it."""
        if force:
            self.is_outbound_link = self.is_inbound_link = False
        assert self.is_inactive, "cancelling a link that is not inactive"

        if self._retransmitting_ping_task:
            self._retransmitting_ping_task.cancel()
        self._retransmitting_ping_task = None

        self._failed_timeout_task.cancel()

    def send_ping(self, i_req: bool, ack_key: bytes = b"") -> asyncio.Task:
        """
        Transmit a ping to the specified neighbor.  The ping may request that the receiver acknowledge the receipt of
        the ping (i_req) or it may also contain the acknowledgement key (ack_key).
        """

        self._expire_ack_requests(15.0)

        packet = PacketPG(src_ipp=self.main.osm.me.ipp,
                          uwant=self.is_outbound_link, u_got_ack=self.got_ack,
                          req=i_req, ack=bool(ack_key), nblist=i_req,
                          offline=self.main.osm.synced and (self._ipp not in self.main.osm._lookup_ipp),
                          req_key=self._generate_req_key() if i_req else b"",
                          ack_key=ack_key,
                          nbs=self.main.osm.pgm.get_pnbs_short_list(self) if i_req else [])
        return asyncio.create_task(self.main.ph.sendPacket(packet, Ad(self._ipp)))

    async def _ping_with_retransmit(self, tries: int, later: bool, ack_key: bytes = b""):
        """
        Schedule a retransmitting ping asking for an acknowledgement.
        """

        self.ping_reqs.clear()

        if later:
            when = 5.0
        else:
            # Send first ping
            self.send_ping(True, ack_key)
            tries -= 1
            when = 1.0

        # leave a flag value in the dcall so we can test whether this ping can be made a bit sooner
        if later:
            self._ping_is_shortable = True

        while True:
            when *= random.uniform(0.9, 1.1)
            self._next_ping_when = time.monotonic() + when
            await asyncio.sleep(when)
            self.send_ping(True)

            # while tries is positive, use 1 second intervals.
            # When it hits zero, trigger a timeout.  As it goes negative,
            # pings get progressively more spaced out.

            when = 1 if tries > 0 else (2 ** -tries)

            # Just failed now
            if tries == 0:
                self._is_alive = False

                if self.main.osm.synced and self.got_ack:
                    # Note that we had to set sendPing_dcall before this.
                    self.main.osm.pgm.handle_node_failure(self.ipp)

                self.got_ack = False

                # If this was an inbound node, forget it.
                self.is_inbound_link = False

                if self.is_outbound_link:
                    # An outbound link just failed.  Go find another one.
                    self.main.osm.pgm.schedule_make_new_links()
                else:
                    # Neither side wants this link.  Clean up.
                    self.main.osm.pgm.cancel_link(self)

            tries = max(tries - 1, -7)

    def schedule_ping_with_retransmit(self, tries: int, later: bool, ack_key: bytes = b""):
        """
        Schedule a retransmitting ping asking for an acknowledgement.
        """
        if self._retransmitting_ping_task and not self._retransmitting_ping_task.done():
            self._retransmitting_ping_task.cancel()
        self._retransmitting_ping_task = asyncio.create_task(self._ping_with_retransmit(tries, later, ack_key))

    def set_fail_timeout(self):
        self._failed_timeout_task.reset(15.0)

    def cancel_fail_timeout(self):
        self._failed_timeout_task.cancel()

    @property
    def is_failing(self):
        return not self._failed_timeout_task.is_set

    @property
    def is_alive(self) -> bool:
        """
        Return true if the connection hasn't timed out yet, that is, if a ping is still set to be sent.
        """
        return self._is_alive

    @property
    def is_inactive(self) -> bool:
        return not self.is_outbound_link and not self.is_inbound_link

    @property
    def strongly_connected(self) -> bool:
        """
        Return true if both have received acknowledgements.
        """
        # return True if both ends are willing to accept broadcast traffic
        return self.got_ack and self.u_got_ack

    @property
    def ping_is_shortable(self) -> bool:
        return (self._ping_is_shortable and self._retransmitting_ping_task and
                (self._next_ping_when - time.monotonic() < 1))


class PingManager(object):
    """
    Manager for handling pings and maintaining links to a few nodes on the network.

    PingManager begins with a call to schedule_make_new_links, which will fire out link requests to every node that
    it knows (new nodes only have data from initial contact and are not synced with the network yet).  The lifecycle
    of building a link from Alice to Bob is as follows:

    - Alice immediately sends retransmitting ping to Bob
    - Bob immediately replies to Alice acknowledging her request
    - Alice replies back to Bob to acknowledge his acknowledgement
    """

    OUTLINK_GOAL = 3

    def __init__(self, main: "DtellaMain_Base"):
        self.main: "DtellaMain_Base" = main
        self.pnbs: Dict[bytes, _PingNeighbor] = {}

        self._last_received_time: float = time.monotonic()
        self._timeout_task: asyncio.Task = asyncio.create_task(self._online_timeout())
        self._make_new_links_task: Optional[asyncio.Task] = None
        self._chop_excess_links_task: Optional[asyncio.Task] = None

    def get_pnbs_short_list(self, pn: _PingNeighbor) -> List[bytes]:
        """
        Return a list of at most 8 IP:port of ping neighbors that are not pn.
        """
        if not self.main.osm.synced:
            return []

        nbs = sorted(qn.ipp for qn in self.pnbs.values()
                     if (qn.ipp != pn.ipp and qn.ipp in self.main.osm._lookup_ipp and qn.strongly_connected))
        del nbs[8:]
        return nbs

    def received_ping(self, packet: PacketPG):

        osm = self.main.osm

        try:
            pn = self.pnbs[packet.src_ipp]
        except KeyError:
            # if we're not fully online yet, then reject pings that we never asked for
            if not osm.synced:
                raise BadTimingError("not ready to accept pings yet")
            pn = self.pnbs[packet.src_ipp] = _PingNeighbor(packet.src_ipp, self.main)

        assert osm.synced or pn.is_outbound_link

        # cache some data before updating the PingNeighbor
        was_strongly_connected = pn.strongly_connected
        had_got_ack_before = pn.got_ack
        pn.update(packet)

        # if they requested an ACK or if we just got the first ACK from them, then we'll want to ping soon.
        ping_now = packet.req or (packet.ack and not had_got_ack_before)

        # if this ping contains an acknowledgement, schedule next ping in ~5 seconds and reset online timeout
        if packet.ack:
            pn.schedule_ping_with_retransmit(tries=4, later=True)
            self._last_received_time = time.monotonic()

        # just got strongly connected, so make sure we don't have an excess of links and begin sync if not synced
        if not was_strongly_connected and pn.strongly_connected:
            if pn.is_outbound_link:
                self._schedule_chop_excess_links()
            if not (osm.synced or osm.sm):
                osm.sm.start()

        # determine if a ACK should be requested
        if not (pn.is_outbound_link or pn.is_inbound_link):
            # don't request an ack for an unwanted connection
            pass
        elif not pn.is_alive or (ping_now and pn.ping_is_shortable):
            # Try to revitalize this connection
            # We've got a REQ to send out in a very short time, so
            # send it out early with this packet we're sending already.
            pn.schedule_ping_with_retransmit(tries=4, later=False, ack_key=packet.req_key)
            ping_now = False

        if ping_now:
            # Send a ping without an ACK request
            pn.send_ping(i_req=False, ack_key=packet.req_key)

        # If neither end wants this connection, throw it away.
        if not (pn.is_outbound_link or pn.is_inbound_link):
            self.cancel_link(pn)

    def cancel_link(self, pn: _PingNeighbor, force=False):
        """
        Cancel attempting to link with this node if the link has gone inactive.  If force, then intentionally drop
        the link (used for bans).
        """
        iwant = pn.is_outbound_link
        pn.cancel(force)
        del self.pnbs[pn.ipp]
        if force and iwant:
            self.schedule_make_new_links()

    def handle_node_failure(self, failed_ipp, nb_ipp=None):
        """
        Handle a node that may have failed.  nb_ipp is supplied when this is coming from a PF packet from that neighbor.
        """
        assert self.main.osm and self.main.osm.synced, "node failure called when not online"

        # if this node isn't my neighbor or if it isn't online, then don't even bother.
        try:
            pn = self.pnbs[failed_ipp]
            n = self.main.osm.get_node(failed_ipp)
        except KeyError:
            return

        # only accept a remote failure if that node is a neighbor of pn.
        if nb_ipp and pn.ping_nbs and nb_ipp not in pn.ping_nbs:
            return
        # # A bridge node will just have to time out on its own
        # if isinstance(n, BridgeNode):
        #     return
        # if the node's about to expire anyway, don't bother
        if n.timeout_left <= NODE_EXPIRE_EXTEND * 1.1:
            return

        if not pn.is_alive and (nb_ipp or pn.is_failing or pn.ping_nbs == ()):
            # Trigger an NF message if I've experienced a failure, and:
            # - someone else just experienced a failure, or
            # - someone else experienced a failure recently, or
            # - I seem to be pn's only neighbor.

            pn.cancel_fail_timeout()
            packet = PacketNF(nb_ipp=self.main.osm.me.ipp, hops=32, b_flags=0, src_ipp=n.ipp, pktnum=n.status_pktnum,
                              sesid=n.sesid)
            try:
                self.main.osm.mrm.new_message(packet, tries=2)
            except MessageCollisionError:
                # it's possible, but rare, that we've seen this NF before without fully processing it.
                pass

            n.reschedule_timeout(NODE_EXPIRE_EXTEND)

        elif nb_ipp:
            # if this failure was reported by someone else and I have not noticed yet, then set the failure timeout,
            # so when I detect a failure, I'll be sure of it.
            pn.set_fail_timeout()

        elif pn.ping_nbs:
            # reported by me, and pn has neighbors, so send PF to pn's neighbors
            packet = PacketPF(nb_ipp=self.main.osm.me.ipp, dead_ipp=n.ipp, pktnum=n.status_pktnum, sesid=n.sesid)
            for nb_ipp in pn.ping_nbs:
                self.main.ph.sendPacket(packet, Ad(nb_ipp))

    def remove_outbound_link(self, ipp):
        """
        Tell a neighbor that you wish to drop your connection with them.  Return true if the neighbor was an outgoing
        connection (and subsequently told to drop the link).
        """
        try:
            pn = self.pnbs[ipp]
        except KeyError:
            return False

        if not pn.is_outbound_link:
            return False

        # Send iwant=0 to neighbor
        pn.is_outbound_link = False
        if pn.is_inbound_link:
            pn.schedule_ping_with_retransmit(tries=4, later=False)
        else:
            pn.send_ping(i_req=False)
            self.cancel_link(pn)
        return True

    def schedule_make_new_links(self):
        """
        Ping the network to make new links.  Iterate through the list of online nodes, send an initial ping to each,
        and mark them as outbound links.  Ignore any nodes that were marked as outbound but had their retransmitting
        ping cancelled.  Continue until the list of nodes is exhausted or OUTLINK_GOAL number of nodes is reached.

        This method should be scheduled whenever a node wishes to the join the network or when a link dies.  It
        should also not be scheduled if its already in progress.

        Note:
        Normally nodes in the OSM are sorted by their pseudo-random distance, but when a node is joining the network,
        nodes is randomly shuffled.
        """

        async def make_new_links():
            n_alive = 0
            for ipp in self.main.osm.nearest_node_ipp():

                if ipp not in self.pnbs:
                    self.pnbs[ipp] = _PingNeighbor(ipp, self.main)
                pn = self.pnbs[ipp]

                if not pn.is_outbound_link:
                    tries = 2
                    if pn.is_inbound_link and pn.strongly_connected:
                        # an active inbound link is being marked as outbound, so we might want to close some other
                        # outbound link
                        self._schedule_chop_excess_links()
                        tries = 4

                    pn.is_outbound_link = True
                    pn.schedule_ping_with_retransmit(tries=tries, later=False)

                if pn.is_outbound_link and pn.is_alive:
                    n_alive += 1
                    if n_alive >= self.OUTLINK_GOAL:
                        break

            self._make_new_links_task = None

        if self._make_new_links_task and not self._make_new_links_task.done():
            return
        self._make_new_links_task = asyncio.create_task(make_new_links())

    def _schedule_chop_excess_links(self):
        """
        Trim the number of links if we have a strong enough connection to several nodes in the network.  Iterate
        through the list of online nodes by order of distance and find OUTLINK_GOAL number of strongly connected
        nodes.  If any node is found that has not been sent an outgoing ping before OUTLINK_GOAL is reached,
        skip chopping links.

        This is called whenever a node becomes strongly connected.  It should also not be scheduled if its already in
        progress.
        """

        async def chop_excess_links():
            # this method is synchronous, so safe to remove early in case the loop short circuits
            self._chop_excess_links_task = None

            # cache a set of outbound neighbors.  any the remain after the loop will be killed
            outbound_links = set(pn.ipp for pn in self.pnbs.values() if pn.is_outbound_link)
            n_alive = 0

            for n in self.main.osm.nodes:
                try:
                    pn = self.pnbs[n.ipp]
                    if not pn.is_outbound_link:
                        raise KeyError
                except KeyError:
                    # We ran out of nodes before hitting the target number of strongly connected nodes.  That means
                    # stuff's still connecting, and there's no need to remove anyone.
                    return
                else:
                    # this neighbor is NOT unwanted.
                    outbound_links.remove(pn.ipp)

                    # Stop once we reach the desired number of outbound links.
                    if pn.strongly_connected:
                        n_alive += 1
                        if n_alive == self.OUTLINK_GOAL:
                            break

            # if links remain in the set, remove them.
            for ipp in outbound_links:
                assert self.remove_outbound_link(ipp)

        if self._chop_excess_links_task and not self._chop_excess_links_task.done():
            return
        self._chop_excess_links_task = asyncio.create_task(chop_excess_links())

    async def _online_timeout(self):
        """
        Shut down the node if it hasn't received a ping from any neighbor in ONLINE_TIMEOUT seconds.
        """
        while (dt := time.monotonic() - self._last_received_time) < ONLINE_TIMEOUT:
            await asyncio.sleep(dt)

        await self.main.show_login_status("Lost Sync!")
        asyncio.create_task(self.main.shutdown(reconnect='normal'))

    async def shutdown(self):
        """
        Shut down the PingManager.  Cancel all pending tasks, delete all neighbor information, and ping outgoing
        links that this node is shutting down.
        """
        if self._chop_excess_links_task:
            self._chop_excess_links_task.cancel()
        if self._make_new_links_task:
            self._make_new_links_task.cancel()
        if self._timeout_task:
            self._timeout_task.cancel()

        outbounds = [pn for pn in self.pnbs.values() if pn.is_outbound_link]

        for pn in self.pnbs.values():
            pn.cancel(force=True)
        self.pnbs.clear()

        await asyncio.gather(*(pn.send_ping(i_req=False) for pn in outbounds))
