"""
Dtella - Main Base Module
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import math
import random
import signal
from asyncio import transports
from typing import Optional, Set, TYPE_CHECKING

import dtella.common as common
from dtella.common.local_config import allowed_subnets
from dtella.common.log import LOG
from dtella.common.icm import InitialContactManager
from dtella.common.packet import BadPacketError
from dtella.common.protocol import BaseProtocol
from dtella.util import word_wrap, CHECK, Ad
from dtella.util.scheduled_task import ScheduledTask

if TYPE_CHECKING:
    from dtella.common.state import StateManager


class DtellaMain_Base(object):
    """
    Base class for the main namespace.

    Attributes:
        _debug: set debug log level
        _loop: event loop
        _start_icm_task: ScheduledTask to start up the ICM
        _reconnection_wait: cached wait time that grows upon successive failed connections
        _transport: transport instance that manages the main dtella socket
        icm: InitialContactManager
        osm: OnlineStateManager

    There are four control flow methods for the main loop:
    - start: blocking call to start the node and the event loop with a call to _run
    - start_connecting:
    """

    # Reconnect time range.  Currently 10sec .. 15min
    RECONNECT_RANGE = (10, 60 * 15)
    state: "StateManager"
    ph: BaseProtocol

    def __init__(self, debug=False):
        self._debug = debug
        self._loop: asyncio.AbstractEventLoop = asyncio.get_event_loop()
        self._start_icm_task: ScheduledTask = ScheduledTask(self._start_icm(), loop=self._loop)
        self._reconnection_wait: float = DtellaMain_Base.RECONNECT_RANGE[0]
        self._transport: Optional[transports.BaseTransport] = None
        self.icm: InitialContactManager = InitialContactManager(self)
        self.osm: Optional[common.OnlineStateManager] = None

        self.myip_reports = []
        self._accept_IQ_trigger = False

        # # TODO find asyncio equivalent
        # # Register a function that runs before shutting down
        # reactor.addSystemEventTrigger('before', 'shutdown', self.cleanup_on_exit)

        # Set to True to prevent this node from broadcasting.
        self.hide_node = False

    # attribute properties

    @property
    def loop(self) -> asyncio.AbstractEventLoop:
        return self._loop

    @property
    def main_port(self) -> int:
        raise NotImplementedError()

    @property
    def state_observer(self):
        """Alias for a DC handler or IRC server."""
        raise NotImplementedError()

    @property
    def accept_IQ_trigger(self) -> bool:
        return self._accept_IQ_trigger

    # state properties

    @property
    def is_working(self) -> bool:
        """Returns true if the node is trying to connect or has an active network connection."""
        return bool(self.icm or self.osm)

    @property
    def _connection_desired(self) -> bool:
        raise NotImplementedError()

    # socket connections

    async def _is_udp_bound(self):
        """Try to bind the UDP port, if it's not already.  Returns whether or not the UDP port is bound."""

        if self._transport and not self._transport.is_closing():
            return True
        try:
            self._transport, _ = await asyncio.get_running_loop().create_datagram_endpoint(
                self.ph, local_addr=("0.0.0.0", self.main_port))
        except OSError:
            return False
        return self._transport and not self._transport.is_closing()

    # output

    def log_packet(self, text: str):
        if self._debug:
            LOG.debug(text)

    async def show_login_status(self, text, counter=None):
        raise NotImplementedError()

    # control flow

    def start(self, *args):
        """Blocking call to run the dtella node."""

        loop = self._loop
        try:
            loop.add_signal_handler(signal.SIGINT, lambda: loop.stop())
            loop.add_signal_handler(signal.SIGTERM, lambda: loop.stop())
        except RuntimeError:
            pass

        try:
            self._loop.create_task(self._run(*args))
            self._loop.run_forever()
        except KeyboardInterrupt:
            LOG.debug("Keyboard interrupt to terminate.")
        finally:
            # try:
            #     # _cancel_all_tasks(loop)
            #     loop.run_until_complete(loop.shutdown_asyncgens())
            #     loop.run_until_complete(loop.shutdown_default_executor())
            # finally:
            #     events.set_event_loop(None)
            #     loop.close()
            pass

    async def _run(self, *args):
        raise NotImplementedError()

    async def _start_icm(self):
        """
        Begin making a connection to the network.  Bind the main port and start the ICM.  Returns 0 if the ICM is
        started successfully.

        Run the ICM and determine a course of action from its results.

        Do not call this method directly; instead use schedule_start_icm.
        """

        if not await self._is_udp_bound() or not self._connection_desired or self.icm or self.osm:
            return

        assert self._start_icm_task.immenent, "_start_icm called without using the scheduler"

        self.ph.remap_ip = None
        result, node_ipps = await self.icm.run()

        if result == 'good':
            asyncio.create_task(self.start_osm(node_ipps))
            return

        reconnect = 'max'
        if result == 'banned_ip':
            await self.show_login_status("Your IP seems to be banned from this network.")

        elif result == 'foreign_ip':
            try:
                ad = self.selectMyIP(allow_bad=True)
            except ValueError:
                ad = "?"

            await self.show_login_status(f"Your IP address ({ad}) is not authorized to use this network.")

        elif result == 'dead_port':
            reconnect = 'dead_port'

        elif result == 'no_nodes':
            await self.show_login_status("No online nodes found.")
            reconnect = 'normal'

            # If we receive an IQ packet after finding no nodes, then assume we're a root node and form an empty network
            if not self.hide_node:
                self._accept_IQ_trigger = True

        else:
            assert False, "control flow error after ICM completed"

        asyncio.create_task(self.shutdown(reconnect))

    def schedule_start_icm(self, delay: float = 0):
        """
        Schedule the initialization of the ICM to start after a given delay.
        """

        if self._start_icm_task.immenent:
            # initial connection in progress or soon to be
            pass
        elif not self._start_icm_task.uncalled:
            # start_icm was either cancelled or done; either way, need to rebuild
            self._start_icm_task = ScheduledTask(self._start_icm(), delay, loop=self._loop)
        elif self._start_icm_task.prevented or delay < self._start_icm_task.remaining:
            self._start_icm_task.reset(delay)

    # TODO method override to splice in OSM with bridge support
    async def start_osm(self, node_ipps: Set[bytes]):
        # Determine my IP address and enable the osm

        assert not self.is_working

        # Reset the reconnect interval
        self._reconnection_wait = DtellaMain_Base.RECONNECT_RANGE[0]

        # Get my address and port
        try:
            my_ipp = self.selectMyIP().get_raw_ip_port()
        except ValueError:
            await self.show_login_status("Can't determine my own IP?!")
            return

        # # Look up my location string
        # if local.use_locations:
        #     self.queryLocation(my_ipp)

        # Enable the object that keeps us online
        node_ipps.discard(my_ipp)
        if node_ipps:
            await self.show_login_status("Joining The Network.", counter='inc')
        else:
            await self.show_login_status("Creating a new empty network.", counter='inc')
        # self.osm = self.build_osm(my_ipp, node_ipps)
        self.osm = common.OnlineStateManager(self, my_ipp, node_ipps)

    async def shutdown(self, reconnect: str):
        """
        Shutdown the node and set a reconnection wait time.  There are 5 different types of shutdowns:

        - 'no': main loop is halted, any remaining tasks are cleaned up, and main exits
        - 'instant': immediately begin reconnecting as soon as shutdown is complete
        - 'normal': schedule a timeout before reconnecting, growing in duration every time the node fails to reconnect
        - 'max': schedule a timeout for the maximum duration before reconnecting
        - 'dead_port': the main port was unable to be bound
        """

        if reconnect == 'dead_port':
            # TODO better message for this.  this method is called whenever no traffic gets through the main port.
            #  could be for a number of different reasons and not just port forwarding
            await self.show_login_status("*** UDP PORT BLOCKED ***")
            text = ("In order for Dtella to communicate properly, it needs to receive UDP traffic from the Internet.  "
                    f"Dtella is currently listening on UDP port {self.state.udp_port}, but the packets appear to not "
                    "be getting through, most likely by a firewall or a router.  If this is the case, then you will "
                    "have to configure your firewall or router to allow UDP traffic through on this port.  You may "
                    "tell Dtella to use a different port from now on by typing !UDP followed by a number.  Note: You "
                    "will also need to unblock or forward a TCP port for your DC++ client to be able to transfer "
                    "files.")
            for line in word_wrap(text):
                await self.show_login_status(line)

        # It's possible for these both to be None, but we still
        # want to reconnect.  (i.e. after an ICM failure)

        if self.is_working:
            await self.show_login_status("Shutting down.")

        self._accept_IQ_trigger = False

        # Shut down InitialContactManager
        if self.icm:
            await self.icm.shutdown()

        # Shut down BaseOnlineStateManager
        if self.osm:
            # Notify any observers that all the nicks are gone.
            if self.osm.synced:
                self.state_change_DtellaDown()
            await self.osm.shutdown()
            self.osm = None

        # Notify some random handlers of the shutdown
        self.afterShutdownHandlers()

        # Schedule a Reconnect (maybe) ...

        if not self._connection_desired or reconnect == 'no':
            self._reconnection_wait = math.inf
        elif reconnect == 'instant':
            self._reconnection_wait = 0
        elif reconnect == 'max' or reconnect == 'dead_port':
            self._reconnection_wait = int(DtellaMain_Base.RECONNECT_RANGE[1] * random.uniform(0.8, 1.2))
        else:
            # exponentially increase the duration
            self._reconnection_wait = min(int(self._reconnection_wait * random.uniform(1.2, 1.8)),
                                          DtellaMain_Base.RECONNECT_RANGE[1])
            self._reconnection_wait = DtellaMain_Base.RECONNECT_RANGE[0]
        self.schedule_start_icm(self._reconnection_wait)

        if self._reconnection_wait:
            await self.show_login_status("--")
            await self.show_login_status(f"Next reconnect attempt in {self._reconnection_wait} seconds.")

    def cleanup_on_exit(self):
        raise NotImplementedError()

    def afterShutdownHandlers(self):
        raise NotImplementedError()

    # state changes

    def stateChange_ObserverUp(self):
        # Called after a DC client / IRC server / etc. has become available.
        osm = self.osm
        if osm and osm.synced:
            self.osm.updateMyInfo()

        # Make sure the observer's still online, because a nick collison
        # in updateMyInfo could have killed it.
        if so := self.state_observer:
            so.event_DtellaUp()

    def stateChange_ObserverDown(self):
        # Called after a DC client / IRC server / etc. has gone away.
        assert not self.state_observer

        osm = self.osm
        if osm and osm.synced:
            osm.updateMyInfo()

            # Cancel all nick-specific messages.
            for n in osm.nodes:
                n.cancel_messages()

    def stateChange_DtellaUp(self):
        """Called after OSM finishes syncing."""

        osm = self.osm
        CHECK(osm and osm.synced)
        osm.updateMyInfo(send=True)

        if so := self.state_observer:
            so.event_DtellaUp()

    def state_change_DtellaDown(self):
        """Called before Dtella network shuts down."""

        if so := self.state_observer:
            for n in self.osm.user_list:
                so.event_RemoveNick(n, "Removing All Nicks")
            so.event_DtellaDown()

    # utility

    def add_my_ip_report(self, from_ad: Ad, my_ad: Ad):
        # from_ad = the IP who sent us this guess
        # my_ad = the IP that seems to belong to us

        CHECK(self.auth('sx', from_ad))

        fromip = from_ad.ip
        myip = my_ad.ip

        # If we already have a report from this fromip in the list, remove it.
        try:
            i = [r[0] for r in self.myip_reports].index(fromip)
            del self.myip_reports[i]
        except ValueError:
            pass

        # Only let list grow to 5 entries
        del self.myip_reports[:-4]

        # Append this guess to the end
        self.myip_reports.append((fromip, myip))

    def selectMyIP(self, allow_bad: bool = False) -> Ad:
        # Out of the last 5 responses, pick the IP that occurs most often.
        # In case of a tie, pick the more recent one.

        # Map from ip -> list of indexes
        ip_hits = {}

        for i, (reporter, ip) in enumerate(self.myip_reports):
            try:
                ip_hits[ip].append(i)
            except KeyError:
                ip_hits[ip] = [i]

        # Sort by (hit count, highest index) descending.
        scores = [(len(hits), hits[-1], ip)
                  for ip, hits in ip_hits.items()]
        scores.sort(reverse=True)

        for hit_count, highest_index, ip in scores:
            ad = Ad((ip, self.state.udp_port))
            if allow_bad or self.auth('sx', ad):
                return ad

        raise ValueError

    def check_source(self, packet_ipp: bytes, ad: Ad, exempt_ip=False):
        """
        Check that a node is reporting its own IP address correctly in packets and that this address is valid to be
        used on the network.
        """

        packet_ad = Ad(packet_ipp)
        if not self.auth('sx' if exempt_ip else 's', packet_ad):
            raise BadPacketError("invalid Source IP")
        elif not self.auth('b', packet_ad):
            raise BadPacketError("source IP banned")
        elif packet_ad.ip != ad.ip:
            raise BadPacketError("source IP mismatch")
        elif self.osm and packet_ipp == self.osm.me.ipp:
            raise BadPacketError("packet came from myself")

        self.state.refresh_peer(packet_ad, 0)
        return packet_ad

    def auth(self, kinds: str, ad: Ad):
        """
        Checks if IP is valid for the network.  There are three types of checks:

        - Bans (b): checks if the IP is banned.  Nodes take ban authority from bridges, and bridges take authority
        from IRC.
        - Exemptions (x): checks if the IP is exempt.  Exempt IPs are authorized by the dynamic config.
        - Static (s): checks if an IP is a member of the local config allowed subnet and not private.
        """
        int_ip = int(ad)

        # TODO this goes into a subclass
        # if 'b' in kinds:
        #     # Bans
        #     if main.osm and main.osm.banm.isBanned(int_ip):
        #         return False

        if 'x' in kinds:
            # Bridge/Cache Exempt IPs
            if ad.ip in self.state.exempt_ips:
                return True

        if 's' in kinds:
            # Static evaluation
            if not allowed_subnets.containsIP(int_ip):
                return False
            if ad.is_private():
                return False

        return True

    async def kickObserver(self, lines, rejoin_time):
        assert (so := self.state_observer)

        # Act as if Dtella is shutting down.
        self.state_change_DtellaDown()

        # Force the observer to go invisible, with a kick message.
        await so.event_KickMe(lines, rejoin_time)

        # Send empty state to Dtella.
        self.stateChange_ObserverDown()

    def queryLocation(self, my_ipp):
        raise NotImplementedError()