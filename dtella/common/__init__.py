"""
Dtella - Common Modules
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import socket

import dtella.common.local_config as local
from dtella.common.log import LOG


class BadTimingError(Exception):
    pass


class MessageCollisionError(Exception):
    pass


class Reject(Exception):
    pass


class NickError(Exception):
    pass


NODE_EXPIRE_EXTEND = 15.0

# Init response codes
CODE_IP_OK = 0
CODE_IP_FOREIGN = 1
CODE_IP_BANNED = 2

# Status Flags
PERSIST_BIT = 0x1

# Chat Flags
SLASHME_BIT = 0x1
NOTICE_BIT = 0x2

# How many seconds our node will stay online without a DC client
NO_CLIENT_TIMEOUT = 60.0 * 5

# Bridge topic change
CHANGE_BIT = 0x1

# Bridge Kick flags
REJOIN_BIT = 0x1

# Bridge general flags
MODERATED_BIT = 0x1


from dtella.common.mrm import MessageRoutingManager
from dtella.common.osm import BaseOnlineStateManager as OnlineStateManager
from dtella.common.pgm import PingManager
from dtella.common.protocol import BaseProtocol as PeerHandler
from dtella.common.sync import SyncManager, SyncRequestRoutingManager
from dtella.common.topic import TopicManager


def overwrite_modules(mods):
    for k, v in mods.items():
        if not issubclass(v, globals()[k]):
            raise ImportError(f"cannot replace {globals()[k].__name__} with {v.__name__}")
        globals()[k] = v


# if local.use_bridge:
#     from dtella.bridge.client.osm import BridgeClientOnlineStateManager as OnlineStateManager
# else:
#     from dtella.common.osm import BaseOnlineStateManager as OnlineStateManager
#
# try:
#     from dtella.bridge.server import bridge_config as bcfg
#
#     from dtella.common.topic import TopicManager
# except ImportError:
#     pass


try:
    from Crypto.PublicKey import _fastmath
except ImportError:
    LOG.warn("Your version of PyCrypto was compiled without GMP (fastmath).  Signing messages will be slower.")


def terminate(dc_port):
    """Terminate another Dtella process on the local machine.  Return true if packet was sent."""
    try:
        print(f"Sending Packet of Death on port {dc_port}...")
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('127.0.0.1', dc_port))
        sock.sendall(b"$KillDtella|")
        sock.close()
    except socket.error:
        return False

    return True
