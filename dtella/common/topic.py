"""
Dtella - Topic Manager
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import struct
from typing import Optional

from dtella.bridge.node import BridgeNode
from dtella.common.node import Node
from dtella.common.packet import BroadcastPacket, register_packet_classes


class PacketTP(BroadcastPacket):
    """
    Topic Packet.
    """

    _code = b"TP"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'I'), ('nhash', '4s'), ('topic', 'S1')]
    __slots__ = ('pktnum', 'nhash', 'topic')
    pktnum: int
    nhash: bytes
    topic: str


register_packet_classes(PacketTP)


class TopicManager(object):

    def __init__(self, main):
        self.main = main
        self.topic = ""
        self.topic_whoset = ""
        self.topic_node: Optional[Node] = None
        self.waiting = True


    def gotTopic(self, n: Node, topic: str):
        self.updateTopic(n, n.nick, topic, changed=True)


    def receivedSyncTopic(self, n, topic):
        # Topic arrived from a YR packet
        if self.waiting:
            self.updateTopic(n, n.nick, topic, changed=False)


    def updateTopic(self, n: Optional[Node], nick: str, topic: str, changed: bool):
        from dtella.client.dc import DCHandler

        # Don't want any more SyncTopics
        self.waiting = False

        # Don't allow a non-bridge node to override a bridge's topic
        if self.topic_node and n:
            if isinstance(self.topic_node, BridgeNode) and not isinstance(n, BridgeNode):
                return False

        # Sanitize the topic
        topic = topic[:255].replace('\r','').replace('\n','')

        # Get old topic
        old_topic = self.topic

        # Store stuff
        self.topic = topic
        self.topic_whoset = nick
        self.topic_node = n

        # Without DC, there's nothing to say
        if not isinstance(dch := self.main.state_observer, DCHandler):
            return True

        # If it's changed, push it to the title bar
        if topic != old_topic:
            dch.push_Hubname(topic)

        # If a change was reported, tell the user that it changed.
        if changed and nick:
            dch.push_Status(f"{nick} changed the topic to: {topic}")

        # If a change wasn't reported, but it's new to us, and it's not
        # empty, then just say what the topic is.
        if not changed and topic and topic != old_topic:
            dch.push_Status(self.getFormattedTopic())

        return True


    def broadcastNewTopic(self, topic):
        osm = self.main.osm

        if len(topic) > 255:
            topic = topic[:255]

        # Update topic locally
        if not self.updateTopic(osm.me, osm.me.nick, topic, changed=True):

            # Topic is controlled by a bridge node
            if isinstance(self.topic_node, BridgeNode) and self.topic_node.sendTopicChange(topic):
                return

        packet = osm.mrm.broadcastHeader('TP', osm.me.ipp)
        packet.append(struct.pack('!I', osm.mrm.getPacketNumber_search()))

        packet.append(osm.me.nick_hash())
        packet.append(struct.pack('!B', len(topic)))
        packet.append(topic)

        osm.mrm.newMessage(''.join(packet), tries=4)


    def getFormattedTopic(self):

        if not self.topic:
            return "There is currently no topic set."

        text = "The topic is: %s" % self.topic

        if self.topic_node and self.topic_node.nick:
            whoset = self.topic_node.nick
        else:
            whoset = self.topic_whoset

        if whoset:
            text += " (set by %s)" % whoset

        return text


    def checkLeavingNode(self, n):
        # If the node who set the topic leaves, wipe out the topic
        if self.topic_node is n:
            self.updateTopic(None, "", "", changed=False)