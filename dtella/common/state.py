"""
Dtella - State File Management Module
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
import asyncio
import os
import struct
import random
import time
import socket
import heapq
from typing import Dict, Set, Tuple, Type, List, Iterable

from dtella.common.log import LOG
from dtella.common.base import DtellaMain_Base
from dtella.util import get_user_path, CHECK
from dtella.util.ipv4 import Ad


class StateError(Exception):
    pass


class StateManager(object):

    def __init__(self, main: DtellaMain_Base, filename: str, load_savers: Tuple[Type["LoadSaver"], ...]):
        self._main: DtellaMain_Base = main
        self._filename: str = get_user_path(filename)
        self._load_savers: Set[LoadSaver] = set(ls() for ls in load_savers)
        self._task: asyncio.Task = main.loop.create_task(self._save())

        self.persistent: bool = False
        self.localsearch: bool = False
        self.udp_port: int = 0
        self.suffix: str = ""
        self.peers: Dict[bytes, float] = {}
        self.exempt_ips: Set[bytes] = set()
        self.dns_ipcache: Tuple[int, List[bytes]] = (0, [])
        self.dns_pkhashes: Set[bytes] = set()

    @property
    def filename(self) -> str:
        return self._filename

    def _load(self):
        d: Dict[str, bytes] = {}

        try:
            with open(self._filename, 'rb') as f:
                header, n_keys = struct.unpack("!6sI", f.read(10))
                if header != b"DTELLA":
                    raise StateError("header mismatch")

                for i in range(n_keys):
                    k_len, = struct.unpack("!I", f.read(4))
                    k = f.read(k_len).decode()
                    v_len, = struct.unpack("!I", f.read(4))
                    v = f.read(v_len)

                    if len(k) != k_len or len(v) != v_len:
                        raise StateError("unexpected EOF reached")
                    elif k in d:
                        raise StateError("duplicate key in state file")

                    d[k] = v

                if f.read(1):
                    raise StateError("expected EOF")
        except FileNotFoundError:
            LOG.info(f'state file "{self._filename}" not found, creating new')
        except (struct.error, IOError, OSError, StateError) as e:
            LOG.error(f'{e.__class__.__name__}: {e.args[0]}.  Backing up state file to "{self._filename}.backup" and '
                      f'starting a new state file')
            if os.path.exists(f"{self._filename}.backup"):
                raise FileExistsError("can't backup existing state file")
            os.rename(self._filename, f"{self._filename}.backup")
            d = {}

        for ls in self._load_savers:
            ls.load(self, d)

    async def _save(self):
        """Scheduled task to save every few minutes."""
        self._load()
        while True:
            await asyncio.sleep(random.uniform(5*60, 6*60))
            self.save()

    def save(self):
        d: Dict[str, bytes] = {}
        # Store all state data to dictionary
        for ls in self._load_savers:
            ls.save(self, d)

        try:
            with open(self._filename, "wb") as f:
                f.write(struct.pack("!6sI", b"DTELLA", len(d)))
                for k in sorted(d):
                    v = d[k]
                    f.write(struct.pack("!I", len(k)))
                    f.write(k.encode())
                    f.write(struct.pack("!I", len(v)))
                    f.write(v)
        except (IOError, OSError):
            LOG.error("could not write to state file")
        except Exception as e:
            LOG.error("error writing state file")
            LOG.error(f"{e.__class__.__name__}: {e.args[0]}")

    def add_exempt_ip(self, ad: Ad):
        # If this is an offsite IP, add it to the exempt list.
        if not self._main.auth('s', ad):
            self.exempt_ips.add(ad.ip)

    def get_youngest_peers(self, n: int):
        # Return a list of (time, ipp) pairs for the N youngest peers
        peers = zip(self.peers.values(), self.peers.keys())
        return heapq.nlargest(n, peers)

    def refresh_peer(self, ad: Ad, age: float) -> bool:
        # Call this to update the age of a cached peer

        if not ad.port:
            return False
        if not self._main.auth('sx', ad):
            return False

        ipp = ad.get_raw_ip_port()
        updated = False

        if age < 0:
            age = 0
        seen = time.time() - age

        try:
            old_seen = self.peers[ipp]
        except KeyError:
            self.peers[ipp] = seen
        else:
            if seen > old_seen:
                self.peers[ipp] = seen
                updated = True

        # Truncate the peer cache if it grows too large
        target = 100

        if self._main.osm:
            target = max(target, len(self._main.osm))

        if len(self.peers) > target * 1.5:
            keep = set([ipp for when, ipp in self.get_youngest_peers(target)])

            for ipp in self.peers.keys():
                if ipp not in keep:
                    del self.peers[ipp]
        return updated

    def set_dns_ip_cache(self, data: bytes):

        CHECK(len(data) % 6 == 4)

        # 2011-08-21: New nodes ignore the value of 'when'.
        when, = struct.unpack("!I", data[:4])
        ipps = [data[i:i+6] for i in range(4, len(data), 6)]
        
        self.dns_ipcache = when, ipps

        # If DNS contains a foreign IP, add it to the exemption
        # list, so that it can function as a bridge or cache node.

        self.exempt_ips.clear()

        for ipp in ipps:
            ad = Ad(ipp)
            self.add_exempt_ip(ad)


##############################################################################


class LoadSaver(object):

    key: str = ''

    def load(self, state: StateManager, d: Dict[str, bytes]) -> None:
        raise NotImplementedError()

    def save(self, state: StateManager, d: Dict[str, bytes]) -> None:
        raise NotImplementedError()

    def get_key(self, d: Dict[str, bytes]) -> bytes:
        try:
            return d[self.key]
        except KeyError:
            raise StateError("Not Found")

    def set_key(self, d: Dict[str, bytes], value: bytes) -> None:
        d[self.key] = value

    def unpack_value(self, d: Dict[str, bytes], fmt: str):
        try:
            v, = struct.unpack('!' + fmt, self.get_key(d))
            return v
        except (struct.error, ValueError):
            raise StateError("Can't get value")

    def pack_value(self, d: Dict[str, bytes], fmt: str, data) -> None:
        self.set_key(d, struct.pack('!' + fmt, data))

    def unpack_strs(self, d: Dict[str, bytes]) -> List[bytes]:
        strs = []
        i = 0

        data = self.get_key(d)

        while i < len(data):
            slen, = struct.unpack("!I", data[i:i+4])
            i += 4
            s = data[i:i+slen]
            i += slen
            if len(s) != slen:
                return []
            strs.append(s)

        return strs

    def pack_strs(self, d: Dict[str, bytes], strs: Iterable[bytes]):
        data: List[bytes] = []
        for s in strs:
            data.append(struct.pack("!I", len(s)))
            data.append(s)

        self.set_key(d, b''.join(data))


class Persistent(LoadSaver):
    """
    Boolean for whether or not the node should stay connected to the network even if no DC connection is present.
    Used by clients only.
    """

    key = 'persistent'

    def load(self, state: StateManager, d: Dict[str, bytes]) -> None:
        try:
            state.persistent = bool(self.unpack_value(d, 'B'))
        except StateError:
            state.persistent = False

    def save(self, state: StateManager, d: Dict[str, bytes]) -> None:
        self.pack_value(d, 'B', bool(state.persistent))


class LocalSearch(LoadSaver):

    key = 'localsearch'

    def load(self, state: StateManager, d: Dict[str, bytes]) -> None:
        try:
            state.localsearch = bool(self.unpack_value(d, 'B'))
        except StateError:
            state.localsearch = True

    def save(self, state: StateManager, d: Dict[str, bytes]) -> None:
        self.pack_value(d, 'B', bool(state.localsearch))


class UDPPort(LoadSaver):

    key = 'udp_port'

    def load(self, state: StateManager, d: Dict[str, bytes]) -> None:
        
        try:
            state.udp_port = self.unpack_value(d, 'H')
            
        except StateError:
            # Pick a random UDP port to use.  Try a few times.
            for i in range(8):
                state.udp_port = random.randint(1024, 65535)
                
                try:
                    # See if the randomly-selected port is available
                    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    s.bind(('', state.udp_port))
                    s.close()
                    break
                except socket.error:
                    pass

    def save(self, state: StateManager, d: Dict[str, bytes]) -> None:
        self.pack_value(d, 'H', state.udp_port)


class IPCache(LoadSaver):

    key = 'ipcache'

    def load(self, state: StateManager, d: Dict[str, bytes]) -> None:
        # Get IP cache
        try:
            ipcache = self.get_key(d)
            if len(ipcache) % 10 != 0:
                raise StateError
        except StateError:
            ipcache = ''

        now = time.time()

        for i in range(0, len(ipcache), 10):
            ipp, when = struct.unpack('!6sI', ipcache[i:i+10])
            state.refresh_peer(Ad().set_addr(ipp), now - when)

    def save(self, state: StateManager, d: Dict[str, bytes]) -> None:
        ipcache = [struct.pack('!6sI', ipp, int(when)) for when, ipp in state.get_youngest_peers(128)]
        self.set_key(d, b''.join(ipcache))


class Suffix(LoadSaver):

    key = 'suffix'

    def load(self, state: StateManager, d: Dict[str, bytes]) -> None:
        try:
            state.suffix = self.get_key(d)[:8].decode()
        except StateError:
            state.suffix = ""

    def save(self, state: StateManager, d: Dict[str, bytes]) -> None:
        self.set_key(d, state.suffix.encode())


class DNSIPCache(LoadSaver):

    key = 'dns_ipcache'

    def load(self, state: StateManager, d: Dict[str, bytes]) -> None:

        # Get saved DNS ipcache
        try:
            dns_ipcache = self.get_key(d)
            if len(dns_ipcache) % 6 != 4:
                raise StateError
        except StateError:
            state.set_dns_ip_cache(b'\0\0\0\0')
        else:
            state.set_dns_ip_cache(dns_ipcache)

    def save(self, state: StateManager, d: Dict[str, bytes]) -> None:
        when, ipps = state.dns_ipcache
        d['dns_ipcache'] = struct.pack('!I', when) + b''.join(ipps)


class DNSPkHashes(LoadSaver):

    key = 'dns_pkhashes'

    def load(self, state: StateManager, d: Dict[str, bytes]) -> None:

        # Get saved DNS pkhashes
        try:
            state.dns_pkhashes = set(self.unpack_strs(d))
        except StateError:
            state.dns_pkhashes = set()

    def save(self, state: StateManager, d: Dict[str, bytes]) -> None:
        self.pack_strs(d, state.dns_pkhashes)


client_load_savers = (Persistent, LocalSearch, UDPPort, IPCache, Suffix, DNSIPCache, DNSPkHashes)
bridge_load_savers = (IPCache,)
