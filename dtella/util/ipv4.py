"""
Dtella - IPv4 Address Manipulation Functions
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

# Note that there's no IPv6 stuff here.  If Dtella and IPv6 are ever popular
# at the same time, we'll have to redesign the protocol to handle it.
from typing import Tuple, Union, TYPE_CHECKING, Optional, Iterable

import bisect
import struct
import socket
from socket import inet_aton, inet_ntoa

if TYPE_CHECKING:
    from dtella.common.base import DtellaMain_Base


class Ad(object):
    """
    Class for holding an IP:Port and converting to/from various formats.
    """

    __slots__ = ("_ip", "_port", "orig_ip")

    def __init__(self, addr: Union[bytes, str, int, Tuple[Union[str, bytes], int], None] = None):
        self._ip: bytes = b'\00\00\00\00'
        self._port: int = 0
        self.orig_ip: Optional[bytes] = None  # If an address gets NAT-remapped, this is the original.

        if addr is not None:
            self.set_addr(addr)

    def __eq__(self, other: "Ad"):
        return self._ip == other._ip and self._port == other._port

    @property
    def ip(self):
        return self._ip

    @property
    def port(self):
        return self._port

    def remap(self) -> "Ad":
        """Replace the IP with the original IP, if it's known."""
        if self.orig_ip is not None:
            self._ip = self.orig_ip
        return self

    def is_private(self):
        # Return True for an IP in RFC1918 space, except for parts of
        # RFC1918 which have been declared as local.
        from dtella.common.local_config import allowed_subnets
        int_ip = self.get_int_ip()
        return rfc1918_matcher.containsIP(int_ip) and not allowed_subnets.containsIP(int_ip)

    def set_addr(self, addr: Union[bytes, str, int, Tuple[Union[str, bytes], int]]):
        """
        Set the IP:Port address from the following formats:
        - str: e.g. "123.45.67.89" or "123.45.67.89:9001"
        - bytes: e.g. b"\x7b\x2d\x43\x59" or b"\x7b\x2d\x43\x59\x23\x29"
        - int: e.g. 0x7b2d4359 (which translates to 123.45.67.89)
        - (ip, port) tuple
        """
        if isinstance(addr, str) or isinstance(addr, tuple):
            if isinstance(addr, tuple):
                ip, port = addr
            elif ':' in addr:
                ip, port = addr.split(':', 2)
            else:
                ip, port = addr, None

            if port is not None:
                port = int(port)
                if not 0 <= port < 65536:
                    raise ValueError("port out of range")
                self._port = port

            if isinstance(ip, bytes):
                self._ip = ip
            elif ip.count('.') != 3:
                raise ValueError("wrong number of octets")
            else:
                try:
                    self._ip = inet_aton(ip)
                except socket.error:
                    raise ValueError("can't parse IP")
        elif isinstance(addr, bytes):
            if len(addr) == 6:
                try:
                    self._ip, self._port = struct.unpack('!4sH', addr)
                except struct.error:
                    raise ValueError("not a valid 6-byte string")
            elif len(addr) == 4:
                self._ip = addr
            else:
                raise ValueError("not a valid byte string for IP:Port")
        elif isinstance(addr, int):
            try:
                self._ip = struct.pack('!I', addr)
            except struct.error:
                raise ValueError("not a valid IP integer")

        return self

    def get_text_ip(self) -> str:
        return inet_ntoa(self._ip)

    def get_addr_tuple(self) -> Tuple[str, int]:
        return inet_ntoa(self._ip), self._port

    def get_text_ip_port(self) -> str:
        return f"{inet_ntoa(self._ip)}:{self._port}"

    def get_raw_ip_port(self) -> bytes:
        return self._ip + struct.pack('!H', self._port)

    def get_int_ip(self) -> int:
        return struct.unpack('!I', self._ip)[0]

    def get_int_tuple_ip(self) -> Tuple[int, int, int, int]:
        return tuple(self._ip)

    __str__ = get_text_ip_port
    __bytes__ = get_raw_ip_port
    __int__ = get_int_ip


# Convert 24 -> 0xFFFFFF00
def CidrNumToMask(num: int) -> int:
    if 0 <= num <= 32:
        return (0xFFFFFFFF << (32 - num)) & 0xFFFFFFFF
    else:
        raise ValueError("CIDR number out of range")


# Convert 0xFFFFFF00 -> 24
def MaskToCidrNum(mask: int) -> int:
    subnet = 0
    b = ~0 << 31
    while ((b & mask) == b) and (subnet < 32):
        b >>= 1
        subnet += 1

    if subnet == 0 and mask != 0:
        raise ValueError("Not a valid subnet mask")

    return subnet


# Convert (ip, mask) to a "1.2.3.4/5" string
# Might raise ValueError.
def IPMaskToCidrString(ipmask: Tuple[int, int]) -> str:
    ip, mask = ipmask
    return "%s/%d" % (Ad().set_addr(ip).get_text_ip(), MaskToCidrNum(mask))


IPMask = Tuple[int, int]
IPMaskArg = Union[int, str, bytes, Ad, IPMask]


def build_ip_mask(value: IPMaskArg) -> Tuple[int, int]:
    """Convert various input into (ip, mask) ints.  Might raise ValueError."""
    if isinstance(value, int):
        return value, 0xFFFFFFFF
    elif isinstance(value, str):
        try:
            ip, subnet = value.split('/', 1)
        except ValueError:
            ip, subnet = value, "32"
        return int(Ad(ip)), CidrNumToMask(int(subnet))
    elif isinstance(value, bytes):
        if len(bytes) != 4:
            raise ValueError("not a valid 4-byte IP string")
        return struct.unpack('!I', value)[0], 0xFFFFFFFF
    elif isinstance(value, Ad):
        return value.get_int_ip(), 0xFFFFFFFF

    raise ValueError(f"cannot make IP mask with {value}")


CidrStringToIPMask = build_ip_mask


# Test if an IP is a member of a subnet, or if a subnet is a subset of another
# subnet.  Both arguments are (ip, mask) tuples.  A plain IP address should
# have a mask of ~0, which corresponds to a /32.
def IsSubsetOf(candidate: IPMask, group: IPMask) -> bool:
    c_ip, c_mask = candidate
    g_ip, g_mask = group

    # If the candidate is less specific than the group (i.e. the candidate
    # mask has fewer bits), then it can't be a subset.
    if (c_mask & g_mask) != g_mask:
        return False

    # Candidate is a subset if their prefixes are equal.
    return (c_ip & g_mask) == (g_ip & g_mask)


class SubnetMatcher(object):
    """
    Container for a collection of subnets as a list in mutually exclusive CIDR notation.  With the heirarchy that CIDR
    notation creates, subnets can be added, but they cannot be removed.

    IP lookups are O(log n) because of binary search.  In order for binary search to work correctly,
    the class maintains some invariants:

    - The list of (ip, mask) tuples is kept in sorted order.
    - On insertion, any stray bits after the mask are stripped from the ip.
    - When a subnet is added that is a superset of existing subnets, those existing subnets are deleted.

    All this basically means that we keep a sorted list of prefixes, and we can quickly search for the one prefix
    that might match a given IP.  Once the prefix is found, we use the IsSubsetOf() function to see if the IP does in
    fact have that prefix.
    """

    def __init__(self, initial_ranges: Optional[Iterable[IPMaskArg]] = None):
        self._nets = []
        if initial_ranges:
            for r in initial_ranges:
                self.add_range(r)

    def add_range(self, ip_mask: IPMaskArg):
        if not isinstance(ip_mask, tuple):
            ip_mask = build_ip_mask(ip_mask)

        ip, mask = ip_mask
        ip_mask = (ip & mask, mask)

        # See if this range is already covered.
        if ip_mask in self:
            return

        # Delete any existing ranges that are covered by this new range.
        for i in range(len(self._nets) - 1, -1, -1):
            if IsSubsetOf(self._nets[i], ip_mask):
                del self._nets[i]

        # Insert the new range
        bisect.insort_right(self._nets, ip_mask)

    def __contains__(self, item: IPMaskArg) -> bool:
        if not isinstance(item, tuple):
            item = build_ip_mask(item)

        if isinstance(item, tuple) and len(item) == 2:
            # skip the binary search when there's only one element, because searching through signed space is
            # incompatible with the /0 range.
            i = 0
            if len(self._nets) != 1:
                i = bisect.bisect_right(self._nets, item) - 1
                if i < 0:
                    return False
            return IsSubsetOf(item, self._nets[i])
        return False

    containsRange = __contains__
    containsIP = __contains__

    def clear(self):
        del self._nets[:]


# Create a subnet matcher for RFC1918 addresses.
rfc1918_matcher = SubnetMatcher(['10.0.0.0/8', '172.16.0.0/12', '192.168.0.0/16'])
