"""
Dtella - Delayed, Cancellable Call Scheduler for Asyncio
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
from collections import UserDict
from math import inf
from typing import Optional


class ScheduledTask(object):
    """
    A wrapper around an asyncio.Task to delay the execution of a task for a period of time.
    """

    PENDING = 0
    PREVENTED = 1
    IMMENENT = 2
    CANCELLED = 3
    DONE = 4

    def __init__(self, coro, delay: float = inf, *, loop: Optional[asyncio.AbstractEventLoop] = None):
        self._loop: asyncio.AbstractEventLoop = asyncio.get_running_loop() if loop is None else loop
        self._coro = coro

        self._state: int = ScheduledTask.PENDING
        self._when: float = self._loop.time() + delay
        self._delay: asyncio.Task = self._loop.create_task(self._delayed_task(delay))

    @property
    def when(self) -> float:
        """Returns when the task will be executed with respect to self._loop.time().  Returns inf if never."""
        return self._when

    @property
    def remaining(self) -> float:
        """Returns the amount of time left before task is executed.  Returns inf if forever."""
        return self._when - self._loop.time()

    @property
    def running(self):
        return self._when < inf

    @property
    def immenent(self) -> bool:
        return self._state == ScheduledTask.IMMENENT

    @property
    def prevented(self) -> bool:
        return self._state == ScheduledTask.PREVENTED

    @property
    def cancelled(self) -> bool:
        return self._state == ScheduledTask.CANCELLED

    @property
    def uncalled(self) -> bool:
        return self._state == ScheduledTask.PENDING or self._state == ScheduledTask.PREVENTED

    @property
    def done(self) -> bool:
        """Returns a boolean for if the task has completed."""
        return self._delay.done()

    def result(self):
        return self._delay.result()

    def exception(self):
        return self._delay.exception()

    def reset(self, delay: float = inf) -> bool:
        """Reset the delay for execution of the task.  Return true if the task was rescheduled."""

        if self._state == ScheduledTask.PENDING or self._state == ScheduledTask.PREVENTED:
            self._delay.cancel()
            self._when = self._loop.time() + delay
            self._delay = self._loop.create_task(self._delayed_task(delay))
            self._state = ScheduledTask.PENDING
            return True
        return False

    def cancel(self):
        """Cancel the task.  Returns true if the task had not been scheduled yet."""

        if self._state == ScheduledTask.PENDING:
            self._delay.cancel()
            self._state = ScheduledTask.PREVENTED
            return True
        elif self._state == ScheduledTask.PREVENTED:
            return True
        elif self._state == ScheduledTask.IMMENENT:
            self._delay.cancel()
            self._state = ScheduledTask.CANCELLED
        return False

    async def _delayed_task(self, delay: float):
        await asyncio.sleep(delay)
        self._state = ScheduledTask.IMMENENT
        result = await self._coro
        self._state = ScheduledTask.DONE
        return result

    def __await__(self):
        return self._delay.__await__()


class ScheduledEvent(object):
    """
    An asyncio.Event like object that sets its internal flag after a delay.
    """

    def __init__(self, delay: float = inf, *, loop: Optional[asyncio.AbstractEventLoop] = None):
        self._loop: asyncio.AbstractEventLoop = asyncio.get_running_loop() if loop is None else loop

        self._flag: bool = False
        self._when: float = self._loop.time() + delay
        self._delay: asyncio.Task = self._loop.create_task(self._delayed_set(delay))

    @property
    def when(self) -> float:
        """Returns when the task will be executed with respect to self._loop.time().  Returns inf if never."""
        return self._when

    @property
    def remaining(self) -> float:
        """Returns the amount of time left before task is executed.  Returns inf if forever."""
        return self._when - self._loop.time()

    @property
    def running(self):
        return self._when < inf

    @property
    def is_set(self):
        return self._flag

    def reset(self, delay: float = inf):
        """Reset the internal flag and start the delay again."""
        self._delay = self._loop.create_task(self._delayed_set(delay))

    def cancel(self):
        self._flag = True
        self._delay.cancel()

    async def _delayed_set(self, delay: float):
        self._flag = False
        await asyncio.sleep(delay)
        self._flag = True


class ExpiringDictError(Exception):
    pass


class ExpiringDict(UserDict):

    def __init__(self, data=None, /, expire=inf, **kwargs):
        self._expire = expire
        self._expirations = {}
        super().__init__(data, **kwargs)

    def set_expire(self, expire):
        self._expire = expire

    def refresh(self, key, expire=None):
        if key not in self:
            raise KeyError(key)
        return self._expirations[key].reset(self._expire if expire is None else expire)

    async def _delete(self, key):
        del self[key]

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        if key in self._expirations:
            self._expirations[key].cancel()
        self._expirations[key] = ScheduledTask(self._delete(key), delay=self._expire)

    def __delitem__(self, key):
        super().__delitem__(key)
        self._expirations[key].cancel()
        del self._expirations[key]

    def __iter__(self):
        raise ExpiringDictError("cannot iterate over a dictionary that may mutate")

    def clear(self):
        # UserDict.clear() uses __iter__, so work around it.
        for k in self.data:
            self._expirations[k].cancel()
            del self._expirations[k]
        self.data.clear()
