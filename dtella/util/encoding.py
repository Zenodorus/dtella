"""
Dtella - Packet Encoder/Decoder
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

"""
The functions listed here are part of the encoding scheme for converting Python objects into raw data to transmit
across the Dtella network or to save to the state file.  Data that can be encoding using this scheme fall into one
of the following categories:

- a fixed length byte-string
- a variable length byte-string
- a fixed size integer
- a tuple containing fixed length byte-strings and fixed size integers
- a variable length array of fixed length byte-strings
- a variable length array of variable length byte-strings
- a variable length array of fixed size integers
- a variable length array of tuples containing fixed length byte-strings and fixed size integers

NOTE:  variable length strings cannot be in a tuple.

Variable length arrays come in two varieties: a byte or a short at the start of the data structure to state how many
items are in the array.  Moving forward, this should be standardized to be the short, but for backwards compatibility
for the current structure of packets in the older versions of Dtella, both will be supported.

All format codes follow the standard struct codes, with two additions:

- Codes starting with a '+' denote a variable length array, with the following struct code describing the format
  of each entry, e.g., '+I' is an array of unsigned ints.  In addition, if more than one struct format code is
  used in combination with '+', then the result will be a variable length array of tuples, with the tuples
  specified by the code after the '+', e.g., '+6sI' is an array of 2-tuples containing a 6 character string and
  a 4 byte integer.
- The 'S' code is used to denote a variable length string.  'S' may be combined with the above rule as '+S' to
  denote an array of variable length strings.  Any other use of 'S' outside of 'S' or '+S' (for example,
  '+SI') will result in error.

In addition, as this is for a network based protocol, all structs pack data following big endian byte order.
"""


import functools
import struct
from typing import Union, Tuple, Iterable, Sequence


DecodedItem = Union[bytes, int, str, Iterable[Union[bytes, int]]]
DecodedArray = Sequence[Union[bytes, int, Iterable[Union[bytes, int]]]]


class DecodingError(Exception):
    pass


class EncodingError(Exception):
    pass


def decode_item(fmt: str, data: bytes) -> Tuple[DecodedItem, bytes]:
    """
    Decode raw data into a Python object following the given struct format.  Return the unpacked value and leftover
    data as a tuple.
    """
    if fmt == 'S':
        return decode_string(data)
    elif fmt == 'S1':
        return decode_string1(data)
    elif fmt == 'S2':
        return decode_string2(data)

    size = struct.calcsize(f'!{fmt}')
    try:
        value = struct.unpack(f'!{fmt}', data[:size])
    except struct.error:
        raise DecodingError("cannot decode data")
    else:
        if len(value) == 1:
            value = value[0]
        data = data[size:]
    return value, data


def decode_string(data: bytes, len_bytes_size: int = 1) -> Tuple[str, bytes]:
    """
    Decode raw data into a string.  Return the unpacked value and leftover data as a tuple.
    """
    try:
        if len_bytes_size == 1:
            str_len, = struct.unpack('!B', data[:1])
        elif len_bytes_size == 2:
            str_len, = struct.unpack('!H', data[:2])
        else:
            raise DecodingError("unsupported integer type for size of array")
    except struct.error:
        raise DecodingError("cannot decode array size")
    else:
        data = data[len_bytes_size:]

    return data[:str_len].decode(), data[str_len:]


def decode_array(fmt: str, data: bytes, len_bytes_size: int = 2) -> Tuple[Union[DecodedArray, bytes], bytes]:
    """
    Decode raw data into a Python list following the given struct format.  Return the list of unpacked values and
    leftover data as a tuple.

    len_bytes_size is a int passed for the size of the int at the start of the array that gives the length of the array.
    """
    # TODO '+S' code
    if fmt[0] != '+':
        raise DecodingError("expected '+' in struct format code")
    elif fmt[1] == '+':
        len_bytes_size = 2
        fmt = fmt[2:]
    else:
        fmt = fmt[1:]

    try:
        if len_bytes_size == 2:
            array_len, = struct.unpack('!H', data[:2])
        elif len_bytes_size == 1:
            array_len, = struct.unpack('!B', data[:1])
        else:
            raise DecodingError("unsupported integer type for size of array")
    except struct.error:
        raise DecodingError("cannot decode array size")
    else:
        data = data[len_bytes_size:]

    # if the format code is 's', then return a byte string
    if fmt == 's':
        return struct.unpack(f'!{array_len}s', data[:array_len])[0], data[array_len:]

    entry_size = struct.calcsize(f'!{fmt}')
    try:
        decoded = [struct.unpack(f'!{fmt}', data[i:i + entry_size])
                   for i in range(0, array_len * entry_size, entry_size)]
    except struct.error:
        raise DecodingError("cannot decode array elements.")
    else:
        # instead of a list of length 1 tuples, convert to just the raw versions of the elements
        if decoded and len(decoded[0]) == 1:
            for i in range(array_len):
                decoded[i] = decoded[i][0]
        rest = data[array_len * entry_size:]

    return decoded, rest


def encode_item(fmt: str, value: DecodedItem) -> bytes:
    """
    Encode a Python object into raw data given the struct format for each item.
    """
    if fmt == 'S':
        return encode_string(value)
    elif fmt == 'S1':
        return encode_string1(value)
    elif fmt == 'S2':
        return encode_string2(value)

    try:
        if isinstance(value, bytes) or isinstance(value, int):
            return struct.pack(f'!{fmt}', value)
        else:
            return struct.pack(f'!{fmt}', *value)
    except struct.error:
        raise EncodingError("cannot encode data")


def encode_string(s: str, len_bytes_size: int = 2) -> bytes:
    try:
        if len_bytes_size == 1:
            return struct.pack('!B', len(s)) + s.encode()
        elif len_bytes_size == 2:
            return struct.pack('!H', len(s)) + s.encode()
        else:
            raise EncodingError("unsupported integer type for size of array")
    except struct.error:
        raise EncodingError("cannot encode array length")


def encode_array(fmt: str, array: Union[DecodedArray, bytes], len_bytes_size: int = 2) -> bytes:
    """
    Encode a Python list into raw data given the struct format for each item.

    len_bytes_size is a int passed for the size of the int at the start of the array that gives the length of the array.
    """
    # TODO '+S' code
    if fmt[0] != '+':
        raise DecodingError("expected '+' in struct format code")
    elif fmt[1] == '+':
        len_bytes_size = 2
        fmt = fmt[2:]
    else:
        fmt = fmt[1:]

    if fmt != 's' and not isinstance(array, Sequence):
        raise EncodingError(f"expected array argument to be a list for format code '+{fmt}'")

    array_len = len(array)
    try:
        if len_bytes_size == 2:
            data = struct.pack('!H', array_len)
        elif len_bytes_size == 1:
            data = struct.pack('!B', array_len)
        else:
            raise EncodingError("unsupported integer type for size of array")
    except struct.error:
        raise EncodingError("cannot encode array length")

    try:
        if array_len == 0:
            pass
        elif fmt == 's':
            data += struct.pack(f'!{array_len}s', array)
        elif isinstance(array[0], Iterable) and not isinstance(array[0], bytes):
            data += b''.join(struct.pack(f'!{fmt}', *item) for item in array)
        else:
            data += b''.join(struct.pack(f'!{fmt}', item) for item in array)
        return data
    except struct.error:
        raise EncodingError("cannot encode array element")


# ##########  Patch Functions for 1.2.7 Compatibility  ##########

decode_array1 = functools.partial(decode_array, len_bytes_size=1)
encode_array1 = functools.partial(encode_array, len_bytes_size=1)
decode_string1 = functools.partial(decode_string, len_bytes_size=1)
decode_string2 = functools.partial(decode_string, len_bytes_size=2)
encode_string1 = functools.partial(encode_string, len_bytes_size=1)
encode_string2 = functools.partial(encode_string, len_bytes_size=2)


# ##########  Old encoding functions from PeerHandler  ##########
#
# def decode_packet(fmt, data):
#     if fmt[-1] == '+':
#         fmt = fmt[:-1]
#         size = struct.calcsize(fmt)
#         rest = (data[size:],)
#         data = data[:size]
#     else:
#         rest = ()
#
#     try:
#         parts = struct.unpack(fmt, data)
#     except struct.error:
#         raise BadPacketError("Can't decode packet")
#
#     return parts + rest
#
#
# def decode_string1(data, factor=1):
#     try:
#         length, = struct.unpack('!B', data[:1])
#     except struct.error:
#         raise BadPacketError("Can't decode 1string")
#
#     length *= factor
#
#     if len(data) < 1 + length:
#         raise BadPacketError("Bad 1string length")
#
#     return data[1:1 + length], data[1 + length:]
#
#
# def decode_string2(data, max_len=1024):
#     try:
#         length, = struct.unpack('!H', data[:2])
#     except struct.error:
#         raise BadPacketError("Can't decode 2string")
#
#     if length > max_len or len(data) < 2 + length:
#         raise BadPacketError("Bad 2string length")
#
#     return data[2:2 + length], data[2 + length:]
#
#
# def decode_chunk_list(fmt, data):
#     size = struct.calcsize(fmt)
#
#     try:
#         return [struct.unpack(fmt, data[i:i + size])
#                 for i in range(0, len(data), size)]
#     except struct.error:
#         raise BadPacketError("Can't decode chunk list")
