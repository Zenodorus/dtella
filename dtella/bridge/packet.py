"""
Dtella - Bridge Packet Structures
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

from typing import List

from dtella.common import PERSIST_BIT
from dtella.common.packet import Packet, BroadcastPacket, AckablePacket
from dtella.util.encoding import encode_array1, decode_array1


class PacketbY(Packet):
    """
    Bridge Sync Reply packet.
    """

    _code = b'bY'
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('pktnum', 'Q'), ('expire', 'H'), ('sesid', '4s'),
                   ('uptime', 'I'), ('flags', 'B'), ('hashes', '+16s'), ('pubkey', '++s'), ('c_nbs', '+6s'),
                   ('u_nbs', '+6s')]
    _encode_array = encode_array1
    _decode_array = decode_array1
    _signed = True
    __slots__ = ('src_ipp', 'pktnum', 'expire', 'sesid', 'uptime', 'flags', 'hashes', 'pubkey', 'c_nbs', 'u_nbs')

    src_ipp: bytes
    pktnum: int
    expire: int
    sesid: bytes
    uptime: int
    flags: int
    hashes: List[bytes]
    pubkey: bytes
    c_nbs: List[bytes]
    u_nbs: List[bytes]
    signature: bytes

    def __init__(self, **kwargs):
        self.flags = 0x00
        Packet.__init__(self, **kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    persist = property(lambda self: PacketbY._get_flag(self, PERSIST_BIT),
                       lambda self, v: PacketbY._set_flag(self, PERSIST_BIT, v))


class PacketBS(BroadcastPacket):

    _code = b"BS"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'Q'), ('expire', 'H'), ('sesid', '4s'), ('uptime', 'I'), ('flags', 'B'),
                   ('hashes', '+16s'), ('pubkey', '++s')]
    _encode_array = encode_array1
    _decode_array = decode_array1
    _signed = True
    __slots__ = ('pktnum', 'expire', 'sesid', 'uptime', 'flags', 'hashes', 'pubkey')
    pktnum: int
    expire: int
    sesid: bytes
    uptime: int
    flags: int
    hashes: List[bytes]
    pubkey: bytes

    def __init__(self, **kwargs):
        self.flags = 0x00
        BroadcastPacket.__init__(self, **kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    persist = property(lambda self: PacketBS._get_flag(self, PERSIST_BIT),
                       lambda self, v: PacketBS._set_flag(self, PERSIST_BIT, v))


class PacketBB(BroadcastPacket):

    _code = b"BB"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'Q'), ('blockdata', '++s')]
    __slots__ = ('pktnum', 'blockdata')
    pktnum: int
    blockdata: bytes


class PacketBC(BroadcastPacket):

    _code = b"BC"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'Q'), ('chunks', '++s')]
    _signed = True
    __slots__ = ('pktnum', 'chunks')
    pktnum: int
    chunks: bytes


class PacketBX(BroadcastPacket):
    """
    Bridge Exit Packet.
    """

    _code = b"BX"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'Q')]
    _signed = True
    __slots__ = ('pktnum')
    pktnum: int


class PacketbC(AckablePacket):
    """
    Bridge Chunks Packet.
    """

    _code = b"bC"
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('ack_key', '8s'), ('dst_nhash', '4s'), ('chunks', '++s')]
    _signed = True
    __slots__ = ('src_ipp', 'dst_nhash', 'chunks')
    src_ipp: bytes
    dst_nhash: bytes
    chunks: bytes


class PacketbB(Packet):
    """
    Bridge private data block packet.
    """

    _code = b"bB"
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('blockdata', '++s')]
    __slots__ = ('src_ipp', 'blockdata')
    src_ipp: bytes
    blockdata: bytes


class PacketbP(AckablePacket):
    """
    Private Message Packet.
    """

    _code = b'bP'
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('ack_key', '8s'), ('src_nhash', '4s'), ('nick', 'S1'),
                   ('flags', 'B'), ('text', 'S2')]
    __slots__ = ('src_ipp', 'src_nhash', 'nick', 'flags', 'text')
    src_ipp: bytes
    src_nhash: bytes
    nick: str
    flags: int
    text: str


class PacketbT(AckablePacket):
    """
    Topic Change Request.
    """

    _code = b"bT"
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('ack_key', '8s'), ('src_nhash', '4s'), ('topic', 'S1')]
    __slots__ = ('src_ipp', 'src_nhash', 'topic')
    src_ipp: bytes
    src_nhash: bytes
    topic: str


class PacketbQ(Packet):
    """
    Bridge Data Request Packet.
    """

    _code = b"bQ"
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('bhash', '16s')]
    __slots__ = ('src_ipp', 'bhash')
    src_ipp: bytes
    bhash: bytes
