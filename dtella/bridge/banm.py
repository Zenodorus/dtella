"""
Dtella - Core P2P Module
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

from twisted.internet import reactor

from dtella.util import Ad, dcall_discard
from dtella.util.ipv4 import SubnetMatcher


class BanManager(object):

    def __init__(self, main: "DtellaMain_Base"):
        self.main = main
        self.rebuild_bans_dcall = None
        self.ban_matcher = SubnetMatcher()
        self.isBanned = self.ban_matcher.containsIP

    def scheduleRebuildBans(self):
        if self.rebuild_bans_dcall:
            return

        def cb():
            self.rebuild_bans_dcall = None
            osm = self.main.osm
            self.ban_matcher.clear()

            # Get all bans from bridges.
            if osm:
                for bridge in osm.bridges:
                    for b in bridge.bans.itervalues():
                        if b.enable:
                            self.ban_matcher.add_range(b.ipmask)

            # If I'm a bridge, get bans from IRC.
            if osm.bsm and self.main.ism:
                for ipmask in self.main.ism.bans:
                    self.ban_matcher.add_range(ipmask)

            self.enforceAllBans()

        # This time is slightly above zero, so that broadcast deliveries
        # will have a chance to take place before carnage occurs.
        self.rebuild_bans_dcall = reactor.callLater(1.0, cb)

    def enforceAllBans(self):
        osm = self.main.osm

        # Check all the online nodes.
        for n in list(osm.nodes):
            int_ip = Ad().set_addr(n.ipp).get_int_ip()
            if self.isBanned(int_ip):
                osm.node_exited(n, "Node Banned")

        # Check my ping neighbors.
        for pn in osm.pgm.pnbs.values():  # can't use itervalues
            int_ip = Ad().set_addr(pn.ipp).get_int_ip()
            if self.isBanned(int_ip):
                osm.pgm.cancel_link(pn, force=True)

        # Check myself
        if not osm.bsm:
            int_ip = Ad().set_addr(osm.me.ipp).get_int_ip()
            if self.isBanned(int_ip):
                self.main.show_login_status("You were banned.")
                self.main.shutdown(reconnect='max')

    def shutdown(self):
        dcall_discard(self, 'rebuild_bans_dcall')