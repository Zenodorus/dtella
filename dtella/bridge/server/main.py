"""
Dtella - Bridge Main Module
Copyright (C) 2008  Dtella Labs (http://www.dtella.org/)
Copyright (C) 2008  Paul Marks (http://www.pmarks.net/)
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import twisted.internet.error
from twisted.internet import reactor

import dtella.bridge.server.bridge_server as bridge_server
import dtella.bridge.server.protocol
import dtella.bridge_config as cfg
import dtella.common.base
import dtella.common.state
from dtella.common.log import LOG, set_log_file
from dtella.dconfig import DynamicConfigUpdateManager
from dtella.util import CHECK
from dtella.util.ipv4 import Ad


class DtellaMain_Bridge(dtella.common.base.DtellaMain_Base):

    def __init__(self):
        dtella.common.base.DtellaMain_Base.__init__(self)

        # State Manager
        self.state = dtella.common.state.StateManager(
            self, cfg.file_base + '.state',
            dtella.common.state.bridge_load_savers)
        self.state.initLoad()
        
        self.state.persistent = True
        self.state.udp_port = cfg.udp_port

        # Add an inital value for my own IP, adding it to the exempt list
        # if it's offsite.
        if cfg.myip_hint:
            ad = Ad().set_addr((cfg.myip_hint, cfg.udp_port))
            self.state.add_exempt_ip(ad)
            self.add_my_ip_report(ad, ad)

        # Add pre-defined entries to my local cache, and add them to
        # the exempt list of they're offsite.
        for text_ipp in cfg.ip_cache:
            ad = Ad().set_addr(text_ipp)
            self.state.add_exempt_ip(ad)
            self.state.refresh_peer(ad, 0)

        # Peer Handler
        self.ph = dtella.bridge.server.protocol.BridgeServerProtocol(self)

        # Reverse DNS Manager
        self.rdns = bridge_server.ReverseDNSManager(self)

        # DNS Update Manager
        self.dum = DynamicConfigUpdateManager(self)

        # IRC State Manager
        self.ism = None

        self._start_icm()

    def start(self):
        set_log_file(cfg.file_base + ".log", 4 << 20, 4)
        LOG.debug("Bridge Logging Manager Initialized")

        from dtella.bridge.server.bridge_server import getServiceConfig
        scfg = getServiceConfig()
        scfg.startService(self)

        # reactor.run()
        super().start()

    def cleanup_on_exit(self):
        LOG.info("Reactor is shutting down.  Doing cleanup.")

        self.shutdown(reconnect='no')
        self.state.save()

        # Cleanly close the IRC connection before terminating
        if self.ism:
            return self.ism.shutdown()


    async def _start_icm(self):
        udp_state = self.ph.getSocketState()
        if udp_state == 'dead':
            bind_ip = bridge_server.getBindIP()
            try:
                reactor.listenUDP(cfg.udp_port, self.ph, interface=bind_ip)
            except twisted.internet.error.BindError:
                LOG.error("Failed to bind UDP port!")
                raise SystemExit
        elif udp_state == 'dying':
            return
        
        CHECK(self.ph.getSocketState() == 'alive')
        await super()._start_icm()


    def _connection_desired(self):
        return True


    def get_bridge_manager(self):
        return {'bsm': bridge_server.BridgeServerManager(self)}


    def show_login_status(self, text, counter=None):
        LOG.info(text)


    def queryLocation(self, my_ipp):
        pass


    def afterShutdownHandlers(self):
        pass


    def state_observer(self):
        # Return the IRC Server, iff it's fully online

        if not (self.osm and self.osm.synced):
            return None

        if self.ism:
            return self.ism

        return None


    def addIRCStateManager(self, ism):
        CHECK(not self.ism)
        CHECK(ism.synced)
        self.ism = ism
        self.stateChange_ObserverUp()


    def removeIRCStateManager(self, ism):
        CHECK(ism and (self.ism is ism))

        self.ism = None

        # Send empty IRC state to Dtella.
        self.stateChange_ObserverDown()

        osm = self.osm
        if osm:
            # Rebuild ban table.
            osm.banm.scheduleRebuildBans()

