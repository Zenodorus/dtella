"""
Dtella - Bridge Server Online State Manager
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

from typing import Iterable

from dtella.bridge.banm import BanManager
from dtella.bridge.server.bridge_server import BridgeServerManager
from dtella.bridge.server.main import DtellaMain_Bridge
from dtella.common.osm import BaseOnlineStateManager


class BridgeServerOnlineStateManager(BaseOnlineStateManager):

    main: "DtellaMain_Bridge"

    def __init__(self, main: "DtellaMain_Bridge", my_ipp: bytes, node_ipps: Iterable[bytes]):
        super().__init__(main, my_ipp, node_ipps)
        self.banm: BanManager = BanManager(main)
        self.bsm = BridgeServerManager(self.main)

    async def sync_complete(self):

        # Forget the SyncManager
        self.sm = None

        # Unconfirmed nodes (without an expiration) can't exist once the
        # network is synced, so purge them from the nodes list.
        old_nodes = list(self._lookup_ipp.values())
        self._nodes = []
        self._lookup_ipp.clear()
        for n in old_nodes:
            if not n.expired:
                self._add_node(n)
            else:
                self.pgm.remove_outbound_link(n.ipp)
        self._sort_nodes()

        self.syncd = True

        self.bsm.syncComplete()

        # make new connections to the closest nodes now that we have a complete set of online nodes
        self.pgm.schedule_make_new_links()

        # Tell observers to get the nick list, topic, etc.
        self.main.stateChange_DtellaUp()

        await self.main.show_login_status("Sync Complete; You're Online!", counter='inc')

    def send_my_status(self, send_full=True):
        assert False, "bridge servers do not send status with this method"

    def updateMyInfo(self, send=False):
        # If I'm a bridge, send bridge state instead.
        if self.syncd:
            self.bsm.sendState()

    @property
    def is_moderated(self):
        return self.bsm.isModerated()

    async def shutdown(self):
        await super().shutdown()

        # Shut down the BanManager (just cancels some dcalls)
        if self.banm:
            self.banm.shutdown()

        # Shut down the BridgeServerManager
        if self.bsm:
            self.bsm.shutdown()
