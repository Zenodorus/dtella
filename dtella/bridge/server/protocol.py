"""
Dtella - Bridge Server Protocol
Copyright (C) 2008  Dtella Labs (http://www.dtella.org/)
Copyright (C) 2008  Paul Marks (http://www.pmarks.net/)
Copyright (C) 2008  Jacob Feisley  (http://www.feisley.com/)
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
from typing import TYPE_CHECKING

from dtella.bridge.node import BridgeNode
from dtella.bridge.packet import PacketbP, PacketbT, PacketbQ
from dtella.common import BadTimingError
from dtella.common.packet import BadPacketError, register_packet_classes
from dtella.common.protocol import BaseProtocol, PacketAK, ACK_PRIVATE, ACK_REJECT_BIT

if TYPE_CHECKING:
    from dtella.util import Ad
    from dtella.client.dc import PacketCA, PacketCP, PacketPM
    from dtella.common.sync import PacketYR


class BridgeServerProtocol(BaseProtocol):

    def handle_packet_YR(self, ad: "Ad", packet: "PacketYR"):

        osm = self.main.osm
        if not (osm and osm.sm):
            raise BadTimingError("not ready for sync reply")

        try:
            n = osm.get_node(packet.src_ipp)
        except KeyError:
            n = None

        # the source shouldn't be a known bridge node
        if isinstance(n, BridgeNode) or (osm and packet.src_ipp == osm.me.ipp):
            raise BadPacketError("bridge can't use YR")

        super().handle_packet_YR(ad, packet)

    def handle_packet_CA(self, ad: Ad, packet: "PacketCA"):

        # if we're not on the network, ignore it.
        osm = self.main.osm
        if not osm:
            raise BadTimingError("not ready to handle private message")

        # send reject acknowledgement
        packet = PacketAK(src_ipp=osm.me.ipp, mode=ACK_PRIVATE, flags=ACK_REJECT_BIT, ack_key=packet.ack_key)
        asyncio.create_task(self.main.ph.sendPacket(packet, Ad(packet.src_ipp)))

    def handle_packet_CP(self, ad: Ad, packet: "PacketCP"):

        # if we're not on the network, ignore it.
        osm = self.main.osm
        if not osm:
            raise BadTimingError("not ready to handle private message")

        # send reject acknowledgement
        packet = PacketAK(src_ipp=osm.me.ipp, mode=ACK_PRIVATE, flags=ACK_REJECT_BIT, ack_key=packet.ack_key)
        asyncio.create_task(self.main.ph.sendPacket(packet, Ad(packet.src_ipp)))

    def handle_packet_PM(self, ad: Ad, packet: "PacketPM"):

        # if we're not on the network, ignore it.
        osm = self.main.osm
        if not osm:
            raise BadTimingError("not ready to handle private message")

        # send reject acknowledgement
        packet = PacketAK(src_ipp=osm.me.ipp, mode=ACK_PRIVATE, flags=ACK_REJECT_BIT, ack_key=packet.ack_key)
        asyncio.create_task(self.main.ph.sendPacket(packet, Ad(packet.src_ipp)))

    def handlePacket_bP(self, ad, data):
        # Private message to IRC nick
        (kind, src_ipp, ack_key, src_nhash, rest
         ) = self.decodePacket('!2s6s8s4s+', data)

        self.checkSource(src_ipp, ad)

        (dst_nick, rest
         ) = self.decodeString1(rest)

        (flags, rest
         ) = self.decodePacket('!B+', rest)

        (text, rest
         ) = self.decodeString2(rest)

        if rest:
            raise BadPacketError("Extra data")

        osm = self.main.osm
        if not (osm and osm.synced):
            raise BadTimingError("Not ready for PM")

        osm.bsm.receivedPrivateMessage(src_ipp, ack_key, src_nhash,
                                       dst_nick, text)

    def handlePacket_bT(self, ad, data):
        # Topic change request

        (kind, src_ipp, ack_key, src_nhash, rest
         ) = self.decodePacket('!2s6s8s4s+', data)

        self.checkSource(src_ipp, ad)

        (topic, rest
         ) = self.decodeString1(rest)

        if rest:
            raise BadPacketError("Extra data")

        osm = self.main.osm
        if not (osm and osm.synced):
            raise BadTimingError("Not ready for bT")

        osm.bsm.receivedTopicChange(src_ipp, ack_key, src_nhash, topic)

    def handlePacket_bQ(self, ad, data):
        # Requesting a full data block

        (kind, src_ipp, bhash
         ) = self.decodePacket('!2s6s16s', data)

        self.checkSource(src_ipp, ad)

        osm = self.main.osm
        if not (osm and osm.synced):
            raise BadTimingError("Not ready for bQ")

        osm.bsm.receivedBlockRequest(src_ipp, bhash)


register_packet_classes(PacketbP, PacketbT, PacketbQ)

