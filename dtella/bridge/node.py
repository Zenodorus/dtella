"""
Dtella - Bridge Node Data
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
import random
import struct
from time import time as seconds
from typing import Dict, TYPE_CHECKING, Optional, List

from Crypto.PublicKey import pubkey

import dtella.common
from dtella.bridge.packet import PacketbP, PacketbQ
from dtella.common import Reject, NickError
from dtella.common.node import User, Node, ChatSequencedMixin
from dtella.common.protocol import ACK_REJECT_BIT, PacketAK, ACK_PRIVATE
from dtella.util import Ad, ipv4 as ipv4, AsyncRandSet
from dtella.util.encoding import decode_item, DecodingError

if TYPE_CHECKING:
    from dtella.client.main import DtellaMain_Client
    from dtella.common.base import DtellaMain_Base
    from dtella.client.dc import PacketPM


class ChunkError(Exception):
    pass


class LockingChatSequencedMixin(ChatSequencedMixin):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def add_message(self, nick: str, pktnum: int, text: str, flags: int):

        # False == the bridge wants us to queue everything
        unlocked = not hasattr(self, 'dns_pending')

        msg = (nick, text, flags)

        if self._chat_queue_pktnum is None:
            self.chat_queue_pktnum = pktnum

        # How far forward/back to accept messages
        FUZZ = 10

        # Find the pktnum index relative to the current base.
        # If it's slightly older, this will be negative.
        idx = ((pktnum - self.chat_queue_pktnum + FUZZ) % 0x100000000) - FUZZ

        if idx < 0:
            # Older message, send out of order
            if unlocked:
                self._send_message(*msg)

        elif idx >= FUZZ:
            # Way out there; put this at the end and dump everything
            if unlocked:
                self._chat_queue.append(msg)
                self.flush_queue()

        else:
            # From the near future: (0 <= idx < PKTNUM_BUF)

            # Make sure the queue is big enough;
            # put a timestamp in the empty spaces.
            extra = (idx - len(self._chat_queue)) + 1
            if extra > 0:
                self._chat_queue.extend([seconds()] * extra)

            # Insert the current message into its space
            if type(self._chat_queue[idx]) is float:
                self._chat_queue[idx] = msg

            # Possible spoof?
            # Don't know which one's real, so flush the queue and move on.
            elif self._chat_queue[idx] != msg:
                if unlocked:
                    self._chat_queue.insert(idx + 1, msg)
                    self.flush_queue()
                    return

            if unlocked:
                self._advance_queue()


class _Ban(object):

    def __init__(self, ipmask, enable, pktnum):
        self.ipmask = ipmask
        self.enable = enable
        self.pktnum = pktnum


class BridgeNode(Node):

    def __init__(self, ipp: bytes, main: "DtellaMain_Base"):
        super().__init__(ipp, main)
        self.rsa_obj: Optional[pubkey.pubkey] = None

        self.hashlist: List[bytes] = []
        self.blocks: Dict[bytes, Optional[bytes]] = {}

        self.req_blocks: AsyncRandSet[bytes] = AsyncRandSet()
        self._request_blocks_task: asyncio.Task = asyncio.create_task(self.schedule_request_blocks())




        self.status_pktnum = None

        self.topic_flag = False

        self.moderated = False

        self.last_assembled_pktnum = None
        self.nicks: Dict[str, BridgedUser] = {} # {nick: BridgedUser()}
        self.bans = {}  # {(ip,mask): Ban()}

        # Tuple of info strings; indices match up with the nick modes
        self.infostrings = ()

    # manage data blocks

    async def schedule_request_blocks(self):
        """Perform a retransmitting request for any outstanding data blocks from the bridge node."""

        timeout = 1
        while True:
            # peek an item on the list
            bhash = await self.req_blocks.get()
            self.req_blocks.put_nowait(bhash)

            # Send to bridge
            await self.main.ph.sendPacket(PacketbQ(src_ipp=self.main.osm.me.ipp, bhash=bhash), Ad(self._ipp))

            if timeout > 30.0:
                # Too many failures, just give up
                self.req_blocks.clear()
                timeout = 1
            else:
                await asyncio.sleep(random.uniform(0.9, 1.1) * timeout)
                timeout *= 1.2
                # Schedule next request.
                # This will become immediate if a reply arrives.

    def set_hash_list(self, hashlist: List[bytes], do_request: bool):
        """Set the list of data block hashes from the most recent bridge status update."""

        self.hashlist = hashlist
        self.blocks = dict.fromkeys(self.hashlist, None)

        osm = self.main.osm

        # Start with no requested blocks
        self.req_blocks.clear()

        for bhash in self.blocks:
            try:
                bk = osm.unclaimed_blocks.pop((self.ipp, bhash))
            except KeyError:
                # If we're requesting blocks, then add this to the list
                if do_request:
                    self.req_blocks.put_nowait(bhash)
            else:
                bk.cancel()
                self.blocks[bhash] = bk.data

        self._assemble_blocks()

        # Start requesting blocks (if any)
        self._request_blocks_task.cancel()
        self._request_blocks_task = asyncio.create_task(self.schedule_request_blocks())

    def add_data_block(self, bhash: bytes, data: bytes):
        # Return True if the block was accepted, False otherwise

        if bhash not in self.blocks:
            return False

        # If we're requesting blocks, then mark this one off
        # and possibly ask for more.

        if bhash in self.req_blocks:
            self.req_blocks.discard(bhash)
            self._request_blocks_task.cancel()
            self._request_blocks_task = asyncio.create_task(self.schedule_request_blocks())

        # Record the block data, and check if it's time to assemble
        # all the blocks together.

        if self.blocks[bhash] is None:
            self.blocks[bhash] = data
            self._assemble_blocks()

        return True

    def receivedPrivateChunks(self, pktnum, ack_key, dst_nhash, chunks):

        osm = self.main.osm

        ack_flags = 0

        try:
            if dst_nhash != osm.me.nick_hash:
                raise Reject

            if self.poke_pm_key(ack_key):
                # Haven't seen this message before, so handle it.

                try:
                    self.process_chunks(chunks, pktnum)
                except ChunkError:
                    raise Reject

        except Reject:
            ack_flags |= ACK_REJECT_BIT

        packet = PacketAK(src_ipp=self.main.osm.me.ipp, mode=ACK_PRIVATE, flags=ack_flags, ack_key=ack_key)
        asyncio.create_task(self.main.ph.sendPacket(packet, Ad(self.ipp)))

    def _assemble_blocks(self):

        osm = self.main.osm

        # DEBUG
        i = 0
        for bk in self.blocks.values():
            if bk is None:
                i += 1

        # Check if all the blocks exist yet
        if None in self.blocks.values():
            return

        data: bytes = b''.join([self.blocks[bhash] for bhash in self.hashlist])

        self.hashlist = []
        self.blocks = {}

        # This will be toggled back to True if the topic is set
        self.topic_flag = False

        # Default to disabled
        self.moderated = False

        try:
            self.process_chunks(data, self.status_pktnum)
        except ChunkError as e:
            self.main.log_packet("Couldn't assemble blocks: %s" % e)

        # Remove any nicks who aren't mentioned in this update
        dead_nicks = []

        for n in self.nicks.values():
            if n.pktnum < self.status_pktnum:
                del self.nicks[n.nick]
                if n.mode != 0xFF:
                    dead_nicks.append(n)

        # Report all the nicks that we deleted
        dead_nicks.sort()
        for n in dead_nicks:
            osm.remove_user(n, "Dead")

        # Remove any bans which aren't mentioned in this update
        for b in self.bans.values():
            if b.pktnum < self.status_pktnum:
                del self.bans[b.ipmask]

        # If not topic was set, release control of it
        if not self.topic_flag:
            osm.tm.checkLeavingNode(self)

        self.last_assembled_pktnum = self.status_pktnum

    def process_chunks(self, data: bytes, pktnum: int):

        # Outdated means that this chunk list is older than the chunk
        # list from the last full status update.  Therefore, it only
        # really applies to private bC messages.
        outdated = (self.last_assembled_pktnum is not None) and (pktnum < self.last_assembled_pktnum)
        osm = self.main.osm

        while data:
            code, data = data[0], data[1:]
            if code == ord(b'N'):
                # IRC nick
                try:
                    mode, data = decode_item('B', data)
                    nick, data = decode_item('S1', data)
                except DecodingError:
                    raise ChunkError("N: struct error")
                if not outdated:
                    self._update_nick(nick, mode, pktnum)

            elif code == ord(b'C'):
                # hub message
                try:
                    chat_pktnum, data = decode_item('I', data)
                    flags, data = decode_item('B', data)
                    nick, data = decode_item('S1', data)
                    text, data = decode_item('S2', data)
                except DecodingError:
                    raise ChunkError("C: struct Error")
                if len(text) > 1024:
                    raise ChunkError("C: too much text")
                if osm.synced:
                    self.add_message(nick, chat_pktnum, text, flags)

            elif code == ord(b'M'):
                # private message
                try:
                    flags, data = decode_item('B', data)
                    nick, data = decode_item('S1', data)
                    text, data = decode_item('S2', data)
                except DecodingError:
                    raise ChunkError("M: struct Error")
                if len(text) > 1024:
                    raise ChunkError("M: too much text")
                asyncio.create_task(self._handle_private_message(flags, nick, text))

            elif code == ord(b'K'):
                # IRC kick
                try:
                    ipp, data = decode_item('6s', data)
                    pktnum, data = decode_item('I', data)
                    flags, data = decode_item('B', data)
                    l33t, data = decode_item('S1', data)
                    n00b, data = decode_item('S1', data)
                    reason, data = decode_item('S2', data)
                except DecodingError:
                    raise ChunkError("K: struct error")
                if len(reason) > 1024:
                    raise ChunkError("K: too much text")
                self._handle_kick(ipp, pktnum, flags, l33t, n00b, reason)

            elif code == ord(b'B'):
                try:
                    subnet, data = decode_item('B', data)
                    ip, data = decode_item('i', data)
                except DecodingError:
                    raise ChunkError("B: struct error")
                else:
                    enable = bool(subnet & 0x80)
                    subnet &= 0x3F

                try:
                    mask = ipv4.CidrNumToMask(subnet)
                except ValueError:
                    raise ChunkError("B: subnet out of range")

                if not outdated:
                    self._update_ban((ip, mask), enable, pktnum)

            elif code == ord(b'I'):
                # info
                try:
                    info, data = decode_item('S2', data)
                except DecodingError:
                    raise ChunkError("I: struct error")
                if not outdated:
                    self._handle_info(info)

            elif code == ord(b'T'):
                # topic
                try:
                    flags, data = decode_item('B', data)
                    nick, data = decode_item('S1', data)
                    topic, data = decode_item('S1', data)
                except DecodingError:
                    raise ChunkError("T: struct Error")
                else:
                    changed = bool(flags & dtella.common.CHANGE_BIT)

                if len(topic) > 1024:
                    raise ChunkError("T: too much text")

                if not outdated:
                    osm.tm.updateTopic(self, nick, topic, changed)
                    self.topic_flag = True

            elif code == ord(b'F'):
                # flags (specifically just moderation)
                try:
                    flags, data = decode_item('B', data)
                except DecodingError:
                    raise ChunkError("F: struct error")
                if not outdated:
                    self.moderated = bool(flags & dtella.common.MODERATED_BIT)

            else:
                raise ChunkError(f"unknown chunk type '{chr(code)}'")

    # process data blocks

    def _update_nick(self, nick: str, mode, pktnum):
        osm = self.main.osm

        try:
            if mode == 0xFF:
                raise IndexError
            info = self.infostrings[mode]
        except IndexError:
            info = ''

        try:
            n = self.nicks[nick]
        except KeyError:
            # New nick
            n = self.nicks[nick] = BridgedUser(self, nick, info, mode, pktnum)

            if mode != 0xFF:
                try:
                    osm.add_user(n)
                except NickError:
                    # Collision of some sort
                    n.mode = 0xFF
        else:

            # Existing nick
            if pktnum < n.pktnum:
                return

            n.pktnum = pktnum

            if n.mode == mode:
                return

            if mode != 0xFF:

                if n.mode != 0xFF:
                    # Change mode of existing nick
                    osm.update_user(n, info)

                else:
                    # Dead nick coming back online
                    n.set_info(info)
                    try:
                        osm.add_user(n)
                    except NickError:
                        # Collision of some sort
                        mode = 0xFF

            elif n.mode != 0xFF:
                # Remove existing nick
                osm.remove_user(n, "Going Offline")

            n.mode = mode

    def _update_ban(self, ipmask, enable, pktnum):

        osm = self.main.osm

        try:
            b = self.bans[ipmask]
        except KeyError:
            b = self.bans[ipmask] = _Ban(ipmask, enable, pktnum)
        else:
            # Update packet number, ignore old ones.
            if b.pktnum < pktnum:
                b.pktnum = pktnum
            else:
                return

            # Update state, ignore non-changes.
            if b.enable != enable:
                b.enable = enable
            else:
                return

        osm.banm.scheduleRebuildBans()

    async def _handle_private_message(self, flags, nick, text):
        from dtella.client.dc import DCHandler

        if isinstance(dch := self.main.state_observer, DCHandler):
            if flags & dtella.common.NOTICE_BIT:
                # Notice sent directly to this user.
                # Display it in the chat window
                dch.push_ChatMessage("*N %s" % nick, text)
            else:
                # Can't support /me very well in a private message,
                # so just stick a * at the beginning.
                if flags & dtella.common.SLASHME_BIT:
                    text = '* ' + text
                dch.push_PrivMsg(nick, text)

    def _handle_kick(self, ipp, pktnum, flags, l33t, n00b, reason):
        from dtella.client.dc import DCHandler
        # Find the node associated with the n00b's ipp
        osm = self.main.osm
        ph = self.main.ph
        me = osm.me

        if ipp == me.ipp:
            n = me
        else:
            try:
                n = osm.get_node(ipp)
            except KeyError:
                return

        # Check if the user has rejoined by the time we got this
        outdated = ph.is_outdated_status(n, pktnum)

        dch = self.main.state_observer

        if n is me:

            # Make sure I'm online, and this kick isn't old somehow
            if isinstance(dch, DCHandler) and not outdated:

                # If the bridge requested a rejoin, then have the client come
                # back in 5..10 minutes.
                if flags & dtella.common.REJOIN_BIT:
                    rejoin_time = random.uniform(60*5, 60*10)
                else:
                    rejoin_time = None

                # Fix pktnum for my next status update.
                me.status_pktnum = pktnum

                # Force the DC client to become invisible.
                lines = [
                    "You were kicked by %s: %s" % (l33t, reason),
                    "Type !REJOIN to get back in."
                ]
                self.main.kickObserver(lines, rejoin_time)

        else:
            # Display text even for outdated messages, because the
            # Updated status message from the kicked node is racing
            # against the kick packet.  Also, if n00b is empty, then
            # treat it as a silent kick.
            if isinstance(dch, DCHandler) and n00b:
                dch.push_Status("%s has kicked %s: %s" % (l33t, n00b, reason))

            if not outdated:
                # Drop this node from the nick list (if it's there)
                osm.remove_user(n, "Kicked")
                n.set_no_user()

                # The next valid broadcast should have pktnum+1
                n.status_pktnum = pktnum

    def _handle_info(self, info: str):
        infostrings = tuple(info.split('|'))

        if self.infostrings == infostrings:
            return

        self.infostrings = infostrings

        osm = self.main.osm

        # Scan through all the nicks, and fill in their info strings

        for n in self.nicks.values():

            if n.mode == 0xFF:
                continue

            try:
                info = infostrings[n.mode]
            except IndexError:
                info = ''

            osm.update_user(n, info)

    # other

    def shutdown(self):
        super().shutdown()
        self._request_blocks_task.cancel()

        osm = self.main.osm

        for n in self.nicks.values():
            osm.remove_user(n, "Bridge Exited")

        self.nicks.clear()

        # Unregister me from the BridgeClientManager
        osm.bridges.remove(self)
        osm.banm.scheduleRebuildBans()


class BridgedUser(User):

    is_peer: bool = False

    def __init__(self, bridge_n: BridgeNode, nick: str, info: str, mode, pktnum):
        super().__init__()
        self.bridge_n: BridgeNode = bridge_n
        self.nick = nick
        self.set_info(info)

        self.pktnum = pktnum
        self.mode = mode

    async def event_PrivateMessage(self, packet: "PacketPM"):
        osm = self.bridge_n.main.osm
        text = packet.text

        if len(text) > 512:
            text = text[:512]
        packet = PacketbP(src_ipp=osm.me.ipp, src_nhash=osm.me.nick_hash, nick=self.nick, flags=0, text=text)
        return await self.bridge_n.send_private_message(packet)

    async def event_ConnectToMe(self, packet):
        return "IRC users don't have any files."

    async def event_RevConnectToMe(self, packet):
        return "IRC users don't have any files."


    def checkRevConnectWindow(self):
        return False


class BridgeNodeData(object):

    def __init__(self, main: "DtellaMain_Client", parent_n: Node):
        self.main: "DtellaMain_Client" = main
        self.parent_n: Node = parent_n
        self.blocks: Dict[bytes, bytes] = {}   # {hash: [None | data]}
        self.hashlist = []
        self.status_pktnum = None

        self.topic_flag = False

        self.moderated = False

        self.last_assembled_pktnum = None
        self.nicks: Dict[str, BridgedUser] = {} # {nick: BridgedUser()}
        self.bans = {}  # {(ip,mask): Ban()}

        # Tuple of info strings; indices match up with the nick modes
        self.infostrings = ()

        self.req_blocks: AsyncRandSet[bytes] = AsyncRandSet()
        self._request_blocks_task: asyncio.Task = asyncio.create_task(self.schedule_request_blocks())

        # Register me in BridgeClientManager
        self.main.osm.bcm.bridges.add(self)
        self.main.osm.bridges.add(self)


    def sendTopicChange(self, topic):
        from dtella.client.dc import DCHandler
        osm = self.main.osm
        me = osm.me

        topic = topic[:255]

        ack_key = self.parent_n.generate_ack_key()

        packet = ['bT']
        packet.append(osm.me.ipp)
        packet.append(ack_key)
        packet.append(me.nick_hash)
        packet.append(struct.pack('!B', len(topic)))
        packet.append(topic)
        packet = ''.join(packet)

        def fail_cb(detail):
            if isinstance(dch := self.main.state_observer, DCHandler):
                if detail == "Rejected":
                    dch.push_Status("Sorry, the topic has been locked.")
                else:
                    dch.push_Status("Failed to set topic: Timeout")

        ph = self.main.ph
        self.parent_n.send_private_message(ph, ack_key, packet, fail_cb)

