"""
Dtella - Bridge Client Protocol
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import struct
from hashlib import md5
from typing import TYPE_CHECKING

from Crypto.PublicKey import RSA
from Crypto.Util.number import bytes_to_long

from dtella.bridge.node import BridgeNode
from dtella.bridge.packet import PacketbY, PacketBS, PacketBB, PacketBC, PacketBX, PacketbC, PacketbB
from dtella.client.protocol import ClientProtocol
from dtella.common import BadTimingError
from dtella.common.packet import BroadcastPacket, BadPacketError, register_packet_classes
from dtella.common.protocol import BadBroadcast, PKTNUM_BUF
from dtella.util import Ad

if TYPE_CHECKING:
    from dtella.common.sync import PacketYR
    from dtella.client.main import DtellaMain_Client
    from dtella.common.node import Node


class BridgeClientProtocol(ClientProtocol):

    main: "DtellaMain_Client"

    def is_outdated_status(self, n: "Node", pktnum: int):
        # This prevents a node's older status messages from taking
        # precedence over newer messages.

        if n is None:
            # Node doesn't exist, can't be outdated
            return False

        if isinstance(n, BridgeNode):
            # Don't allow updates for a bridge node
            return True

        if n.status_pktnum is None:
            # Don't have a pktnum yet, can't be outdated
            return False

        if 0 < (n.status_pktnum - pktnum) % 0x100000000 < PKTNUM_BUF:
            self.main.log_packet("Outdated Status")
            return True

        return False

    def handle_broadcast(self, ad: Ad, packet: BroadcastPacket, bridgey: bool = False):
        bridgey = type(packet) in (PacketBS, PacketBB, PacketBC, PacketBX)
        if bridgey:
            super().handle_broadcast(ad, packet, bridgey)
            return

        try:
            src_n = self.main.osm.get_node(packet.src_ipp)
        except KeyError:
            pass
        else:
            # Filter all non-bridgey broadcasts from bridge nodes.
            # TODO am I a bridge node
            if isinstance(src_n, BridgeNode) or isinstance(self.main.osm.me, BridgeNode):
                raise BadBroadcast("bridge can't use " + packet.code.decode())

        super().handle_broadcast(ad, packet, bridgey)

    def handle_packet_YR(self, ad: Ad, packet: "PacketYR"):
        # TODO clean up and document
        osm = self.main.osm
        if not (osm and osm.sm):
            raise BadTimingError("not ready for sync reply")

        try:
            n = osm.get_node(packet.src_ipp)
        except KeyError:
            n = None

        # the source shouldn't be a known bridge node
        if isinstance(n, BridgeNode):
            raise BadPacketError("bridge can't use YR")

        super().handle_packet_YR(ad, packet)

    def handle_packet_bY(self, ad: Ad, packet: PacketbY):
        # Bridge Sync Reply

        osm = self.main.osm
        if not osm:
            raise BadTimingError("Not ready for bridge sync reply")

        self.main.check_source(packet.src_ipp, ad)

        if not (packet.expire <= 30*60):
            raise BadPacketError("expire time out of range")

        class Skip(Exception):
            pass

        try:
            # If the signed message is too old, discard it.
            if osm.signature_expired(packet.pktnum):
                raise Skip

            # If we've received a newer status update, then this is useless.
            try:
                n = osm.get_node(packet.src_ipp)
            except KeyError:
                pass
            else:
                from dtella.bridge.node import BridgeNode
                assert isinstance(n, BridgeNode)
                if n.status_pktnum > packet.pktnum:
                    raise Skip

            # Make sure public key matches a hash in DNS
            pkhash = md5(packet.pubkey).digest()
            if pkhash not in self.main.state.dns_pkhashes:
                raise Skip

            # Generate RSA object from public key
            try:
                rsa_obj = RSA.construct((bytes_to_long(packet.pubkey), 65537))
            except:
                raise Skip

            if not packet.verify(rsa_obj):
                raise Skip

            # Keep track of the timestamp
            osm.update_bridge_time(packet.pktnum)

            # Update status
            osm.refresh_bridge_node_status(packet, rsa_obj)

        except Skip:
            pass

        # Process the sync reply
        packet.c_nbs = [ipp for ipp in packet.c_nbs if self.main.auth('sx', Ad(ipp))]
        packet.u_nbs = [ipp for ipp in packet.u_nbs if self.main.auth('sx', Ad(ipp))]
        osm.sm and osm.sm.receivedSyncReply(packet.src_ipp, packet.c_nbs, packet.u_nbs)

    def handle_packet_bC(self, ad: Ad, packet: PacketbC):

        pktnum, = struct.unpack('!Q', packet.ack_key)

        osm = self.main.osm
        if not (osm and osm.synced):
            raise BadTimingError("not ready for bC")

        self.main.check_source(packet.src_ipp, ad, exempt_ip=True)

        # If the signed message is too old, discard it.
        if osm.signature_expired(pktnum):
            raise BadTimingError("bC: expired signature")

        try:
            n = osm.get_node(packet.src_ipp)
        except KeyError:
            raise BadTimingError("bC: not found")
        else:
            from dtella.bridge.node import BridgeNode
            assert isinstance(n, BridgeNode)

        # Verify Signature
        if not packet.verify(n.rsa_obj):
            raise BadPacketError("bC: signature didn't verify")

        # Keep track of the timestamp
        osm.update_bridge_time(pktnum)

        n.receivedPrivateChunks(pktnum, packet.ack_key, packet.dst_nhash, packet.chunks)

    def handle_packet_bB(self, ad: Ad, packet: PacketbB):
        # Bridge private data block

        self.main.check_source(packet.src_ipp, ad, exempt_ip=True)
        osm = self.main.osm
        if not osm:
            raise BadTimingError("Not ready for bB")
        osm.handle_data_block(packet)

    def check_broadcast_BS(self, src_n: "BridgeNode", packet: PacketBS):

        osm = self.main.osm

        if not (packet.expire <= 30*60):
            raise BadPacketError("Expire time out of range")

        # If the signed message is too old, discard it.
        if osm.signature_expired(packet.pktnum):
            raise BadBroadcast

        # If we've received a newer status update, then this is useless.
        if src_n.status_pktnum > packet.pktnum:
            raise BadBroadcast

        # Make sure public key matches a hash in DNS
        pkhash = md5(packet.pubkey).digest()
        if pkhash not in self.main.state.dns_pkhashes:
            # Not useful to me, but still forward it
            return

        # Generate RSA object from public key
        try:
            rsa_obj = RSA.construct((bytes_to_long(packet.pubkey), 65537))
        except:
            return

        # Verify signature
        if not packet.verify(rsa_obj):
            raise BadBroadcast

        # Keep track of the timestamp
        osm.update_bridge_time(packet.pktnum)

        # Update status
        osm.refresh_bridge_node_status(packet, rsa_obj)

    def check_broadcast_BB(self, src_n: "BridgeNode", packet: PacketBB):
        # If the signed message is too old, discard it.
        if self.main.osm.signature_expired(packet.pktnum):
            raise BadBroadcast
        self.main.osm.handle_data_block(packet)

    def check_broadcast_BC(self, src_n: "BridgeNode", packet: PacketBC):
        from dtella.bridge.node import ChunkError, BridgeNode

        # If the signed message is too old, discard it.
        if self.main.osm.signature_expired(packet.pktnum):
            raise BadBroadcast

        # If this doesn't look like a bridge node,
        # then just blindly forward it.
        if not isinstance(src_n, BridgeNode):
            return None

        # Verify signature
        if not packet.verify(src_n.rsa_obj):
            return

        # Keep track of the timestamp
        self.main.osm.update_bridge_time(packet.pktnum)

        try:
            src_n.process_chunks(packet.chunks, packet.pktnum)
        except ChunkError as e:
            self.main.log_packet("BC chunk error: %s" % str(e))

    def check_broadcast_BX(self, src_n: "BridgeNode", packet: PacketBX):
        from dtella.bridge.node import BridgeNode

        osm = self.main.osm

        # If the signed message is too old, discard it.
        if osm.signature_expired(packet.pktnum):
            raise BadBroadcast

        # If we've received a newer status update, then this is useless.
        if src_n.status_pktnum > packet.pktnum:
            raise BadBroadcast

        # If this doesn't look like a bridge node,
        # then just blindly forward it.
        if not isinstance(src_n, BridgeNode):
            return None

        # Verify signature
        if not packet.verify(src_n.rsa_obj):
            return

        # Keep track of the timestamp
        osm.update_bridge_time(packet.pktnum)

        # Exit the node
        osm.node_exited(src_n, "Bridge Exit")


register_packet_classes(PacketbY, PacketBS, PacketBB, PacketBC, PacketBX, PacketbC, PacketbB)
