from dtella.bridge.client.osm import BridgeClientOnlineStateManager
from dtella.bridge.client.protocol import BridgeClientProtocol
from dtella.client.main import DtellaMain_Client
from dtella.common import overwrite_modules

modules = {"PeerHandler": BridgeClientProtocol,
           "OnlineStateManager": BridgeClientOnlineStateManager}


def main(dc_port):
    overwrite_modules(modules)

    DtellaMain_Client(True).start(dc_port)
