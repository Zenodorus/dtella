"""
Dtella - Bridge Client Module
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
from hashlib import md5
from time import time as seconds
from typing import Union, TYPE_CHECKING, Dict, Tuple, Iterable

from dtella.bridge.banm import BanManager
from dtella.bridge.node import BridgeNode
from dtella.bridge.packet import PacketbY, PacketBS, PacketBB, PacketbB
from dtella.common import NODE_EXPIRE_EXTEND
from dtella.common.osm import BaseOnlineStateManager
from dtella.util import parse_dtella_tag, Ad, dcall_discard
from dtella.util.scheduled_task import ScheduledTask

if TYPE_CHECKING:
    from Crypto.PublicKey import pubkey
    from dtella.client.main import DtellaMain_Client
    from dtella.common.node import Node


class _UnclaimedBlock(object):

    def __init__(self, data: bytes):
        self._data: bytes = data
        self._timeout_task: ScheduledTask = ScheduledTask(asyncio.sleep(0), delay=15.0)

    @property
    def data(self) -> bytes:
        return self._data

    @property
    def expired(self) -> bool:
        return self._timeout_task.done

    def reschedule_timeout(self):
        return self._timeout_task.reset(15.0)

    def cancel(self):
        self._timeout_task.cancel()


class BridgeClientOnlineStateManager(BaseOnlineStateManager):

    main: "DtellaMain_Client"

    def __init__(self, main: "DtellaMain_Client", my_ipp: bytes, node_ipps: Iterable[bytes]):
        super().__init__(main, my_ipp, node_ipps)
        self.banm: BanManager = BanManager(main)

        self._cache_task: asyncio.Task = asyncio.create_task(self._clear_unclaimed_blocks())
        self.unclaimed_blocks: Dict[Tuple[bytes, bytes], _UnclaimedBlock] = {}
        self._bridge_time: int = 0
        self.bridges = set()

    def signature_expired(self, pktnum):
        """Return True if the given timestamp has expired."""
        return (pktnum >> 24) < self._bridge_time - 60

    def update_bridge_time(self, pktnum: int):
        """Update the stored bridge time value, if the new value is larger."""
        if (time := pktnum >> 24) > self._bridge_time:
            self._bridge_time = time

    @property
    def is_moderated(self):
        return any(b.moderated for b in self.bridges)

    async def _clear_unclaimed_blocks(self):
        while True:
            await asyncio.sleep(60)
            expired = [k for k, v in self.unclaimed_blocks.items() if v.expired]
            for k in expired:
                del self.unclaimed_blocks[k]

    def handle_data_block(self, packet: Union[PacketBB, PacketbB]):
        """Call this when a data block arrives from the network"""

        bhash: bytes = md5(packet.blockdata).digest()
        key: Tuple[bytes, bytes] = (packet.src_ipp, bhash)

        try:
            bn = self.main.osm.get_node(packet.src_ipp)
        except KeyError:
            self._add_unclaimed_block(key, packet.blockdata)
        else:
            assert isinstance(bn, BridgeNode), f"node at {Ad(packet.src_ipp)} is not being marked as a bridge"
            if not bn.add_data_block(bhash, packet.blockdata):
                self._add_unclaimed_block(key, packet.blockdata)

    def _add_unclaimed_block(self, key: Tuple[bytes, bytes], data: bytes):
        """
        Add a data block to the unclaimed_blocks list, and let it sit there either until it's claimed, or it expires.
        """

        try:
            bk = self.unclaimed_blocks[key]
        except KeyError:
            bk = self.unclaimed_blocks[key] = _UnclaimedBlock(data)
        if not bk.reschedule_timeout():
            del self.unclaimed_blocks[key]

    def refresh_bridge_node_status(self, packet: Union[PacketbY, PacketBS], rsa_obj: "pubkey.pubkey"):

        assert packet.src_ipp != self.me.ipp, "should not be updating my own status with data from peers"

        try:
            n = self._lookup_ipp[packet.src_ipp]
            in_nodes = True
        except KeyError:
            n = BridgeNode(packet.src_ipp, self.main)
            in_nodes = False

        self.main.log_packet(f"Status: {Ad(packet.src_ipp)} {packet.expire} (<bridge>)")

        # if session ID changed, remove and reinsert the node so that the list is sorted
        if self.synced and in_nodes and n.sesid != packet.sesid:
            self._remove_node(n)
            in_nodes = False

        # Update info
        # TODO this was None, so may have some implications
        n.status_pktnum = packet.pktnum
        n.sesid = packet.sesid
        n.uptime = seconds() - packet.uptime
        n.persist = packet.persist
        n.rsa_obj = rsa_obj

        # Nick hasn't changed, just update info
        self.update_user(n, "")

        # If n isn't in nodes list, then add it
        if not in_nodes:
            self._add_node(n)

        # Expire this node after the expected retransmit
        n.reschedule_timeout(packet.expire + NODE_EXPIRE_EXTEND)

        # Possibly make this new node an outgoing link
        self.pgm.schedule_make_new_links()

        self.bridges.add(n)
        n.set_hash_list(packet.hashes, isinstance(packet, PacketbY))

    async def shutdown(self):
        await super().shutdown()

        # Shut down the BanManager (just cancels some dcalls)
        if self.banm:
            self.banm.shutdown()

        # Cancel dcalls
        for bk in self.unclaimed_blocks.values():
            bk.expire_dcall.cancel()

        self.unclaimed_blocks.clear()
