"""
Dtella - DNS Updater Plugin (Text File)
Copyright (C) 2007  Dtella Labs (http://www.dtella.org)
Copyright (C) 2007  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
from typing import List

from dtella.util import get_user_path


class TextFileUpdater(object):

    def __init__(self, filename: str):
        self._filename = filename

    def update(self, entries):
        with open(get_user_path(self._filename), 'w') as f:
            for k in sorted(entries):
                f.write(f"{k}={entries[k]}\n")


class TextFilePuller(object):

    def __init__(self, filename: str):
        self._filename = filename

    def start_text(self) -> str:
        return "Loading config from text file..."

    def query(self) -> List[str]:
        try:
            with open(get_user_path(self._filename), 'r') as f:
                data = f.readlines()
            return [line[:-1] for line in data]
        except FileNotFoundError:
            return []
