"""
Dtella - Dynamic Config Puller Module
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
import asyncio
from typing import TYPE_CHECKING

import dns.resolver

import dtella.common.local_config as local
from dtella.common.packet import pk_enc
from dtella.util import cmpify_version, dcall_discard, parse_bytes, word_wrap

import binascii
import time
import random
import dns.resolver

if TYPE_CHECKING:
    from dtella.common.base import DtellaMain_Base


class DynamicConfigPuller(object):

    def __init__(self, main: "DtellaMain_Base"):
        self.main = main

        self.override_vc = cmpify_version(local.version_string)
        self.resetReportedVersion()
        self.minshare = 1
        self.version = None

        self.cfg_lastUpdate: float = 0
        self.cfg_busy: bool = False

        # Increases logarithmically until we get a first reply
        self._fail_delay = 10.0

        self.cfgRefresh_dcall = None

        self._dcfg_task: asyncio.Task = self.main.loop.create_task(self._run())

    async def get_dynamic_config(self):
        """Manually grab the dynamic config when starting a new connection."""
        if local.dconfig_puller is None:
            return

        await self.main.show_login_status(local.dconfig_puller.start_text(), counter=0)
        if not await self._get_dynamic_config():
            await self.main.show_login_status("Query failed!  Trying to proceed without it...")

    async def _get_dynamic_config(self):
        # TODO make the query async
        self.cfg_busy = True
        try:
            records = local.dconfig_puller.query()
        except AttributeError:
            return False
        except dns.resolver.Timeout:
            self.cfg_busy = False
            return False

        if not records:
            self.cfg_busy = False
            return False

        self.main.state.dns_pkhashes = set()
        self.main.state.dns_ipcache = (0, [])

        for line in records:
            try:
                name, value = line.split('=', 1)
            except ValueError:
                continue

            name = name.lower()

            if name == 'minshare':
                try:
                    self.minshare = parse_bytes(value)
                except ValueError:
                    pass
                else:
                    cap = local.minshare_cap
                    if (cap is not None) and (self.minshare > cap):
                        self.minshare = cap

            elif name == 'version':
                try:
                    min_v, new_v, url = value.split()
                except ValueError:
                    pass
                else:
                    self.version = (min_v, new_v, url)

            elif name == 'pkhash':
                h = binascii.a2b_base64(value)
                self.main.state.dns_pkhashes.add(h)

            elif name == 'ipcache':
                try:
                    data = binascii.a2b_base64(value)
                    data = pk_enc.decrypt(data)
                except (ValueError, binascii.Error):
                    continue

                if (len(data) - 4) % 6 != 0:
                    continue

                self.main.state.set_dns_ip_cache(data)

        self.cfg_lastUpdate = time.time()
        self.cfg_busy = False
        return True

    async def _run(self):
        while True:
            if self.belowMinimumVersion():
                return
            self.reportNewVersion()

            if self.cfg_lastUpdate > 0:
                # Automatically query DNS a couple times a day
                when = random.uniform(3600*12, 3600*24)
            else:
                # If we've never gotten an update, request sooner
                when = self._fail_delay * random.uniform(0.8, 1.2)
                self._fail_delay = min(3600 * 2, self._fail_delay * 1.5)

            await asyncio.sleep(when)
            await self._get_dynamic_config()

    def dtellaShutdown(self):
        dcall_discard(self, 'cfgRefresh_dcall')
        self.cfg_cb = None

    def belowMinimumVersion(self):

        if not self.version:
            return False

        min_v, new_v, url = self.version
        min_vc = cmpify_version(min_v)

        if self.override_vc < min_vc:

            self.main.shutdown(reconnect='no')
            
            text = (
                " ",
                "Your version of Dtella (%s) is too old to be used on this "
                "network.  Please upgrade to the latest version (%s)."
                % (local.version_string, new_v),
                " ",
                "[If unusual circumstances prevent you from upgrading, "
                "type !VERSION_OVERRIDE to attempt to connect using this "
                "unsupported client.]",
                " ",
                "Download link: %s" % url
                )

            for par in text:
                for line in word_wrap(par):
                    self.main.show_login_status(line)
            return True

        return False

    def reportNewVersion(self):

        if not self.version:
            return

        min_v, new_v, url = self.version
        new_vc = cmpify_version(new_v)

        if self.reported_vc < new_vc:
            
            if self.main.dch:
                say = self.main.dch.bot.say
                say("You have Dtella version %s.  "
                    "A newer version (%s) is available."
                    % (local.version_string, new_v))
                say("Download link: %s" % url)
                
                self.reported_vc = new_vc

    def overrideVersion(self):
        # User requested skipping of the minimum version control

        if self.version:
            min_v, new_v, url = self.version
            min_vc = cmpify_version(min_v)

            if not (self.override_vc < min_vc):
                return False

            self.override_vc = min_vc

        return True

    def resetReportedVersion(self):
        self.reported_vc = cmpify_version(local.version_string)
