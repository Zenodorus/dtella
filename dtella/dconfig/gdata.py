"""
Dtella - Google Spreadsheets Puller Module
Copyright (C) 2008  Dtella Labs (http://dtella.org)
Copyright (C) 2008  Paul Marks (http://pmarks.net)
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

from twisted.internet import threads
from twisted.internet.threads import deferToThread
import urllib
import xml.dom.minidom
import atom.service
import gdata.spreadsheet
import gdata.spreadsheet.service


import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

BASE_DIR = os.path.join(os.path.expanduser('~'))
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets'
PROJECT_NAME = ""


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    credential_dir = os.path.join(BASE_DIR, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, f'sheets.googleapis.com-{PROJECT_NAME}.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(os.path.join(credential_dir, CLIENT_SECRET_FILE), SCOPES)
        flow.user_agent = APPLICATION_NAME
        credentials = tools.run_flow(flow, store)
    return credentials


def get_sheets_service():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discovery_url = "https://sheets.googleapis.com/$discovery/rest?version=v4"
    return discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discovery_url)



def ForceSSL():
    # Patch the Atom library to force all GData requests to run over SSL.

    real_BuildUri = atom.service.BuildUri
    def BuildUri(*args, **kw):
        uri = real_BuildUri(*args, **kw)
        if uri.startswith("http:"):
            uri = "https:" + uri[5:]
        return uri
    atom.service.BuildUri = BuildUri

ForceSSL()

PAGE_TEMPLATE = ("https://spreadsheets.google.com/feeds/cells/"
                 "%s/1/public/basic?max-col=1&max-row=10")


class GDataPuller(object):

    # Tell our py2exe script to let XML/SSL be included.
    needs_xml = True
    needs_ssl = True

    def __init__(self, sheet_key):
        self.sheet_key = sheet_key

    def startText(self):
        return "Requesting config data from Google Spreadsheet..."

    def query(self):

        def f(url):
            return urllib.urlopen(url).read()

        d = deferToThread(f, PAGE_TEMPLATE % self.sheet_key)

        def cb(result):
            config_list = []
            doc = xml.dom.minidom.parseString(result)
            for c in doc.getElementsByTagName("content"):
                if c.firstChild:
                    config_list.append(str(c.firstChild.nodeValue))
            return config_list

        d.addCallback(cb)
        return d


class GDataUpdater(object):

    def __init__(self, email, password, sheet_key):
        self.email = email
        self.password = password
        self.sheet_key = sheet_key


    def update(self, entries):
        d = threads.deferToThread(self._submitData, entries)
        return d


    def _submitData(self, entries):
        keys = entries.keys()
        keys.sort()

        # Log in to Google Spreadsheets
        gd_client = gdata.spreadsheet.service.SpreadsheetsService()
        gd_client.email = self.email
        gd_client.password = self.password
        gd_client.source = "Dtella_GData_Updater_0"
        gd_client.ProgrammaticLogin()

        # Work within the upper-left 1x10 block of cells.
        query = gdata.spreadsheet.service.CellQuery()
        query.max_col = '1'
        query.max_row = '10'
        query.return_empty = 'true'

        # Get existing cells
        feed = gd_client.GetCellsFeed(self.sheet_key, '1', query=query)

        # Prepare batch object for updates.
        batch_feed = gdata.spreadsheet.SpreadsheetsCellsFeed()

        n_changes = 0

        # Walk through the existing cells
        for entry in feed.entry:

            # Decide what the new cell value should be
            try:
                k = keys[int(entry.cell.row) - 1]
                new_value = "%s=%s" % (k, entries[k])
            except IndexError:
                new_value = ""

            old_value = entry.cell.inputValue or ""

            # Update cells which have changed
            if old_value != new_value:
                entry.cell.inputValue = new_value
                batch_feed.AddUpdate(entry)
                n_changes += 1

        # Push the updates
        if batch_feed.entry:
            gd_client.ExecuteBatch(batch_feed, url=feed.GetBatchLink().href)

        return ("Cells modified = %d" % n_changes)