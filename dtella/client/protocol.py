"""
Dtella - Client Only Protocol
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import asyncio
from typing import TYPE_CHECKING

from dtella.common import BadTimingError, Reject
from dtella.common.packet import BadPacketError
from dtella.common.protocol import BaseProtocol, ACK_REJECT_BIT, PacketAK, ACK_PRIVATE
from dtella.util import Ad

if TYPE_CHECKING:
    from dtella.client.main import DtellaMain_Client
    from dtella.common.node import Node
    from dtella.client.dc import PacketCA, PacketCP, PacketPM, PacketSQ


class ClientProtocol(BaseProtocol):

    main: "DtellaMain_Client"

    def __init__(self, main: "DtellaMain_Client"):
        super().__init__(main)

    def _process_packet(self, data: bytes, ad: Ad):
        # Special handler for search results directly from DC
        if data[:4] == b'$SR ':
            if (dch := self.main.state_observer) is not None and self.main.auth('sb', ad):
                dch.push_SearchResult(data)
            return
        super()._process_packet(data, ad)

    def handle_packet_CA(self, ad: Ad, packet: "PacketCA"):

        # Common code for handling private messages (PM, CA, CP)

        # If we're not on the network, ignore it.
        osm = self.main.osm
        if not osm:
            raise BadTimingError("not ready to handle private message")

        ack_flags = 0

        try:
            # Make sure src_ipp agrees with the sender's IP
            self.main.check_source(packet.src_ipp, ad)

            # Make sure we're ready to receive it
            if (dch := self.main.dch) is None:
                raise Reject

            try:
                n = osm.get_node(packet.src_ipp)
            except KeyError:
                raise Reject("Unknown node")

            if packet.src_nhash != n.nick_hash:
                raise Reject("Source nickhash mismatch")

            if packet.dst_nhash != osm.me.nick_hash:
                raise Reject("Dest nickhash mismatch")

            if n.poke_pm_key(packet.ack_key):
                # SSLHACK: newer Dtella versions have an extra flags byte, to allow
                #          for SSL connection requests.  Try to decode both forms.

                if packet.port == 0:
                    raise BadPacketError("Zero port")

                dch.push_ConnectToMe(Ad((n.ipp[:4], packet.port)), packet.use_ssl)

        except (BadPacketError, BadTimingError, Reject):
            ack_flags |= ACK_REJECT_BIT

        # Send acknowledgement
        packet = PacketAK(src_ipp=osm.me.ipp, mode=ACK_PRIVATE, flags=ack_flags, ack_key=packet.ack_key)
        asyncio.create_task(self.main.ph.sendPacket(packet, Ad(packet.src_ipp)))

    def handle_packet_CP(self, ad: Ad, packet: "PacketCP"):
        # Common code for handling private messages (PM, CA, CP)

        # If we're not on the network, ignore it.
        osm = self.main.osm
        if not osm:
            raise BadTimingError("not ready to handle private message")

        ack_flags = 0

        try:
            # Make sure src_ipp agrees with the sender's IP
            self.main.check_source(packet.src_ipp, ad)

            # Make sure we're ready to receive it
            if (dch := self.main.dch) is None:
                raise Reject

            try:
                n = osm.get_node(packet.src_ipp)
            except KeyError:
                raise Reject("Unknown node")

            if packet.src_nhash != n.nick_hash:
                raise Reject("Source nickhash mismatch")

            if packet.dst_nhash != osm.me.nick_hash:
                raise Reject("Dest nickhash mismatch")

            if n.pokePMKey(packet.ack_key):
                n.open_RevConnect_window()
                dch.push_RevConnectToMe(n.nick)

        except (BadPacketError, BadTimingError, Reject):
            ack_flags |= ACK_REJECT_BIT

        # Send acknowledgement
        packet = PacketAK(src_ipp=osm.me.ipp, mode=ACK_PRIVATE, flags=ack_flags, ack_key=packet.ack_key)
        asyncio.create_task(self.main.ph.sendPacket(packet, Ad(packet.src_ipp)))

    def handle_packet_PM(self, ad: Ad, packet: "PacketPM"):
        # Common code for handling private messages (PM, CA, CP)

        # If we're not on the network, ignore it.
        osm = self.main.osm
        if not osm:
            raise BadTimingError("not ready to handle private message")

        ack_flags = 0

        try:
            # Make sure src_ipp agrees with the sender's IP
            self.main.check_source(packet.src_ipp, ad)

            # Make sure we're ready to receive it
            if (dch := self.main.dch) is None:
                raise Reject

            try:
                n = osm[packet.src_ipp]
            except KeyError:
                raise Reject("Unknown node")

            if packet.src_nhash != n.nick_hash:
                raise Reject("Source nickhash mismatch")

            if packet.dst_nhash != osm.me.nick_hash:
                raise Reject("Dest nickhash mismatch")

            if n.pokePMKey(packet.ack_key):
                if packet.notice:
                    dch.push_ChatMessage(f"*N {n.nick}", packet.text)
                else:
                    dch.push_PrivMsg(n.nick, packet.text)

        except (BadPacketError, BadTimingError, Reject):
            ack_flags |= ACK_REJECT_BIT

        # Send acknowledgement
        packet = PacketAK(src_ipp=osm.me.ipp, mode=ACK_PRIVATE, flags=ack_flags, ack_key=packet.ack_key)
        asyncio.create_task(self.main.ph.sendPacket(packet, Ad(packet.src_ipp)))

    def check_broadcast_SQ(self, src_n: "Node", packet: "PacketSQ"):
        super().check_broadcast_SQ(src_n, packet)
        if (dch := self.main.dch) is not None:
            dch.push_SearchRequest(packet.src_ipp, packet.search)
