from dtella.client.main import DtellaMain_Client
from dtella.client.protocol import ClientProtocol
from dtella.common import overwrite_modules

modules = {"PeerHandler": ClientProtocol}


def main(dc_port):
    overwrite_modules(modules)

    DtellaMain_Client(True).start(dc_port)
