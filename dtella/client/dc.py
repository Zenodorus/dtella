"""
Dtella - DirectConnect Interface Module
Copyright (C) 2008  Dtella Labs (http://www.dtella.org)
Copyright (C) 2008  Paul Marks
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
import asyncio
import binascii
import re
import struct
import time
from asyncio import StreamReader, StreamWriter, IncompleteReadError, LimitOverrunError
from typing import Dict, Tuple, Callable, Set, Optional, List, Union, TYPE_CHECKING, Coroutine

import dtella.common
import dtella.common.local_config as local
from dtella.common.log import LOG
from dtella.common.packet import BroadcastPacket, register_packet_classes, Packet, BadPacketError, AckablePacket
from dtella.util import validate_nick, lock2key, remove_dc_escapes, format_bytes, word_wrap, CHECK, encoding
from dtella.util.encoding import DecodingError
from dtella.util.ipv4 import Ad

if TYPE_CHECKING:
    from dtella.client.main import DtellaMain_Client
    from dtella.common.node import Node, User

# ConnectToMe Flags
USE_SSL_BIT = 0x1


class PacketCH(BroadcastPacket):
    """
    Public Chat Message Packet.
    """

    _code = b"CH"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'I'), ('nhash', '4s'), ('flags', 'B'), ('text', 'S2')]
    __slots__ = ('pktnum', 'nhash', 'flags', 'text')
    pktnum: int
    nhash: bytes
    flags: int
    text: str

    def __init__(self, **kwargs):
        self.flags = 0x00
        BroadcastPacket.__init__(self, **kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    notice = property(lambda self: PacketCH._get_flag(self, dtella.common.NOTICE_BIT),
                      lambda self, v: PacketCH._set_flag(self, dtella.common.NOTICE_BIT, v))

    slash_me = property(lambda self: PacketCH._get_flag(self, dtella.common.SLASHME_BIT),
                        lambda self, v: PacketCH._set_flag(self, dtella.common.SLASHME_BIT, v))


class PacketSQ(BroadcastPacket):
    """
    Search Request Packet.
    """

    _code = b"SQ"
    _data_model = [('code', '2s'), ('nb_ipp', '6s'), ('hops', 'B'), ('b_flags', 'B'), ('src_ipp', '6s'),
                   ('pktnum', 'I'), ('search', 'S1')]
    __slots__ = ('pktnum', 'search')
    pktnum: int
    search: str


class PacketCA(AckablePacket):
    """
    ConnectToMe Packet.
    """

    _code = b"CA"
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('ack_key', '8s'), ('src_nhash', '4s'), ('dst_nhash', '4s'),
                   ('flags', 'B'), ('port', 'H')]
    __slots__ = ('src_ipp', 'src_nhash', 'dst_nhash', 'flags', 'port')
    src_ipp: bytes
    src_nhash: bytes
    dst_nhash: bytes
    flags: int
    port: int

    def __init__(self, **kwargs):
        self.flags = 0x00
        Packet.__init__(self, **kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    use_ssl = property(lambda self: PacketCA._get_flag(self, USE_SSL_BIT),
                       lambda self, v: PacketCA._set_flag(self, USE_SSL_BIT, v))

    @classmethod
    def decode(cls, data: bytes) -> "PacketCA":
        """
        Decode raw data into a Packet following its _data_model.  This method is patched for older versions of Dtella
        where the flags byte is omitted when zero.
        """
        decoded = {"_data": data}
        try:
            for (key, fmt) in cls._data_model[:5]:
                value, data = encoding.decode_item(fmt, data)
                decoded[key] = value
            if len(data) == 2:
                decoded["flags"], decoded["port"] = 0, struct.unpack('!H', data)
            elif len(data) == 3:
                decoded["flags"], decoded["port"] = struct.unpack('!BH', data)
            else:
                raise BadPacketError(f"extra data decoding {cls.__name__}")
        except DecodingError as e:
            raise BadPacketError(f"could not decode packet: {e}")

        return cls(**decoded)


class PacketCP(AckablePacket):
    """
    RevConnectToMe Packet.
    """

    _code = b"CP"
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('ack_key', '8s'), ('src_nhash', '4s'), ('dst_nhash', '4s')]
    __slots__ = ('src_ipp', 'src_nhash', 'dst_nhash')
    src_ipp: bytes
    src_nhash: bytes
    dst_nhash: bytes


class PacketPM(AckablePacket):
    """
    Private Message Packet.
    """

    _code = b"PM"
    _data_model = [('code', '2s'), ('src_ipp', '6s'), ('ack_key', '8s'), ('src_nhash', '4s'), ('dst_nhash', '4s'),
                   ('flags', 'B'), ('text', 'S2')]
    __slots__ = ('src_ipp', 'src_nhash', 'dst_nhash', 'flags', 'text')
    src_ipp: bytes
    src_nhash: bytes
    dst_nhash: bytes
    flags: int
    text: str

    def __init__(self, **kwargs):
        self.flags = 0x00
        super().__init__(**kwargs)

    def _get_flag(self, bit_code: int) -> bool:
        return bool(self.flags & bit_code)

    def _set_flag(self, bit_code: int, bool_value: bool):
        if bool_value:
            self.flags |= bit_code
        else:
            self.flags &= (0xFF - bit_code)

    notice = property(lambda self: PacketPM._get_flag(self, dtella.common.NOTICE_BIT),
                      lambda self, v: PacketPM._set_flag(self, dtella.common.NOTICE_BIT, v))


register_packet_classes(PacketCH, PacketSQ, PacketCA, PacketCP, PacketPM)


LOCK_STR = "EXTENDEDPROTOCOLABCABCABCABCABCABC Pk=DTELLA"


##############################################################################


class AbortHandler(Exception):
    pass


class AbortDCHandler(object):

    delimiter: bytes = b'|'
    MAX_TIME: float = 5.0

    def __init__(self, nick: str, reader: StreamReader, writer: StreamWriter):
        self._nick: str = nick
        self._reader: StreamReader = reader
        self._writer: StreamWriter = writer
        self._start_time: float = time.monotonic()
        self._task: asyncio.Task = asyncio.create_task(self._run())

    async def _run(self):
        try:
            while line := await asyncio.wait_for(self._reader.readuntil(self.delimiter),
                                                 timeout=AbortOut.MAX_TIME - time.monotonic() + self._start_time):
                LOG.debug(f"DCH {self.__class__.__name__}<: {line}")
                assert line[-1] == ord(self.delimiter), "malformed NMDC line"
                line = line[:-1]
                cmd = line.split(b' ', 1)

                if cmd[0] == b"$Lock":
                    await self.d_Lock(*cmd[1].split(b' ', 1))
                elif cmd[0] == b"$Key":
                    await self.d_Key(cmd[1])
        except (IncompleteReadError, LimitOverrunError, asyncio.TimeoutError, TypeError):
            pass
        finally:
            self._writer.close()

    async def d_Lock(self, lock: bytes, pk: bytes):
        raise NotImplementedError()

    async def d_Key(self, key: bytes):
        raise NotImplementedError()

    def _send_line(self, line: Union[str, bytes]):
        LOG.debug(f"DCH {self.__class__.__name__}>: {line}")
        if isinstance(line, str):
            self._writer.writelines((line.replace('|', '&#124;').encode(), self.delimiter))
        else:
            self._writer.writelines((line.replace(b'|', b'&#124;'), self.delimiter))


class AbortOut(AbortDCHandler):

    # if I initiate the connection:
    # send $MyNick + $Lock
    # (catch $Lock)
    # wait for $Key
    # -> send $Supports + $Direction + $Key + $Error

    def __init__(self, nick: str, reader: StreamReader, writer: StreamWriter):
        super().__init__(nick, reader, writer)
        self._lock: bytes = b""

    async def _run(self):
        self._send_line(f"$MyNick {self._nick}")
        self._send_line(f"$Lock {LOCK_STR}")
        await self._writer.drain()
        await super()._run()

    async def d_Lock(self, lock: bytes, pk: bytes):
        self._lock = lock

    async def d_Key(self, key: bytes):
        if self._lock.startswith(b"EXTENDEDPROTOCOL"):
            self._send_line("$Supports ADCGet TTHL TTHF")
        self._send_line("$Direction Upload 12345")
        self._send_line(f"$Key {lock2key(self._lock)}")
        self._send_line("$Error File Not Available")
        await self._writer.drain()


class AbortIn(AbortDCHandler):

    # if I receive the connection:
    # receive $MyNick
    # build this protocol and pass the reader and writer
    # wait for $Lock
    # -> send $MyNick + $Lock + $Supports + $Direction + $Key
    # wait for $Key
    # -> send $Error

    async def d_Lock(self, lock: bytes, pk: bytes):
        self._send_line(f"$MyNick {self._nick}")
        self._send_line(f"$Lock {LOCK_STR}")
        if lock.startswith(b"EXTENDEDPROTOCOL"):
            self._send_line("$Supports ADCGet TTHL TTHF")
        self._send_line("$Direction Upload 12345")
        self._send_line(f"$Key {lock2key(lock)}")
        await self._writer.drain()

    async def d_Key(self, key: bytes):
        self._send_line("$Error File Not Available")
        await self._writer.drain()


##############################################################################


# DC Protocol
# https://sourceforge.net/p/nmdc/svnroot/HEAD/tree/trunk/Other/mutor_protocoldoc.htm?format=raw
# https://nmdc.sourceforge.io/NMDC.html
#
# Client-Hub Login                                                      Hub State
# C>H: Connection                                                       login_1
# H>C: $Lock<lock> Pk=<pk>|
# H>C: $HubName<hubname>|
# C>H: $Key<key>|$ValidateNick<nick>|                                   login_1->login_2
# H>C: $Hello <nick>
# C>H: $MyINFO $ALL<nick><interest>$ $<speed>$<e-mail>$<sharesize>$
# C>H: $GetNickList                                                     login_2->queued/ready
#
# Note: $Key is ignored by Dtella, except for the abort connections above

# TODO better state management
class DCHandler(object):
    """
    Handler for the DC protocol.  Dtella acts as the hub of a typical DC network.

    Attributes:
        _info: info string that comes from DC client
    """

    delimiter: bytes = b'|'
    _abort_nick: str = ""

    def __init__(self, main: "DtellaMain_Client", reader: StreamReader, writer: StreamWriter):
        self.main: "DtellaMain_Client" = main
        self._reader: StreamReader = reader
        self._writer: StreamWriter = writer
        self._dispatch: Dict[bytes, Tuple[int, Callable[..., Coroutine[None, None, None]]]] = {}
        self._hub_ad = Ad(self._writer.get_extra_info('sockname'))

        # TODO check if this is set for the connection
        # self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, enabled)
        # self.transport.setTcpNoDelay(True)

        self._info: str = ''
        self._nick: str = ''
        self.bot = DtellaBot(self, '*Dtella')

        # Handlers which can be used before attaching to Dtella
        self._dispatch[b'$ValidateNick'] = (1, self.d_ValidateNick)
        self._dispatch[b'$GetNickList'] = (0, self.d_GetNickList)
        self._dispatch[b'$MyINFO'] = (-3, self.d_MyInfo)
        self._dispatch[b'$GetINFO'] = (2, self.d_GetInfo)
        self._dispatch[b''] = (0, self.d_KeepAlive)
        self._dispatch[b'$KillDtella'] = (0, self.d_KillDtella)
        self._dispatch[b'$MyNick'] = (1, self.d_MyNick)

        # Chat messages waiting to be sent
        self._chat_queue: List[Tuple[int, str]] = []
        self._chat_counter: int = 99999
        self._chat_rate_task: asyncio.Task = asyncio.create_task(self._chat_rate_control())

        # ['login_1', 'login_2', 'queued', 'ready', 'invisible']
        self.state: str = 'login_1'
        self._loginblockers: Set[str] = {'MyINFO', 'GetNickList'}

        self._queued_task: Optional[asyncio.Task] = None
        self._auto_rejoin_task: Optional[asyncio.Task] = None
        self._task: asyncio.Task = asyncio.create_task(self._run())
        self._init_task: asyncio.Task = asyncio.create_task(self._init())

    async def _init(self):
        """
        If $RevConnect needs to be terminated, the port this handler listens to will be used to fake a $RevConnect
        handshake so that an error can be sent and the DC client will stop trying to $RevConnect.  Initial connection
        to the hub will be delayed in case a $MyNick is sent (signifying that the node faked a $RevConnect).
        """
        if DCHandler._abort_nick:
            await asyncio.sleep(1.0)
        self._send_line("$Lock FOO Pk=BAR")
        self.push_Hubname()

    async def _run(self):
        close_stream = True
        try:
            while line := await self._reader.readuntil(self.delimiter):
                LOG.debug(f"DCH<: {line}")
                assert line[-1] == ord(self.delimiter), "malformed NMDC line"
                line = line[:-1]
                cmd = line.split(b' ', 1)

                # Do a dict lookup to find the parameters for this command
                try:
                    nargs, fn = self._dispatch[cmd[0]]
                except KeyError:
                    # Unknown DC message
                    continue

                # Create the argument list
                if len(cmd) <= 1:
                    args = []
                elif nargs < 0:
                    nargs = -nargs
                    args = cmd[1].split(b' ', nargs - 1)
                else:
                    args = cmd[1].split(b' ', nargs)

                if len(args) == nargs:
                    try:
                        await fn(*args)
                    except AbortHandler as e:
                        # elevate out of the loop
                        raise e
                    except Exception as e:
                        import traceback
                        LOG.warn(f"DCHandler dispatch error for {cmd[0].decode()}.  {e.__class__.__name__}: {e.args}")
                        LOG.error(traceback.format_exc())

                # clear the buffer before taking any more input
                await self._writer.drain()
        except IncompleteReadError:
            pass
        except LimitOverrunError:
            pass
        except AbortHandler:
            close_stream = False
        finally:
            self.main.remove_dch(self)

            self._init_task.cancel()
            self._chat_rate_task.cancel()
            if self._queued_task:
                self._queued_task.cancel()
            if self._auto_rejoin_task:
                self._auto_rejoin_task.cancel()
            if close_stream:
                self.close()

    def _send_line(self, line: Union[str, bytes]):
        LOG.debug(f"DCH>: {line}")
        if isinstance(line, str):
            self._writer.writelines((line.replace('|', '&#124;').encode(), self.delimiter))
        else:
            self._writer.writelines((line.replace(b'|', b'&#124;'), self.delimiter))

    @property
    def is_online(self) -> bool:
        osm = self.main.osm
        return self.state == 'ready' and osm and osm.synced

    def fatal_error(self, text: str):
        self.push_Status(f"ERROR: {text}")
        self.close()

    def close(self):
        DCHandler._abort_nick = ""
        self._writer.close()

    @property
    def nick(self):
        return self._nick

    @property
    def info(self):
        # Build and return a hacked-up version of my info string.

        # Get version string
        ver_string = local.version_tag
        info = self._info.split('$')
        if len(info) != 6:
            return f"{ver_string}"

        # splice in dtella version to the tag
        if m := re.match("(.*)<([^<]*)>$", info[0]):
            info[0] = f"{m.group(1)}<{m.group(2)},{ver_string}>"
        else:
            info[0] = f"{info[0]}<{ver_string}>"

        # replace connection with location
        if local.use_locations:
            try:
                # try to get my location name and append location suffix, if it exists
                loc = self.main.location[str(Ad(self.main.osm.me.ipp))]
                if not loc:
                    raise KeyError

                if suffix := self.main.state.suffix:
                    loc = f"{loc}|{suffix}"

                info[2] = loc + info[2][-1]
            except (AttributeError, KeyError):
                pass

        info = '$'.join(info)

        if len(info) > 255:
            self.push_Status("*** Your info string is too long!")
            info = ''

        return info

    # Client-Hub login methods

    def push_ChatMessage(self, nick: str, text: str):
        self._send_line(f"<{nick}> {text}")

    def push_Status(self, text: str):
        self.push_ChatMessage(self.bot.nick, text)

    def push_Hubname(self, topic: Optional[str] = None):
        if topic:
            self._send_line(f"$HubName {local.hub_name} - {topic}")
        else:
            self._send_line(f"$HubName {local.hub_name}")

    async def d_ValidateNick(self, nick: bytes):
        self._init_task.cancel()

        nick = nick.decode()
        if self.state != 'login_1':
            self.fatal_error("$ValidateNick not expected.")
            return
        self.state = 'login_2'

        if reason := validate_nick(nick):
            self.push_Status("Your nick is invalid: %s" % reason)
            self.push_Status("Please fix it and reconnect.  Goodbye.")
            self.close()
            return

        self._nick = nick
        self.push_Hello(self._nick)

    def push_Hello(self, nick: str):
        self._send_line(f"$Hello {nick}")

    async def d_GetNickList(self):
        if self.state == 'login_1':
            self.fatal_error("Got $GetNickList, expected $ValidateNick")
            return

        # Me and the bot are ALWAYS online
        # Add in the Dtella nicks, if we're fully online (DC and Dtella)
        nicks = [self.bot.nick, self._nick]
        if self.is_online:
            nicks = set(nicks)
            nicks.update(n.nick for n in self.main.osm.user_list)
            nicks = list(nicks)
        nicks.sort()

        self._send_line(f"$NickList {'$$'.join(nicks)}$$")
        self._send_line(f"$OpList {self.bot.nick}$$")

        if self.state == 'login_2':
            self.remove_login_blocker('GetNickList')

    async def d_MyInfo(self, _1: bytes, _2: bytes, info: bytes):
        if self.state == 'login_1':
            self.fatal_error("Got $MyINFO, expected $ValidateNick")
            return

        self._info = info.decode().replace('\r', '').replace('\n', '')

        if self.state == 'login_2':
            self.remove_login_blocker('MyINFO')
        elif self.is_online:
            self.main.osm.updateMyInfo()

    def push_Info(self, nick: str, dcinfo: str):
        self._send_line(f'$MyINFO $ALL {nick} {dcinfo}')

    async def d_GetInfo(self, nick: bytes, _: bytes):
        nick = nick.decode()
        if nick == self.bot.nick:
            dcinfo = "Local Dtella Bot$ $Bot\x01$$0$"
            self.push_Info(nick, dcinfo)
            return

        if not self.is_online:
            return

        try:
            n = self.main.osm.get_user(nick)
        except KeyError:
            return

        if n.dc_info:
            self.push_Info(n.nick, n.dc_info)

    async def d_KillDtella(self):
        asyncio.get_running_loop().stop()

    async def d_MyNick(self, nick: bytes):
        """
        This command is received when the DC client is first connecting and when the node faked a $RevConnect to send
        an error.
        """

        self._init_task.cancel()

        if self.state != 'login_1':
            self.fatal_error("$MyNick not expected.")
            return

        if not DCHandler._abort_nick:
            self.close()
            return

        # transfer the streamers to the abort protocol and raise an error to terminate this instance
        AbortIn(DCHandler._abort_nick, self._reader, self._writer)
        DCHandler._abort_nick = ""
        raise AbortHandler

    def remove_login_blocker(self, blocker: str):
        CHECK(self.state == 'login_2')

        try:
            self._loginblockers.remove(blocker)
            if self._loginblockers:
                return
        except KeyError:
            return

        LOG.debug("DC connection ready.  Connecting to Dtella network")

        if self.main.dch is None:
            self.attach_me_to_dtella()
        elif self.main.pending_dch is None:
            self.state = 'queued'
            self.main.pending_dch = self

            async def cb(sec):
                await asyncio.sleep(sec)
                self._queued_task = None
                self.main.pending_dch = None
                self.push_Status("Nope, it didn't leave.  Goodbye.")
                self.close()

            self.push_Status("Another DC client is already using Dtella on this computer.")
            self.push_Status("Waiting 5 seconds for it to leave.")
            self._queued_task = asyncio.create_task(cb(5.0))
        else:
            self.push_Status("Dtella is busy with other DC connections from your computer.  Goodbye.")
            self.close()

    def attach_me_to_dtella(self):
        assert self.main.dch is None

        if self.state == 'queued':
            self.push_Status("The other client left.  Resuming normal connection.")
        if self._queued_task is not None:
            self._queued_task.cancel()

        # Add the post-login handlers
        self._dispatch[b'$ConnectToMe'] = (2, self.d_ConnectToMe)
        self._dispatch[b'$RevConnectToMe'] = (2, self.d_RevConnectToMe)
        self._dispatch[b'$Search'] = (-2, self.d_Search)
        self._dispatch[b'$To:'] = (-5, self.d_PrivateMsg)
        self._dispatch[f"<{self._nick}>".encode()] = (-1, self.d_PublicMsg)

        # Announce my presence.  If Dtella's online too, this will trigger an event_DtellaUp.
        self.state = 'ready'
        self.main.set_dch(self)

    # Client-Client protocol

    async def d_Search(self, addr_string: bytes, search_string: bytes):
        # Send a search request

        if not self.is_online:
            self.push_Status("Can't Search: Not online!")
            return
        elif len(search_string) > 255:
            self.push_Status("Search string too long")
            return

        osm = self.main.osm

        packet = osm.mrm.broadcastHeader('SQ', osm.me.ipp)
        packet.append(struct.pack('!I', osm.mrm.getPacketNumber_search()))

        packet.append(struct.pack('!B', len(search_string)))
        packet.append(search_string)

        osm.mrm.newMessage(''.join(packet), tries=4)

        # If local searching is enabled, send the search to myself
        if self.main.state.localsearch:
            self.push_SearchRequest(osm.me.ipp, search_string.decode())

    async def d_PrivateMsg(self, nick: bytes, _1: bytes, _2: bytes, _3: bytes, text: bytes):
        """Send a private message to a user."""

        nick = nick.decode()
        text = remove_dc_escapes(text.decode())

        if nick == self.bot.nick:

            # No ! is needed for commands in the private message context
            if text[0] == '!':
                text = text[1:]

            def out(text):
                if text is not None:
                    self.bot.say(text)

            self.bot.commandInput(out, text)
            return

        short_text = text[:10] + '...' if len(text) > 10 else text

        if not self.is_online:
            self.push_PrivMsg(nick, f"*** Your message \"{short_text}\" could not be sent: you're not online.")
            return

        try:
            n = self.main.osm.get_user(nick)
        except KeyError:
            self.push_PrivMsg(nick, f"*** Your message \"{short_text}\" could not be sent: user doesn't seem to exist.")
            return

        if len(text) > 1024:
            text = text[:1024-12] + ' [Truncated]'

        if fail_result := await n.event_PrivateMessage(PacketPM(flags=0, text=text)):
            self.push_PrivMsg(nick, f"*** Your message \"{short_text}\" could not be sent: {fail_result}")

    async def d_ConnectToMe(self, nick: bytes, addr: bytes):
        nick = nick.decode()

        try:
            port, use_ssl = re.match("\d+[.]\d+[.]\d+[.]\d+:(\d+)(S?)", addr.decode()).groups()
            port, use_ssl = int(port), bool(use_ssl)
            if not (1 <= port <= 65535):
                raise ValueError
        except (AttributeError, ValueError):
            self.push_Status(f"*** Connection to <{nick}> failed: malformed address: <{addr.decode()}>")
            return

        if not self.is_online:
            self.push_Status(f"*** Connection to <{nick}> failed: you're not online.")
            if not use_ssl:
                AbortOut(nick, *(await asyncio.open_connection(host="127.0.0.1", port=port)))
            return

        try:
            n = self.main.osm.get_user(nick)
        except KeyError:
            if nick == self.bot.nick:
                self.push_Status(f"*** Connection to <{nick}> failed: can't get files from yourself!")
            else:
                self.push_Status(f"*** Connection to <{nick}> failed: user doesn't seem to exist.")
            if not use_ssl:
                AbortOut(nick, *(await asyncio.open_connection(host="127.0.0.1", port=port)))
            return

        suppress_error = False
        if n.poke_RevConnect_window():
            # If we're responding to a RevConnect, disable errors
            suppress_error = True

        elif self.is_leech():
            if not use_ssl:
                AbortOut(nick, *(await asyncio.open_connection(host="127.0.0.1", port=port)))
            return

        if fail_result := await n.event_ConnectToMe(PacketCA(flags=use_ssl and USE_SSL_BIT, port=port)):
            if not suppress_error:
                self.push_Status(f"*** Connection to <{nick}> failed: {fail_result}")
            if not use_ssl:
                AbortOut(nick, *(await asyncio.open_connection(host="127.0.0.1", port=port)))

    async def d_RevConnectToMe(self, _, nick: bytes):
        nick = nick.decode()

        if not self.is_online:
            self.push_Status(f"*** Connection to <{nick}> failed: you're not online.")
            DCHandler._abort_nick = nick
            self._send_line(f"$ConnectToMe {self._nick} {self._hub_ad}")
            return

        try:
            n = self.main.osm.get_user(nick)
        except KeyError:
            if nick == self.bot.nick:
                self.push_Status(f"*** Connection to <{nick}> failed: can't get files from yourself!")
            else:
                self.push_Status(f"*** Connection to <{nick}> failed: user doesn't seem to exist.")
            DCHandler._abort_nick = nick
            self._send_line(f"$ConnectToMe {self._nick} {self._hub_ad}")
            return

        if self.is_leech():
            # I'm a leech
            DCHandler._abort_nick = nick
            self._send_line(f"$ConnectToMe {self._nick} {self._hub_ad}")
            return

        if fail_result := await n.event_RevConnectToMe(PacketCP()):
            self.push_Status(f"*** Connection to <{nick}> failed: {fail_result}")
            DCHandler._abort_nick = nick
            self._send_line(f"$ConnectToMe {self._nick} {self._hub_ad}")

    def is_leech(self):
        osm = self.main.osm
        minshare = self.main.dcfg.minshare

        if osm.me.shared < minshare:
            self.push_Status(
                f"*** You must share at least {format_bytes(minshare)} in order to download!  "
                f"(You currently have {format_bytes(osm.me.shared)})")
            return True

        return False

    async def d_PublicMsg(self, text: bytes):
        text = remove_dc_escapes(text.decode())

        # Route commands to the bot
        if text[:1] == '!':

            async def out(out_text, flag=[True]):

                # If the bot produces output, inject our text input before
                # the first line.
                if flag[0]:
                    self.push_Status("You commanded: %s" % text)
                    flag[0] = False

                if out_text is not None:
                    self.push_Status(out_text)

            if self.bot.commandInput(out, text[1:], '!'):
                return

        if not self.is_online:
            self.push_Status("*** You must be online to chat!")
            return

        if self.main.osm.is_moderated:
            self.push_Status("*** Can't send text; the chat is currently moderated.")
            return

        text = text.replace('\r\n', '\n').replace('\r', '\n')
        for line in filter(None, text.split('\n')):
            # Limit length
            if len(line) > 1024:
                line = line[:1024 - 12] + ' [Truncated]'

            flags = 0

            # Check for /me
            if len(line) > 4 and line[:4].lower() in ('/me ', '+me ', '!me '):
                line = line[4:]
                flags |= dtella.common.SLASHME_BIT

            # Check rate limiting
            if self._chat_counter > 0:

                # Send now
                self._chat_counter -= 1
                self.broadcast_ChatMessage(flags, line)

            else:
                # Put in a queue
                if len(self._chat_queue) < 5:
                    self._chat_queue.append((flags, line))
                else:
                    self.push_Status("*** Chat throttled.  Stop typing so much!")
                    break

    async def d_KeepAlive(self):
        # Doesn't do much really
        self._send_line(b'')

    def push_Quit(self, nick: str):
        self._send_line('$Quit %s' % nick)

    def push_ConnectToMe(self, ad: Ad, use_ssl: bool):
        self._send_line(f"$ConnectToMe {self._nick} {ad.get_text_ip_port()}" + ('S' if use_ssl else ''))

    def push_RevConnectToMe(self, nick: str):
        self._send_line(f"$RevConnectToMe {nick} {self._nick}")

    def push_SearchRequest(self, ipp: bytes, search_string: str):
        self._send_line(f"$Search {Ad().set_addr(ipp).get_text_ip_port()} {search_string}")

    def push_PrivMsg(self, nick: str, text: str):
        self._send_line(f"$To: {self._nick} From: {nick} $<{nick}> {text}")

    async def _chat_rate_control(self):
        while True:
            if self._chat_queue:
                args = self._chat_queue.pop(0)
                self.broadcast_ChatMessage(*args)
            else:
                self._chat_counter = min(5, self._chat_counter + 1)

            await asyncio.sleep(1)

    def broadcast_ChatMessage(self, flags: int, text: str):
        assert self.is_online
        osm = self.main.osm

        if osm.is_moderated:
            # If the channel went moderated with something in the queue, wipe it out and don't send.
            del self._chat_queue[:]
            return

        packet = PacketCH(nb_ipp=osm.me.ipp, hops=32, b_flags=0, src_ipp=osm.me.ipp,
                          pktnum=osm.mrm.getPacketNumber_chat(), nhash=osm.me.nick_hash, flags=flags,
                          text=text)
        osm.mrm.new_message(packet, tries=4)

        # Echo back to the DC client
        if flags & dtella.common.SLASHME_BIT:
            nick = "*"
            text = f"{osm.me.nick} {text}"
        else:
            nick = osm.me.nick

        self.push_ChatMessage(nick, text)

    def push_SearchResult(self, data: bytes):
        data = data.decode()
        m = re.compile(r"^\$SR ([^ |]+) ([^|]*) \([^ |]+\)\|?$").match(data)
        if not m:
            # Doesn't look like a search reply
            return

        nick = m.group(1)
        data = m.group(2)

        # If I get results from myself, map them to the bot's nick
        if nick == self._nick:
            nick = self.bot.nick

        self._send_line(f"$SR {nick} {data} ({self._hub_ad})")

    def remote_nick_collision(self):

        text = (
            "*** Another node on the network has reported that your nick "
            "seems to be in a conflicting state.  This could prevent your "
            "chat and search messages from reaching everyone, so it'd be "
            "a good idea to try changing your nick.  Or you could wait "
            "and see if the problem resolves itself."
        )

        for line in word_wrap(text):
            self.push_Status(line)

    def do_rejoin(self):
        if self.state != 'invisible':
            return
        if self._auto_rejoin_task:
            self._auto_rejoin_task.cancel()

        self.state = 'ready'
        # This can trigger an event_DtellaUp()
        self.main.stateChange_ObserverUp()

    def is_protected_nick(self, nick: str):
        return nick.lower() in (self._nick.lower(), self.bot.nick.lower())

    def event_DtellaUp(self):
        assert self.is_online
        asyncio.create_task(self.d_GetNickList())

        # Grab the current topic from Dtella.
        tm = self.main.osm.tm
        self.push_Hubname(tm.topic)
        if tm.topic:
            self.push_Status(tm.getFormattedTopic())

    def event_DtellaDown(self):
        assert self.is_online

        # Wipe out the topic and my outgoing chat queue
        self.push_Hubname()
        del self._chat_queue[:]

    def event_KickMe(self, lines: List[str], rejoin_time: float):
        # Sequence of events during a kick:
        # 1. event_RemoveNick(*)
        # 2. event_DtellaDown()
        # 3. event_KickMe()
        # 4. stateChange_ObserverDown()

        # Node will become visible again if:
        # - Dtella node loses its connection
        # - User types !REJOIN
        # - DC client reconnects (creates a new DCHandler)

        CHECK(self.state == 'ready')
        self.state = 'invisible'

        for line in lines:
            self.push_Status(line)

        if rejoin_time is None:
            return

        # Automatically rejoin the chat after a timeout period.
        async def cb(sec):
            await asyncio.sleep(sec)
            self._auto_rejoin_task = None
            self.push_Status("Automatically rejoining...")
            self.do_rejoin()

        CHECK(self._auto_rejoin_task is None)
        self._auto_rejoin_task = asyncio.create_task(cb(rejoin_time))

    def event_AddNick(self, n: "User"):
        if not self.is_protected_nick(n.nick):
            self.push_Hello(n.nick)

    def event_RemoveNick(self, n: "User", reason: str):
        if not self.is_protected_nick(n.nick):
            self.push_Quit(n.nick)

    def event_UpdateInfo(self, n: "User"):
        if n.dc_info:
            self.push_Info(n.nick, n.dc_info)

    def event_ChatMessage(self, n: "User", nick: str, text: str, flags: int):
        if flags & dtella.common.NOTICE_BIT:
            self.push_ChatMessage(f"*N# {nick}", text)
        elif flags & dtella.common.SLASHME_BIT:
            self.push_ChatMessage("*", f"{nick} {text}")
        else:
            self.push_ChatMessage(nick, text)

    def log_prefix(self):
        """
        Return a prefix matching the class name, to identify log messages
        related to this protocol instance.
        """
        return self.__class__.__name__


##############################################################################


class DtellaBot(object):
    # This holds the logic behind the "*Dtella" user

    def __init__(self, dch: DCHandler, nick):
        self.dch: DCHandler = dch
        self.main = dch.main
        self.nick = nick

        self.dbg_show_packets = False

    def say(self, txt):
        self.dch.push_PrivMsg(self.nick, txt)

    def commandInput(self, out, line, prefix=''):

        # Sanitize
        line = line.replace('\r', ' ').replace('\n', ' ')

        cmd = line.upper().split()

        if not cmd:
            return False

        try:
            f = getattr(self, 'handleCmd_' + cmd[0])
        except AttributeError:
            if prefix:
                return False
            else:
                out("Unknown command '%s'.  Type %sHELP for help." %
                    (cmd[0], prefix))
                return True

        # Filter out location-specific commands
        if not local.use_locations:
            if cmd[0] in self.location_cmds:
                return False

        if cmd[0] in self.freeform_cmds:
            try:
                text = line.split(' ', 1)[1]
            except IndexError:
                text = None

            f(out, text, prefix)

        else:
            def wrapped_out(line):
                for l in word_wrap(line):
                    if l:
                        asyncio.create_task(out(l))
                    else:
                        asyncio.create_task(out(" "))

            f(wrapped_out, cmd[1:], prefix)

        return True

    def syntaxHelp(self, out, key, prefix):

        try:
            head = self.bighelp[key][0]
        except KeyError:
            return

        out("Syntax: %s%s %s" % (prefix, key, head))
        out("Type '%sHELP %s' for more information." % (prefix, key))

    freeform_cmds = frozenset(['TOPIC', 'SUFFIX', 'DEBUG'])

    location_cmds = frozenset(['SUFFIX', 'USERS', 'SHARED', 'DENSE'])

    minihelp = [
        ("--", "ACTIONS"),
        ("REJOIN", "Hop back online after a kick or collision"),
        ("ADDPEER", "Add the address of another node to your cache"),
        ("INVITE", "Show your current IP and port to give to a friend"),
        ("REBOOT", "Exit from the network and immediately reconnect"),
        ("TERMINATE", "Completely kill your current Dtella process."),
        ("--", "SETTINGS"),
        ("TOPIC", "View or change the global topic"),
        ("SUFFIX", "View or change your location suffix"),
        ("UDP", "Change Dtella's peer communication port"),
        ("LOCALSEARCH", "View or toggle local search results."),
        ("PERSISTENT", "View or toggle persistent mode"),
        ("--", "INFORMATION"),
        ("VERSION", "View information about your Dtella version."),
        ("USERS", "Show how many users exist at each location"),
        ("SHARED", "Show how many bytes are shared at each location"),
        ("DENSE", "Show the bytes/user density for each location"),
        ("RANK", "Compare your share size with everyone else"),
    ]

    bighelp = {
        "REJOIN": (
            "",
            "If you are kicked from the chat system, or if you attempt to use "
            "a nick which is already occupied by someone else, your node "
            "will remain connected to the peer network in an invisible state. "
            "If this happens, you can use the REJOIN command to hop back "
            "online.  Note that this is only useful after a nick collision "
            "if the conflicting nick has left the network."
        ),

        "TOPIC": (
            "<text>",
            "If no argument is provided, this command will display the "
            "current topic for the network.  This is the same text which "
            "is shown in the title bar.  If you provide a string of text, "
            "this will attempt to set a new topic.  Note that if Dtella "
            "is bridged to an IRC network, the admins may decide to lock "
            "the topic to prevent changes."
        ),

        "SUFFIX": (
            "<suffix>",
            "This command appends a suffix to your location name, which "
            "is visible in the Speed/Connection column of everyone's DC "
            "client.  Typically, this is where you put your room number. "
            "If you provide no arguments, this will display the "
            "current suffix.  To clear the suffix, just follow the command "
            "with a single space."
        ),

        "TERMINATE": (
            "",
            "This will completely kill your current Dtella node.  If you "
            "want to rejoin the network afterward, you'll have to go "
            "start up the Dtella program again."
        ),

        "VERSION": (
            "",
            "This will display your current Dtella version number.  If "
            "available, it will also display the minimum required version, "
            "the newest available version, and a download link."
        ),

        "LOCALSEARCH": (
            "<ON | OFF>",
            "If local searching is enabled, then when you search, you will "
            "see search results from the *Dtella user, which are actually "
            "hosted on your computer.  Use this command without any arguments "
            "to see whether local searching is currently enabled or not."
        ),

        "USERS": (
            "",
            "This will list all the known locations, and show how many "
            "people are currently connecting from each."
        ),

        "SHARED": (
            "",
            "This will list all the known locations, and show how many "
            "bytes of data are being shared from each."
        ),

        "DENSE": (
            "",
            "This will list all the known locations, and show the calculated "
            "share density (bytes-per-user) for each."
        ),

        "RANK": (
            "<nick>",
            "Compare your share size with everyone else in the network, and "
            "show which place you're currently in.  If <nick> is provided, "
            "this will instead display the ranking of the user with that nick."
        ),

        "UDP": (
            "<port>",
            "Specify a port number between 1-65536 to change the UDP port "
            "that Dtella uses for peer-to-peer communication.  If you don't "
            "provide a port number, this will display the port number which "
            "is currently in use."
        ),

        "ADDPEER": (
            "<ip>:<port>",
            "If Dtella is unable to locate any neighbor nodes using the "
            "remote config data or your local neighbor cache, then you "
            "can use this command to manually add the address of an existing "
            "node that you know about."
        ),

        "INVITE": (
            "",
            "If you wish to invite another user to join the network using the "
            "!ADDPEER command, you can use this command to retrieve your "
            "current IP and port to give to them to use."
        ),

        "REBOOT": (
            "",
            "This command takes no arguments.  It will cause your node to "
            "exit from the network, and immediately restart the connection "
            "process.  Use of this command shouldn't be necessary for "
            "normal operation."
        ),

        "PERSISTENT": (
            "<ON | OFF>",
            "This option controls how Dtella will behave when it is not "
            "attached to a Direct Connect client.  When PERSISTENT mode is "
            "OFF, Dtella will automatically close its peer connection after "
            "5 minutes of inactivity.  When this mode is ON, Dtella will "
            "try to stay connected to the network continuously.  To see "
            "whether PERSISTENT is enabled, enter the command with no "
            "arguments."
        )
    }

    def handleCmd_HELP(self, out, args, prefix):

        if len(args) == 0:
            out("This is your local Dtella bot.  You can send messages here "
                "to control the various features of Dtella.  A list of "
                "commands is provided below.  Note that you can PM a command "
                "directly to the %s user, or enter it in the main chat "
                "window prefixed with an exclamation point (!)" % self.nick)

            for command, description in self.minihelp:

                # Filter location-specific commands
                if not local.use_locations:
                    if command in self.location_cmds:
                        continue

                if command == "--":
                    out("")
                    out("  --%s--" % description)
                else:
                    out("  %s%s - %s" % (prefix, command, description))

            out("")
            out("For more detailed information, type: "
                "%sHELP <command>" % prefix)

        else:
            key = ' '.join(args)

            # If they use a !, strip it off
            if key[:1] == '!':
                key = key[1:]

            try:
                # Filter location-specific commands
                if not local.use_locations:
                    if key in self.location_cmds:
                        raise KeyError

                (head, body) = self.bighelp[key]

            except KeyError:
                out("Sorry, no help available for '%s'." % key)

            else:
                out("Syntax: %s%s %s" % (prefix, key, head))
                out("")
                out(body)

    def handleCmd_REBOOT(self, out, args, prefix):

        if len(args) == 0:
            out("Rebooting Node...")
            self.main.shutdown(reconnect='instant')
            return

        self.syntaxHelp(out, 'REBOOT', prefix)

    def handleCmd_UDP(self, out, args, prefix):
        if len(args) == 0:
            out("Dtella's UDP port is currently set to: %d"
                % self.main.state.udp_port)
            return

        elif len(args) == 1:
            try:
                port = int(args[0])
                if not 1 <= port <= 65535:
                    raise ValueError
            except ValueError:
                pass
            else:
                out("Changing UDP port to: %d" % port)
                asyncio.create_task(self.main.changeUDPPort(port))
                return

        self.syntaxHelp(out, 'UDP', prefix)

    def handleCmd_ADDPEER(self, out, args, prefix):

        if len(args) == 1:
            try:
                ad = Ad().set_addr(args[0])
            except ValueError:
                pass
            else:
                if not ad.port:
                    out("Port number must be nonzero.")

                elif self.main.auth('sx', ad):
                    self.main.state.refresh_peer(ad, 0)
                    out("Added to peer cache: %s" % ad.get_text_ip_port())

                    # Jump-start stuff if it's not already going
                    self.main.schedule_start_icm(0)
                else:
                    out("The address '%s' is not permitted on this network."
                        % ad.get_text_ip_port())
                return

        self.syntaxHelp(out, 'ADDPEER', prefix)

    def handleCmd_INVITE(self, out, args, prefix):

        if len(args) == 0:
            osm = self.main.osm
            if osm:
                ad = Ad().set_addr(osm.me.ipp)
                extra_msg = ""
            else:
                # If I don't know my own IP, at least fill in a dummy one.
                ad = Ad().set_addr(("0.0.0.0", self.main.state.udp_port))
                extra_msg = " (replace 0.0.0.0 with your real IP address)"

            out("Tell your friend to enter the following into their client "
                "to join the network%s:" % extra_msg)
            out("")
            out("  !addpeer %s" % ad.get_text_ip_port())
            out("")
            return

        self.syntaxHelp(out, 'INVITE', prefix)

    def handleCmd_PERSISTENT(self, out, args, prefix):
        if len(args) == 0:
            if self.main.state.persistent:
                out("Persistent mode is currently ON.")
            else:
                out("Persistent mode is currently OFF.")
            return

        if len(args) == 1:
            if args[0] == 'ON':
                out("Set persistent mode to ON.")
                self.main.state.persistent = True
                self.main.state.save()

                if self.main.osm:
                    self.main.osm.updateMyInfo()

                self.main.schedule_start_icm(0)
                return

            elif args[0] == 'OFF':
                out("Set persistent mode to OFF.")
                self.main.state.persistent = False
                self.main.state.save()

                if self.main.osm:
                    self.main.osm.updateMyInfo()
                return

        self.syntaxHelp(out, 'PERSISTENT', prefix)

    def handleCmd_LOCALSEARCH(self, out, args, prefix):
        if len(args) == 0:
            if self.main.state.localsearch:
                out("Local searching is currently ON.")
            else:
                out("Local searching is currently OFF.")
            return

        if len(args) == 1:
            if args[0] == 'ON':
                out("Set local searching to ON.")
                self.main.state.localsearch = True
                self.main.state.save()
                return

            elif args[0] == 'OFF':
                out("Set local searching to OFF.")
                self.main.state.localsearch = False
                self.main.state.save()
                return

        self.syntaxHelp(out, 'LOCALSEARCH', prefix)

    def handleCmd_REJOIN(self, out, args, prefix):

        if len(args) == 0:

            if self.dch.state != 'invisible':
                out("Can't rejoin: You're not invisible!")
                return

            out("Rejoining...")
            self.dch.do_rejoin()
            return

        self.syntaxHelp(out, 'REJOIN', prefix)

    def handleCmd_USERS(self, out, args, prefix):

        if not self.dch.is_online:
            out("You must be online to use %sUSERS." % prefix)
            return

        self.showStats(
            out,
            "User Counts",
            lambda u, b: u,
            lambda v: "%d" % v,
            peers_only=False
        )

    def handleCmd_SHARED(self, out, args, prefix):

        if not self.dch.is_online:
            out("You must be online to use %sSHARED." % prefix)
            return

        self.showStats(
            out,
            "Bytes Shared",
            lambda u, b: b,
            lambda v: "%s" % format_bytes(v),
            peers_only=True
        )

    def handleCmd_DENSE(self, out, args, prefix):

        if not self.dch.is_online:
            out("You must be online to use %sDENSE." % prefix)
            return

        def compute(u, b):
            try:
                return (b / u, u)
            except ZeroDivisionError:
                return (0, u)

        self.showStats(
            out,
            "Share Density",
            compute,
            lambda v: "%s/user (%d)" % (format_bytes(v[0]), v[1]),
            peers_only=True
        )

    def handleCmd_RANK(self, out, args, prefix):

        if not self.dch.is_online:
            out("You must be online to use %sRANK." % prefix)
            return

        osm = self.main.osm

        tie = False
        rank = 1

        target = None

        if len(args) == 0:
            target = osm.me
        elif len(args) == 1:
            try:
                target = osm.get_user(args[0])
            except KeyError:
                out("The nick <%s> cannot be located." % args[0])
                return
        else:
            self.syntaxHelp(out, 'RANK', prefix)
            return

        if target is osm.me:
            who = "You are"
        else:
            who = "%s is" % target._nick

        for n in osm.user_list:
            if n is target:
                continue

            if n.shared > target.shared:
                rank += 1
            elif n.shared == target.shared:
                tie = True

        try:
            suffix = {1: 'st', 2: 'nd', 3: 'rd'}[rank % 10]
            if 11 <= (rank % 100) <= 13:
                raise KeyError
        except KeyError:
            suffix = 'th'

        if tie:
            tie = "tied for"
        else:
            tie = "in"

        out("%s %s %d%s place, with a share size of %s." %
            (who, tie, rank, suffix, format_bytes(target.shared))
            )

    def handleCmd_TOPIC(self, out, topic, prefix):

        if not self.dch.is_online:
            out("You must be online to use %sTOPIC." % prefix)
            return

        tm = self.main.osm.tm

        if topic is None:
            asyncio.create_task(out(tm.getFormattedTopic()))
        else:
            asyncio.create_task(out(None))
            tm.broadcastNewTopic(topic)

    def handleCmd_SUFFIX(self, out, text, prefix):

        if text is None:
            out("Your location suffix is \"%s\"" % self.main.state.suffix)
            return

        text = text[:8].rstrip().replace('$', '')

        self.main.state.suffix = text
        self.main.state.save()

        out("Set location suffix to \"%s\"" % text)

        osm = self.main.osm
        if osm:
            osm.updateMyInfo()

    def showStats(self, out, title, compute, format, peers_only):

        CHECK(self.dch.is_online)

        # Count users and bytes
        ucount = {}
        bcount = {}

        # Collect user count and share size
        for n in self.main.osm.user_list:

            if peers_only and not n.is_peer:
                continue

            try:
                ucount[n.location] += 1
                bcount[n.location] += n.shared
            except KeyError:
                ucount[n.location] = 1
                bcount[n.location] = n.shared

        # Collect final values
        values = {}
        for loc in ucount:
            values[loc] = compute(ucount[loc], bcount[loc])

        # Sort by value, in descending order
        locs = values.keys()
        locs.sort(key=lambda loc: values[loc], reverse=True)

        overall = compute(sum(ucount.values()), sum(bcount.values()))

        # Build info string and send it
        out("/== %s, by Location ==\\" % title)
        for loc in locs:
            out("| %s <= %s" % (format(values[loc]), loc))
        out("|")
        out("\\_ Overall: %s _/" % format(overall))

    def handleCmd_VERSION(self, out, args, prefix):
        if len(args) == 0:
            out("You have Dtella version %s." % local.version_string)

            if self.main.dcfg.version:
                min_v, new_v, url = self.main.dcfg.version
                out("The minimum required version is %s." % min_v)
                out("The latest posted version is %s." % new_v)
                out("Download Link: %s" % url)

            return

        self.syntaxHelp(out, 'VERSION', prefix)

    def handleCmd_TERMINATE(self, out, args, prefix):
        if len(args) == 0:
            self.main.loop.stop()
            return

        self.syntaxHelp(out, 'TERMINATE', prefix)

    def handleCmd_VERSION_OVERRIDE(self, out, text, prefix):
        if self.main.dcfg.overrideVersion():
            out("Overriding minimum version!  Don't be surprised "
                "if something breaks.")
            self.main.schedule_start_icm(0)
        else:
            out("%sVERSION_OVERRIDE not needed." % prefix)

    def handleCmd_DEBUG(self, out, text, prefix):

        asyncio.create_task(out(None))

        if not text:
            return

        text = text.strip().lower()
        args = text.split()

        if args[0] == "nbs":
            self.debug_neighbors(out)

        elif args[0] == "nodes":
            try:
                sortkey = int(args[1])
            except (IndexError, ValueError):
                sortkey = 0
            self.debug_nodes(out, sortkey)

        elif args[0] == "packets":
            if len(args) < 2:
                pass
            elif args[1] == "on":
                self.dbg_show_packets = True
            elif args[1] == "off":
                self.dbg_show_packets = False

        elif args[0] == "killudp":
            self.main.ph.transport.stopListening()

    def debug_neighbors(self, out):

        osm = self.main.osm
        if not osm:
            return

        asyncio.create_task(out("Neighbor Nodes: {direction, ipp, ping, nick}"))

        for pn in osm.pgm.pnbs.values():
            info = []

            if pn.is_outbound_link and pn.is_inbound_link:
                info.append("<->")
            elif pn.is_outbound_link:
                info.append("-->")
            elif pn.is_inbound_link:
                info.append("<--")

            info.append(binascii.hexlify(pn.ipp).upper().decode())

            if pn.avg_ping is not None:
                delay = pn.avg_ping * 1000.0
            else:
                delay = 0.0
            info.append("%7.1fms" % delay)

            try:
                nick = osm._lookup_ipp[pn.ipp].nick
            except KeyError:
                nick = ""
            info.append("(%s)" % nick)

            asyncio.create_task(out(' '.join(info)))

    def debug_nodes(self, out, sortkey):

        osm = self.main.osm
        if not (osm and osm.synced):
            asyncio.create_task(out("Not synced"))
            return

        me = osm.me

        now = time.time()

        asyncio.create_task(out("Online Nodes: {ipp, nb, persist, expire, uptime, dttag, nick}"))

        lines = []

        for n in ([me] + osm.nodes):
            n: "Node"
            info = []
            info.append(binascii.hexlify(n.ipp).upper())

            if n.ipp in osm.pgm.pnbs:
                info.append("Y")
            else:
                info.append("N")

            if n.persist:
                info.append("Y")
            else:
                info.append("N")

            if n is me:
                info.append(f"{osm.next_status_update_time:.4f}")
            else:
                info.append("%4d" % n.timeout_left)

            info.append("%8d" % (now - n.uptime))
            info.append("%8s" % n.dttag[3:])
            info.append("(%s)" % n.nick)

            lines.append(info)

        if 1 <= sortkey <= 7:
            lines.sort(key=lambda l: l[sortkey - 1])

        for line in lines:
            asyncio.create_task(out(' '.join(line)))
