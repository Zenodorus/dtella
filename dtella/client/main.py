"""
Dtella - Client Main Module
Copyright (C) 2008  Dtella Labs (http://www.dtella.org/)
Copyright (C) 2008  Paul Marks (http://www.pmarks.net/)
Copyright (C) 2008  Jacob Feisley (http://www.feisley.com/)
Copyright (C) 2021-2022  Daniel Stratman

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
import asyncio
import functools
from typing import Optional, Dict

import dtella.common as common
import dtella.common.local_config as local

from dtella.client.dc import DCHandler
from dtella.common.base import DtellaMain_Base
from dtella.common.log import LOG, set_log_file
from dtella.common.state import StateManager, client_load_savers
from dtella.dconfig.pull import DynamicConfigPuller
from dtella.util import word_wrap
from dtella.util.ipv4 import Ad
from dtella.util.scheduled_task import ScheduledTask

if local.use_locations:
    from dtella.util.reverse_dns import ipToHostname


STATE_FILE = "%s.state" % local.network_key


class DtellaMain_Client(DtellaMain_Base):
    """
    Main namespace class for a dtella node backed by a DC client.
    """

    def __init__(self, debug=False):
        super().__init__(debug)
        self._shutdown_task: ScheduledTask = ScheduledTask(self.shutdown(reconnect='no'), loop=self._loop)

        # Location map: ipp->string, usually only contains 1 entry
        self.location: Dict[str, str] = {}

        # Login counter (just for eye candy)
        self._login_counter: int = 0
        self._login_text: str = ""

        # DC Handler(s)
        self.dch: Optional[DCHandler] = None
        self.pending_dch: Optional[DCHandler] = None

        # Peer Handler
        self.ph = common.PeerHandler(self)

        # State Manager
        self.state: StateManager = StateManager(self, STATE_FILE, client_load_savers)

        # DNS Handler
        self.dcfg = DynamicConfigPuller(self)

    # attribute properties

    @property
    def main_port(self) -> int:
        return self.state.udp_port

    @property
    def state_observer(self) -> Optional[DCHandler]:
        return self.dch if (self.dch and self.dch.is_online) else None

    # state properties

    @property
    def _connection_desired(self):
        return self.dch or self.state.persistent

    # socket connections

    async def _is_udp_bound(self):
        """Returns whether or not the UDP port is bound."""

        if not await super()._is_udp_bound():
            await self.show_login_status("*** FAILED TO BIND UDP PORT ***")

            text = (f"Dtella was not able to listen on UDP port {self.state.udp_port}. One possible reason for this is "
                    "that you've tried to make your DC  client use the same UDP port as Dtella. Two programs are not "
                    "allowed listen on the same port.  To tell Dtella to use a different port, type !UDP followed by "
                    "a number.  Note that if you have a firewall or router, you will have to tell it to let traffic "
                    "through on this port.")
            for line in word_wrap(text):
                await self.show_login_status(line)
            return False
        return True

    # TODO
    async def changeUDPPort(self, udp_port):
        # Shut down the node, and start up with a different UDP port

        # Set a new UDP port, which will be used on the next bind.
        self.state.udp_port = udp_port
        self.state.save()

        if self._transport and not self._transport.is_closing():
            # Port is alive, so shutdown and kill it.  PeerHandler will reconnect when it notices the port is gone.
            await self.shutdown(reconnect='no')
            self.ph.transport.stopListening()
        else:
            # Port is dying, maybe because of a previous change request.  Just let the existing callbacks
            # take care of it.
            # Port is already gone, so try reconnecting.
            self.schedule_start_icm(0)

    # output

    def log_packet(self, text: str):
        super().log_packet(text)
        if (dch := self.dch) and dch.bot.dbg_show_packets:
            dch.bot.say(text)

    async def show_login_status(self, text, counter=None):
        """
        Push a login status update to the DC client.  counter may be:
        - int: set the value of the counter
        - 'inc' increment from the previous counter value
        - None: don't show a counter
        """

        if type(counter) is int:
            self._login_counter = counter
        elif counter == 'inc':
            self._login_counter += 1

        if counter is not None:
            text = f"{self._login_counter}. {text}"
            self._login_text = text

        LOG.debug(text)
        if self.dch:
            self.dch.push_Status(text)

    # control flow

    def start(self, dc_port: Optional[int] = None):

        # Logging for Dtella Client
        set_log_file("dtella.log", 1 << 20, 1)
        LOG.debug("Client Logging Manager Initialized")
        LOG.info(f"{local.hub_name} {local.version_string}")

        def bot_error_reporter(text):
            dch = self.dch
            if dch:
                dch.bot.say(
                    "Something bad happened.  You might want to email this to "
                    "bugs@dtella.org so we'll know about it:\n"
                    "Version: %s %s\n%s" %
                    (local.hub_name, local.version_tag[3:], text))

        # TODO translate this to asyncio
        # addTwistedErrorCatcher(bot_error_reporter)
        # addTwistedErrorCatcher(LOG.critical)

        super().start(dc_port)

    async def _run(self, dc_port: Optional[int] = None):

        # bind port to watch for connections from a DC client
        server = None
        dc_factory = functools.partial(DCHandler, self)
        try:
            server = await asyncio.start_server(dc_factory, host="127.0.0.1", port=dc_port)
        except OSError:
            LOG.warning("TCP bind failed.  Killing old process...")
            if common.terminate(dc_port):
                LOG.info("Ok.  Sleeping...")
                await asyncio.sleep(2.0)
                try:
                    server = await asyncio.start_server(dc_factory, host="127.0.0.1", port=dc_port)
                except OSError:
                    LOG.error("Bind failed again.  Giving up.")
            else:
                LOG.error("Kill failed.  Giving up.")

        if not server:
            # TODO find the right way to graceful exit here
            raise RuntimeError

        LOG.info(f"Listening on 127.0.0.1:{dc_port}")
        self.schedule_start_icm(0)

    async def _start_icm(self):
        # Get a fresh copy of dynamic config before starting ICM
        await self.dcfg.get_dynamic_config()
        try:
            for ipp in self.state.dns_ipcache[1]:
                self.state.refresh_peer(Ad(ipp), 0)
        except ValueError:
            pass
        await super()._start_icm()

    # def build_osm(self, my_ipp, node_ipps):
    #     return OnlineStateManager(self, my_ipp, node_ipps)

    def cleanup_on_exit(self):
        LOG.info("Reactor is shutting down.  Doing cleanup.")
        if self.dch:
            self.dch.state = 'shutdown'
        self.shutdown(reconnect='no')
        self.state.save()

    def afterShutdownHandlers(self):
        # If DC client had been kicked, reset.
        if self.dch:
            self.dch.do_rejoin()

        # Cancel the dns update timer, and remove pending callback.
        self.dcfg.dtellaShutdown()

    # state changes

    def set_dch(self, dch: DCHandler):

        if not self._shutdown_task.uncalled:
            # shutdown is either in progress, soon to be, or completed; don't bother attaching
            return
        assert self._shutdown_task.reset()

        assert not self.dch, "trying to replace an active DC handler"
        assert dch.state == 'ready', "new DC handler not ready"

        self.dch = dch
        self.schedule_start_icm(0)
        if self.is_working:
            text = self._login_text
            LOG.debug(text)
            dch.push_Status(text)

            # Send a message if there's a newer version
            self.dcfg.resetReportedVersion()
            self.dcfg.reportNewVersion()

        self.stateChange_ObserverUp()

    def remove_dch(self, dch: DCHandler):
        # DC client has left.

        LOG.debug("DC Client left")
        if self.pending_dch is dch:
            self.pending_dch = None
            return
        elif self.dch is not dch:
            return

        self.dch = None

        # Announce the DC client's departure.
        if dch.state == 'ready':
            self.stateChange_ObserverDown()

        # If another handler is waiting, let it on.
        if self.pending_dch:
            self.pending_dch.attach_me_to_dtella()
            self.pending_dch = None
            return

        # Maybe forget about reconnecting
        if not self._connection_desired:
            self._start_icm_task.cancel()

        # Maybe skip the disconnect
        if self.state.persistent or not (self.icm or self.osm):
            return

        # set a timeout to shutdown if no handler rejoins
        self._shutdown_task.reset(dtella.common.NO_CLIENT_TIMEOUT)

    # utility

    def queryLocation(self, my_ipp):
        # Try to convert the IP address into a human-readable location name.
        # This might be slightly more complicated than it really needs to be.

        assert local.use_locations

        ad = Ad(my_ipp)
        my_ip = ad.get_text_ip()

        skip = False
        for ip, loc in self.location.items():
            if ip == my_ip:
                skip = True
            elif loc:
                # Forget old entries
                del self.location[ip]

        # If we already had an entry for this IP, then don't start
        # another lookup.
        if skip:
            return

        # A location of None indicates that a lookup is in progress
        self.location[my_ip] = None

        def cb(hostname):

            # Use local_config to transform this hostname into a
            # human-readable location
            loc = local.hostnameToLocation(hostname)

            # If we got a location, save it, otherwise dump the
            # dictionary entry
            if loc:
                self.location[my_ip] = loc
            else:
                del self.location[my_ip]

            # Maybe send an info update
            if self.osm:
                self.osm.updateMyInfo()

        # Start lookup
        ipToHostname(ad).addCallback(cb)
